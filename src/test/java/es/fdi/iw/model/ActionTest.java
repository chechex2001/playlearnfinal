package es.fdi.iw.model;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.fdi.iw.SpringBootLauncher;

/**
 * Tests logic in User.java
 * 
 * @author mfreire
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=SpringBootLauncher.class)
public class ActionTest {	
	
	@Autowired
	EntityManager entityManager;
	
    @Test
    @Transactional
    public void testNewDocument() {
    	Action doc = new Action();
    	
//    	ActionType actionType=ActionType.valueOf("state");
    	String owner="chechex20";
    	long place=2;
    	String content="un content";
    	Date creationDate=new Date();
    	
    	doc=Action.setAction(ActionType.STATE, owner, place, content, creationDate, "1");
    	
    	
//            doc=DocumentInfo.SetDocument("un tittle", "una description", new Date().toString(), "img", new User());
            entityManager.persist(doc);
            entityManager.flush();
//            // see that the ID of the user was set by Hibernate
//            System.out.println("doc=" + doc + ", doc.id=" + doc.getId());
            Action foundDoc = entityManager.find(Action.class, doc.getId());
            System.out.println("foundUser=" + foundDoc.toString());
            assertEquals("Same name", doc.getActionType(), foundDoc.getActionType());
    }
	
	@Test
	public void testHexStringConversion() {
		assertArrayEquals(new byte[] {0x00, (byte) 0xff}, User.hexStringToByteArray("00ff"));
		assertArrayEquals(new byte[] {(byte) 0xff, 0x42}, User.hexStringToByteArray("ff42"));

		String example = "aabb1234deadbeef";
		assertEquals(User.byteArrayToHexString(User.hexStringToByteArray(example)), example);
	}
}
