package es.fdi.iw.model;

import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.fdi.iw.SpringBootLauncher;

/**
 * Tests logic in MessageTest.java
 * 
 * @author JLG
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=SpringBootLauncher.class)
public class MessageTest {	
	
	@Autowired
	EntityManager entityManager;
	
    @Test
    @Transactional
    public void testNewDocument() {
    	MessageEmail doc = new MessageEmail();
    	String owner="Chechex";
    	User userFrom = new User();
    	userFrom.setLogin("userFrom");
        entityManager.persist(userFrom);
        entityManager.flush();
        
        User userTo = new User();
        userTo.setLogin("userTo");
        entityManager.persist(userTo);
        entityManager.flush();
    	
    	doc=MessageEmail.createEmail("subject", "content", userFrom.getLogin(),userTo.getLogin(),owner , false);
    	
        entityManager.persist(doc.getEmail());
        entityManager.persist(doc);
        entityManager.flush();
        
        MessageEmail foundDoc = entityManager.find(MessageEmail.class, doc.getId());
        System.out.println("foundUser=" + foundDoc.toString());
        assertEquals("estos correos van a ", doc.getFromOwner(), foundDoc.getFromOwner());
    }
	

}
