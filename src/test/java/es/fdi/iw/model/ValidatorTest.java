package es.fdi.iw.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import utils.Validators;

public class ValidatorTest {

	
		
	@Test
	public void introduzcoUnIdLoEncriptoYloDesencripto(){
		
		String id="1";
		
		String elementEncripted= Validators.encriptElementId(id);
		if(Validators.isElementEncriptValid(id,elementEncripted)!="NOTVALID"){
			System.out.println("esta parte del test fue valida");
		}
		
		
		if(Validators.isElementEncriptValid("2345",elementEncripted)=="NOTVALID"){
			//compruebo que no funciona con una id no correcta 
				String idUnEncripted=Validators.getElementEncriptKey(elementEncripted,2);//deberia 
				
				 assertEquals(id,idUnEncripted);
		}
			
			
	}
}
