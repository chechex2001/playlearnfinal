package es.fdi.iw.model;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.fdi.iw.SpringBootLauncher;

/**
 * Tests logic in User.java
 * 
 * @author mfreire
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=SpringBootLauncher.class)
public class DocumentInfoTest {	
	
	@Autowired
	EntityManager entityManager;
	
    @Test
    @Transactional
    public void testNewDocument() {
    	DocumentInfo doc = new DocumentInfo();
    	User user = new User();
        user.setLogin("bob");
        
        List<FileInfo> files=new ArrayList<FileInfo>();
        
        FileInfo fInfo=new FileInfo();
        fInfo.setFileName("image1");
        fInfo.setPostTittleReference("un tittle");
        
        FileInfo fInfo1=new FileInfo();
        fInfo1.setFileName("image2");
        fInfo1.setPostTittleReference("un tittle");
        
        entityManager.persist(fInfo);
        entityManager.persist(fInfo1);
        entityManager.flush();
        files=(List<FileInfo>) entityManager.createNamedQuery("filesInfoByTittle", FileInfo.class).setParameter("postTittleReferenceParam", "un tittle").getResultList();
       for (FileInfo fileInfo : files) {
    	   System.out.println(fileInfo.toString());
		
	}            doc=DocumentInfo.SetDocument("un tittle", "una description", new Date().toString(), "img", user,files);
            entityManager.persist(doc);
            entityManager.flush();
            // see that the ID of the user was set by Hibernate
            System.out.println("doc=" + doc + ", doc.id=" + doc.getId());
            DocumentInfo foundDoc = entityManager.find(DocumentInfo.class, doc.getId());
            System.out.println("foundUser=" + foundDoc);
            assertEquals("Same name", doc.getTitle(), foundDoc.getTitle());
    }
	
//    @Test
//    @Transactional
//    public void testNewDocumentConFilesInfoReferencies() {
//    	DocumentInfo doc = new DocumentInfo();
//    	User user = new User();
//        user.setLogin("bob");
//            doc=DocumentInfo.SetDocument("un tittle", "una description", new Date().toString(), "img", user);
//            entityManager.persist(doc);
//            entityManager.flush();
//            // see that the ID of the user was set by Hibernate
//            System.out.println("doc=" + doc + ", doc.id=" + doc.getId());
//            DocumentInfo foundDoc = entityManager.find(DocumentInfo.class, doc.getId());
//            System.out.println("foundUser=" + foundDoc);
//            assertEquals("Same name", doc.getTitle(), foundDoc.getTitle());
//    }
	@Test
	public void testHexStringConversion() {
		assertArrayEquals(new byte[] {0x00, (byte) 0xff}, User.hexStringToByteArray("00ff"));
		assertArrayEquals(new byte[] {(byte) 0xff, 0x42}, User.hexStringToByteArray("ff42"));

		String example = "aabb1234deadbeef";
		assertEquals(User.byteArrayToHexString(User.hexStringToByteArray(example)), example);
	}
}
