package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.owasp.validator.html.AntiSamy;
import org.owasp.validator.html.CleanResults;
import org.owasp.validator.html.Policy;
import org.owasp.validator.html.PolicyException;
import org.owasp.validator.html.ScanException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import es.fdi.iw.controller.HomeController;
import es.fdi.iw.exceptions.SuspectInputException;

public class Validators {
	private static final Logger log = LoggerFactory.getLogger(HomeController.class);
	
	
	private static BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();
	/**
	 * Generate a hashed&salted hex-string from a user's pass and salt
	 * @param elementId to use; no length-limit!
	 * @param salt to use
	 * @return a string to store in the BD that does not reveal the password even
	 * if the DB is compromised. Note that brute-force is possible, but it will
	 * have to be targeted (ie.: use the same salt)
	 */
	public static String encriptElementId(String elementId) {
		/*
		Código viejo: sólo 1 iteración de SHA-1. bCrypt es mucho más seguro (itera 1024 veces...)
		
		Además, bcryptEncoder guarda la sal junto a la contraseña
		byte[] saltBytes = hexStringToByteArray(user.salt);
		byte[] passBytes = pass.getBytes();
		byte[] toHash = new byte[saltBytes.length + passBytes.length];
		System.arraycopy(passBytes, 0, toHash, 0, passBytes.length);
		System.arraycopy(saltBytes, 0, toHash, passBytes.length, saltBytes.length);
		return byteArrayToHexString(sha1hash(toHash));
		*/
		return bcryptEncoder.encode(elementId);
	}	

	public static String isElementEncriptValid(String elementEncript,String elementOriginalId) {
		if (bcryptEncoder.matches(elementEncript, elementOriginalId))
			return elementEncript;
		else
			return "NOTVALID";
		
	}
	
	public static String getElementEncriptKey(String elementOriginalId, int size) {
		for(int i=0; i<=size; i++){
			if (bcryptEncoder.matches(String.valueOf(i),elementOriginalId))
					return String.valueOf(i);	
					
		}		
			return "NOTVALID";
		
	}
	private static String passwordPattern = "^^[a-zA-Z]\\w{3,14}$";
	
	private static String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	 public static String emailValidator(String email) throws SuspectInputException
			    {
			if (email == null || email.isEmpty()) {
			    log.error("El parametro introducido esta vacio o es nulo");
			   
			}
			String response = email.trim();
			Pattern pattern = Pattern.compile(emailPattern);
			Matcher matcher = pattern.matcher(response);
			if (!matcher.matches()) {
			    log.error("El parametro introducido esta vacio o es nulo");
			   
			}
			return response;
		    }

	 public static String passwordValidator(String password) throws SuspectInputException
			    {
			if (password == null || password.isEmpty()) {
			    log.error("El parametro introducido esta vacio o es nulo");
			    
			}
			String response = password.trim();
			Pattern pattern = Pattern.compile(passwordPattern);
			Matcher matcher = pattern.matcher(response);
			if (!matcher.matches()) {
			    log.error("El parametro introducido esta vacio o es nulo");
			    
			}
			return response;
		    }
	 
	 
	 public static String textValidator(String text) throws SuspectInputException {
	    	
			if (text == null || text.isEmpty()) {
				
			    log.error("El parametro introducido esta vacio o es nulo");
			    
			}
			String response = text.trim();
			try {
			    Policy policy = Policy.getInstance(Validators.class
				    .getResource("/antisamy-tinymce-1.4.4.xml"));
			    AntiSamy validator = new AntiSamy(policy);
			    CleanResults cr = validator.scan(response, policy, AntiSamy.SAX);
			    if (cr.getNumberOfErrors() != 0) {
				log.error("El parametro introducido esta vacio o es nulo");
				
			    }
			} catch (PolicyException  e) {
			    
			    log.error(e.getMessage(), e);
			    
			} catch (ScanException e) {
			    log.error(e.getMessage(), e);
			}
			return response;

		    }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
}
