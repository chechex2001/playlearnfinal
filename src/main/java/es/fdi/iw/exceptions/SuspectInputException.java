package es.fdi.iw.exceptions;

public class SuspectInputException extends Exception {

	public SuspectInputException() {
		super();
		// TODO Auto-generated constructor stub
	}	

	public SuspectInputException(String error, Throwable cause) {
		super(error, cause);
		// TODO register logger
	}

	public SuspectInputException(Throwable cause) {
		super("El parametro introducido no es valido", cause);
		// TODO Auto-generated constructor stub
	}
	

}
