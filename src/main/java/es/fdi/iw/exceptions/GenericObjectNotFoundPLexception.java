package es.fdi.iw.exceptions;

public class GenericObjectNotFoundPLexception extends Exception {

	public GenericObjectNotFoundPLexception() {
		super();
		// TODO Auto-generated constructor stub
	}	

	public GenericObjectNotFoundPLexception(String error, Throwable cause) {
		super(error, cause);
		// TODO register logger
	}

	public GenericObjectNotFoundPLexception(Throwable cause) {
		super("El parametro introducido no es valido", cause);
		// TODO Auto-generated constructor stub
	}
	

}
