package es.fdi.iw.model;

public class ResponseData {
	
	boolean success;
	Object data;
	String cookie;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object tournament) {
		this.data = tournament;
	}
	public String getCookie() {
		return cookie;
	}
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
	
	

}
