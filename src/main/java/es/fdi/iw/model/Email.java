package es.fdi.iw.model;


import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
public class Email {
	private long id;
	private String subject;
	private String content;
	private Date creationDate;	
	private String owner; 
	

	public static Email setAction(String subject, String content, Date creationDate ,String owner) {
		Email a=new Email();
		a.subject = subject;
		a.content = content;
		a.creationDate = creationDate;
		a.owner=owner;
		return a;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setId(long id) {
		this.id = id;
	}	
	
	@Lob
	@Column(length = 10000, name = "content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return "Email [id=" + id + ", subject=" + subject + ", content=" + content + ", creationDate=" + creationDate
				+ "]";
	}

	

}