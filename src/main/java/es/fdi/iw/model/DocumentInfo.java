package es.fdi.iw.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({ @NamedQuery(name = "allDocument", query = "select o from DocumentInfo o"),
		@NamedQuery(name = "docById", query = "select u from DocumentInfo u where u.id = :idParam"),
		@NamedQuery(name = "docByOwner", query = "select u from DocumentInfo u where u.owner = :ownerParam"),
		@NamedQuery(name = "delDocument", query = "delete from DocumentInfo o where o.id=:idParam") })
public class DocumentInfo {
	private long id;
	private String title;
	private String description;
	private String creationDate;
	private String mimeType;
	private String owner;
	// private List<String> postImages;

	private List<FileInfo> files;

	public static DocumentInfo SetDocument(String title, String description, String creationDate, String mimeType,
			User owner, List<FileInfo> files) {
		DocumentInfo d = new DocumentInfo();

		d.title = title;
		d.description = description;
		d.creationDate = creationDate;
		d.mimeType = mimeType;
		d.owner = owner.getLogin();
		d.files=files;
		return d;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	// @ManyToOne(targetEntity=User.class)
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setFiles(List<FileInfo> filesLinked) {
		this.files = filesLinked;
	}

	@ManyToMany(targetEntity = FileInfo.class, mappedBy = "fileName")
	public List<FileInfo> getFiles() {
		return files;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "DocumentInfo [id=" + id + ", title=" + title + ", description=" + description + ", creationDate="
				+ creationDate + ", mimeType=" + mimeType + ", owner=" + owner + ", files=" + files.toString() + "";
	}

	// public List<String> getPostImages() {
	// return postImages;
	// }
	//
	// public void setPostImages(List<String> postImages) {
	// this.postImages = postImages;
	// }

}