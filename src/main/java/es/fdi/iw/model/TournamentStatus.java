package es.fdi.iw.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
		@NamedQuery(name = "getTournamentsAvailables", query = "select o from TournamentStatus o where tournamentStatus=:status"),
		@NamedQuery(name = "getTournamentsById", query = "select o from TournamentStatus o where id=:id"),
		@NamedQuery(name = "getTournamentsByUser", query = "select o from TournamentStatus o where (userId1=:user or userId2=:user) and tournamentStatus=:status"),
		
		@NamedQuery(name = "getTournamentsByWinner", query = "select o from TournamentStatus o where ganador=:user") })
public class TournamentStatus {
	private long id;
	private StatusType tournamentStatus;
	private String userId1;
	private String userId2;
	private int user1Points;
	private int user2Points;
	private int preguntasCompletadasUsr2;
	private String ganador;
	private int preguntasCompletadasUsr1;

	
	
	public boolean setPlayer(String u) {
		if (userId1.isEmpty())
			userId1 = u;
		else if (userId2.isEmpty())
			userId2 = u;
		else
			return false;

		return true;
	}
	public static TournamentStatus setTournament(StatusType tournamentStatus, String userId1, String userId2, int user1Points,
			int user2Points, int preguntasCompletadasUsr2, String ganador, int preguntasCompletadasUsr1) {
		TournamentStatus tour = new TournamentStatus();
		
		tour.tournamentStatus = tournamentStatus;
		tour.userId1 = userId1;
		tour.userId2 = userId2;
		tour.user1Points = user1Points;
		tour.user2Points = user2Points;
		tour.preguntasCompletadasUsr2 = preguntasCompletadasUsr2;
		tour.ganador = ganador;
		tour.preguntasCompletadasUsr1 = preguntasCompletadasUsr1;
		return tour;
	}


	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	


	public StatusType getTournamentStatus() {
		return tournamentStatus;
	}


	public void setTournamentStatus(StatusType tournamentStatus) {
		this.tournamentStatus = tournamentStatus;
	}


	public String getUserId1() {
		return userId1;
	}


	public void setUserId1(String userId1) {
		this.userId1 = userId1;
	}


	public String getUserId2() {
		return userId2;
	}


	public void setUserId2(String userId2) {
		this.userId2 = userId2;
	}


	public int getUser1Points() {
		return user1Points;
	}


	public void setUser1Points(int user1Points) {
		this.user1Points = user1Points;
	}


	public int getUser2Points() {
		return user2Points;
	}


	public void setUser2Points(int user2Points) {
		this.user2Points = user2Points;
	}


	public int getpreguntasCompletadasUsr2() {
		return preguntasCompletadasUsr2;
	}


	public void setpreguntasCompletadasUsr2(int preguntasCompletadasUsr2) {
		this.preguntasCompletadasUsr2 = preguntasCompletadasUsr2;
	}


	public String getGanador() {
		return ganador;
	}


	public void setGanador(String ganador) {
		this.ganador = ganador;
	}


	public int getpreguntasCompletadasUsr1() {
		return preguntasCompletadasUsr1;
	}


	public void setpreguntasCompletadasUsr1(int preguntasCompletadasUsr1) {
		this.preguntasCompletadasUsr1 = preguntasCompletadasUsr1;
	}


	@Override
	public String toString() {
		return "TournamentStatus [id=" + id + ", tournamentStatus=" + tournamentStatus + ", userId1=" + userId1
				+ ", userId2=" + userId2 + ", user1Points=" + user1Points + ", user2Points=" + user2Points
				+ ", preguntasCompletadasUsr2=" + preguntasCompletadasUsr2 + ", ganador=" + ganador + ", preguntasCompletadasUsr1="
				+ preguntasCompletadasUsr1 + "]";
	}

	

}
