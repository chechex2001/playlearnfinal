package es.fdi.iw.model;

public class Stadistics {
	private int numberOfLikes;
	private int numberOfComments;
	private int numberOfTweets;
	private int progress;
	private int microblogging;
	private int activities;
	
	
	
	public int getNumberOfTweets() {
		return numberOfTweets;
	}
	public void setNumberOfTweets(int numberOfTweets) {
		this.numberOfTweets = numberOfTweets;
	}
	public int getProgress() {
		return progress;
	}
	public void setProgress(int progress) {
		this.progress = progress;
	}
	public int getMicroblogging() {
		return microblogging;
	}
	public void setMicroblogging(int microblogging) {
		this.microblogging = microblogging;
	}
	public int getActivities() {
		return activities;
	}
	public void setActivities(int activities) {
		this.activities = activities;
	}
	public int getNumberOfLikes() {
		return numberOfLikes;
	}
	public void setNumberOfLikes(int numberOfLikes) {
		this.numberOfLikes = numberOfLikes;
	}
	public int getNumberOfComments() {
		return numberOfComments;
	}
	public void setNumberOfComments(int numberOfComments) {
		this.numberOfComments = numberOfComments;
	}
	
}
