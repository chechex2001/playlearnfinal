package es.fdi.iw.model;

public enum StatusType {
	WAIT("En espera"),
	READY("Preparada"),//cuando ya hay 2
	AVAILABLE("Disponible"),//cuando hay solo 1 usuario
	INPROGRESS("En proceso"),
	ENDED("Finalizada");
	
	private String content;
	private StatusType(String content) {
		this.content=content;
	}
	public String getContent() {
		return content;
	}
	public static StatusType fromString(String department) {
		if (department != null) {
			for (StatusType aux : StatusType.values()) {
				if (department.equalsIgnoreCase(aux.content)) {
					return aux;
				}
			}
		}
		return null;
	}
	
}
