package es.fdi.iw.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries({ @NamedQuery(name = "allMessage", query = "select o from MessageEmail o"),
		// @NamedQuery(name = "allMessageByfromOwner", query = "select o from
		// MessageEmail o where o.isInbox=true and o.fromOwner
		// =:fromOwnerParam"),
//		@NamedQuery(name = "messageBytoOwner", query = "select o from MessageEmail o where o.toOwner =:toOwnerParam"),
		// @NamedQuery(name = "messageByOwner", query = "select u from
		// MessageEmail u where u.owner = :ownerParam"),
		@NamedQuery(name = "delMessage", query = "delete from MessageEmail o where o.id= :idParam") })
public class MessageEmail {
	private long id;
	Email email;
	String fromOwner;

	String toOwner;
	// @Column(name="owner", length=10)

	boolean isInbox;
	boolean isRead;
	boolean isInTrash;

	public static MessageEmail createEmail(String subject, String content, String from, String to, String owner,
			boolean isInbox) {

		MessageEmail m = new MessageEmail();

		Email email = new Email();
		email = Email.setAction(subject, content, new Date(), owner);
		m.email = email;

		m.fromOwner = from;
		m.toOwner = to;

		m.isRead = false;
		m.isInTrash = false;
		m.isInbox = isInbox;
		return m;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@OneToOne(targetEntity = Email.class)
	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	@Column(length = 90)
	public String getFromOwner() {
		return fromOwner;
	}

	public void setFromOwner(String from) {
		this.fromOwner = from;
	}

	@Column(length = 10)
	public String getToBeSend() {
		return toOwner;
	}

	public void setToBeSend(String to) {
		this.toOwner = to;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public boolean isInTrash() {
		return isInTrash;
	}

	public void setInTrash(boolean isInTrash) {
		this.isInTrash = isInTrash;
	}

	public boolean isInbox() {
		return isInbox;
	}

	public void setInbox(boolean isInbox) {
		this.isInbox = isInbox;
	}

	@Override
	public String toString() {
		return "MessageEmail [id=" + id + ", email=" + email + ", fromOwner=" + fromOwner + ", toOwner=" + toOwner
				+ ", isInbox=" + isInbox + ", isRead=" + isRead + ", isInTrash=" + isInTrash + "]";
	}

}
