package es.fdi.iw.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries({
		@NamedQuery(name = "allAction", query = "select o from Action o order by o.creationDate"),
		@NamedQuery(name = "actionById", query = "select u from Action u where u.id = :idParam "),
		@NamedQuery(name = "actionByPlace", query = "select u from Action u where u.place = :idParam  and u.actionType= :actionTypeParam"),
		@NamedQuery(name = "actionByPlaceTypeOwner", query = "select u from Action u where u.place = :idParam  and u.actionType= :actionTypeParam and u.owner= :ownerId"),
		@NamedQuery(name = "delAction", query = "delete from Action u where u.id= :idParam"),
		@NamedQuery(name = "getLikesComents", query = "select u from Action u where u.actionType = :actionParam order by u.creationDate"),
		@NamedQuery(name = "getByOwnerLikesComents", query = "select u from Action u where u.actionType = :actionParam and u.owner=:ownerParam order by u.creationDate"),
		@NamedQuery(name = "actionByTittle", query = "select u from Action u where u.owner = :ownerParam ") })
public class Action {
	private long id;
	private ActionType actionType;
	private String owner;
	private String ownerId;
	private long place;
	private String content;
	private Date creationDate;

	public static Action setAction(ActionType actionType, String owner, long place, String content, Date creationDate,
			String ownerId) {
		Action a = new Action();

		a.actionType = actionType;
		a.owner = owner;
		a.ownerId = ownerId;
		a.place = place;
		a.content = content;
		a.creationDate = creationDate;
		return a;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public ActionType getActionType() {
		return actionType;
	}

	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	public long getPlace() {
		return place;
	}

	public void setPlace(long place) {
		this.place = place;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	@Override
	public String toString() {
		return "Action [id=" + id + ", actionType=" + actionType + ", owner=" + owner + ", ownerId=" + ownerId
				+ ", place=" + place + ", content=" + content + ", creationDate=" + creationDate + "]";
	}

}