package es.fdi.iw.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@NamedQueries({
		@NamedQuery(name = "allTrophies", query = "select o from Trophies o order by o.creationDate"),
		@NamedQuery(name = "trophiesById", query = "select u from Trophies u where u.id = :idParam "),
		@NamedQuery(name = "trophiesByPlace", query = "select u from Trophies u where u.place = :idParam  and u.trophieType= :TrophieTypeParam"),
		@NamedQuery(name = "trophiesByPlaceTypeOwner", query = "select u from Trophies u where u.place = :idParam  and u.trophieType= :TrophieTypeParam and u.owner= :ownerId"),
		@NamedQuery(name = "delTrophies", query = "delete from Trophies u where u.id= :idParam"),
		@NamedQuery(name = "getTrophiesByType", query = "select u from Trophies u where u.trophieType = :actionParam order by u.creationDate"),
		@NamedQuery(name = "getByOwnerTrophiesType", query = "select u from Trophies u where u.trophieType = :actionParam and u.owner=:ownerParam order by u.creationDate") })
public class Trophies {
	private long id;
	private TrophieType trophieType;
	private String owner;
	private String ownerId;
	private Action place;
	private TrophieValues content;
	private Date creationDate;

	public static Trophies setAction(TrophieType trophieType, String owner, Action place, TrophieValues content, Date creationDate,
			String ownerId) {
		Trophies a = new Trophies();

		a.trophieType = trophieType;
		a.owner = owner;
		a.ownerId = ownerId;
		a.place = place;
		a.content = content;
		a.creationDate = creationDate;
		return a;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public TrophieType getTrophieType() {
		return trophieType;
	}

	public void setTrophieType(TrophieType TrophieType) {
		this.trophieType = TrophieType;
	}
	@OneToOne(targetEntity=Action.class, fetch=FetchType.EAGER)
	public Action getPlace() {
		return place;
	}

	public void setPlace(Action place) {
		this.place = place;
	}

	public TrophieValues getContent() {
		return content;
	}

	public void setContent(TrophieValues content) {
		this.content = content;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	@Override
	public String toString() {
		return "Action [id=" + id + ", TrophieType=" + trophieType + ", owner=" + owner + ", ownerId=" + ownerId
				+ ", place=" + place + ", content= creationDate=" + creationDate + "]";
	}

}