package es.fdi.iw.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries({
		@NamedQuery(name = "allTemas", query = "select o from Tema o order by o.creationDate"),
		@NamedQuery(name = "temaByActionId", query = "select o from Tema o where o.baseAction.id=:actionParam"),
		@NamedQuery(name = "temaById", query = "select u from Tema u where u.id = :idParam "),
		@NamedQuery(name = "delTema", query = "delete from Tema u where u.id= :idParam")})
public class Tema {
	private long id;	
	private String tittle;
	private String hastags;
	private Action baseAction;	
	private Date creationDate;
	private int numberOfLikes;
	private int numberOfComments;


	public static Tema setTema( String tittle, String hastags, Action baseAction, Date creationDate, int numberOfLikes,
			int numberOfComments) {
		Tema a=new Tema();
		a.tittle = tittle;
		a.hastags = hastags;
		a.baseAction = baseAction;
		a.creationDate = creationDate;
		a.numberOfLikes = numberOfLikes;
		a.numberOfComments = numberOfComments;
		return a;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public int getNumberOfLikes() {
		return numberOfLikes;
	}

	public void setNumberOfLikes(int numberOfLikes) {
		this.numberOfLikes = numberOfLikes;
	}

	public int getNumberOfComments() {
		return numberOfComments;
	}

	public void setNumberOfComments(int numberOfComments) {
		this.numberOfComments = numberOfComments;
	}

	public String getTittle() {
		return tittle;
	}

	public void setTittle(String tittle) {
		this.tittle = tittle;
	}

	public String getHastags() {
		return hastags;
	}

	public void setHastags(String hastags) {
		this.hastags = hastags;
	}
	@OneToOne(targetEntity=Action.class, fetch=FetchType.EAGER)
	public Action getBaseAction() {
		return baseAction;
	}

	public void setBaseAction(Action baseAction) {
		this.baseAction = baseAction;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return "Tema [id=" + id + ", tittle=" + tittle + ", hastags=" + hastags + ", baseAction=" + baseAction
				+ ", creationDate=" + creationDate + "]";
	}

}