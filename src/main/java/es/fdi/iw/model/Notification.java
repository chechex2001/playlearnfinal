package es.fdi.iw.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries({ 
	 	@NamedQuery(name = "allNotification", query = "select o from Notification o ORDER BY o.date DESC"),
	 	@NamedQuery(name = "allNotificationsByActionId", query = "select o from Notification o WHERE o.action.id=:typeParam  ORDER BY date DESC"),
	 	@NamedQuery(name = "allNotificationsByActionType", query = "select o from Notification o WHERE o.action.actionType=:typeParam  ORDER BY date DESC"),
	 	@NamedQuery(name = "getNotificationForUser", query = "select o from Notification o WHERE o.action.owner =:userParam ORDER BY date DESC"),
		@NamedQuery(name = "notificationById", query = "select u from Notification u where u.id = :idParam ORDER BY date "),
		@NamedQuery(name = "delNotification", query = "delete from Notification o where o.id=:idParam ") })
public class Notification {
	private long id;
	private Action action;
	private String description;
	private Date date;
	

	public static Notification setNotification(Action action, String description, Date date) {
		Notification n = new Notification();
		n.action = action;
		n.description = description;
		n.date = date;
		
		return n;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@OneToOne(targetEntity = Action.class)
	public Action getAction() {
		return action;
	}


	public void setAction(Action action) {
		this.action = action;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Notification [id=" + id + ", action=" + action.toString() + ", description=" + description + ", date="
				+ date+"]";
	}

}