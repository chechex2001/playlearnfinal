package es.fdi.iw.model;

public enum TrophieValues {
	ORO("Oro"),
	BRONCE("Bronce"),
	PLATA("Plata");
	
	private String content;
	private TrophieValues(String content) {
		this.content=content;
	}
	public String getContent() {
		return content;
	}
	public static TrophieValues fromString(String trophieType) {
		if (trophieType != null) {
			for (TrophieValues aux : TrophieValues.values()) {
				if (trophieType.equalsIgnoreCase(aux.content)) {
					return aux;
				}
			}
		}
		return null;
	}
	
}
