package es.fdi.iw.model;

public enum TrophieType {
	MEDALLA("Medalla"),
	TROFEO("Trofeo"),
	LOGRO("Logro");
	
	private String content;
	private TrophieType(String content) {
		this.content=content;
	}
	public String getContent() {
		return content;
	}
	public static TrophieType fromString(String trophieType) {
		if (trophieType != null) {
			for (TrophieType aux : TrophieType.values()) {
				if (trophieType.equalsIgnoreCase(aux.content)) {
					return aux;
				}
			}
		}
		return null;
	}
	
}
