package es.fdi.iw.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
		@NamedQuery(name = "filesByTittle", query = "select u from FileInfo u where u.postTittleReference = :postTittleReferenceParam"),
		@NamedQuery(name = "allFiles", query = "select o from FileInfo o"),
		@NamedQuery(name = "fileById", query = "select u from FileInfo u where u.id = :idParam"),
		@NamedQuery(name = "delFiles", query = "delete from FileInfo o where o.id=:idParam") })
public class FileInfo {
	private long id;
	String fileName;
	String postTittleReference;

	public static FileInfo createFile(String fileName, String postTittleReference) {
		FileInfo f = new FileInfo();

		f.fileName = fileName;
		f.postTittleReference = postTittleReference;
		return f;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPostTittleReference() {
		return postTittleReference;
	}

	public void setPostTittleReference(String postTittleReference) {
		this.postTittleReference = postTittleReference;
	}

	@Override
	public String toString() {
		return "FileInfo [id=" + id + ", fileName=" + fileName + ", postTittleReference=" + postTittleReference + "]";
	}

}
