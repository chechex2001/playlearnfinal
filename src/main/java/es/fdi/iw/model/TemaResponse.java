package es.fdi.iw.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries({		
		@NamedQuery(name = "allTemaResponses", query = "select o from TemaResponse o"),
		@NamedQuery(name = "allTemaResponsesByTema", query = "select o from TemaResponse o where tema = :temaParam"),
		@NamedQuery(name = "temaResponseById", query = "select u from TemaResponse u where u.id = :idParam"),
		@NamedQuery(name = "delTemaResponse", query = "delete from TemaResponse o where o.id=:idParam") })
public class TemaResponse {
	private long id;
	private  Tema originalLeaf;	
	private Action userResponseReference;
	private String hashtags;
	private int points;
	
	

	

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@ManyToOne(targetEntity=Tema.class)
	public Tema getOriginalLeaf() {
		return originalLeaf;
	}

	public void setOriginalLeaf(Tema originalLeaf) {
		this.originalLeaf = originalLeaf;
	}
	@OneToOne(targetEntity=Action.class)
	public Action getUserResponseReference() {
		return userResponseReference;
	}

	public void setUserResponseReference(Action userResponseReference) {
		this.userResponseReference = userResponseReference;
	}

	public String getHashtags() {
		return hashtags;
	}

	public void setHashtags(String hashtags) {
		this.hashtags = hashtags;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public static TemaResponse setTemaResponse(Tema originalLeaf, Action userResponseReference, String hashtags) {
		TemaResponse a=new TemaResponse();
		
		a.originalLeaf = originalLeaf;
		a.userResponseReference = userResponseReference;
		a.hashtags = hashtags;
		a.points = 0;
		return a;
	}

	@Override
	public String toString() {
		return "TemaResponse [id=" + id + ", originalLeaf=" + originalLeaf + ", userResponseReference="
				+ userResponseReference + ", hashtags=" + hashtags + ", points=" + points + "]";
	}
	
	
	



}
