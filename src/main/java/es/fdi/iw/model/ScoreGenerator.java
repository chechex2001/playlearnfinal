package es.fdi.iw.model;

import es.fdi.iw.dataAcces.DaoFactory;

public class ScoreGenerator {
	private int newRegister;
	private int newLikes;
	private int newCommentsInMicroBlogin;
	private int newCommentsNotInMicroBlogin;
	
	private int totalMicroBloginPlaces;
	private int totalLikesPlaces;
	private int totalCommentsPlaces;
	
	private int usersInLast7Dates;
	private int likesInLast7Dates;
	private int commentsInLast7Dates;
	
	private int microBloginPoints;
	private int likesPoints;
	private int commentsPoints;
	
	private int microBloginPorcent;
	private int likesPorcent;
	private int commentsPorcent;
	
	
	
	
	public ScoreGenerator(DaoFactory dataConnector , String adminName) {
		
//		dataConnector
		newRegister=dataConnector.getDaoUser().getAllLastUserRegistered().size();
		newLikes=dataConnector.getDaoAction().getAllActions().size();
		newCommentsInMicroBlogin=1;
		totalMicroBloginPlaces=dataConnector.getDaoAction().getAllMyStatesCount(adminName);
		totalLikesPlaces=dataConnector.getDaoAction().getAllComments().size();
		totalCommentsPlaces=dataConnector.getDaoAction().getAllPosts(); 
		usersInLast7Dates=newRegister;//falta hacer
		likesInLast7Dates=newLikes;//falta hacer
		commentsInLast7Dates=dataConnector.getDaoAction().getAllComments().size();
		microBloginPoints=totalLikesPlaces*5;
		likesPoints=totalLikesPlaces*2;
		commentsPoints=totalCommentsPlaces*3;
		microBloginPorcent=12;
		likesPorcent=((totalLikesPlaces-newLikes)*10)/100;
		commentsPorcent=((totalCommentsPlaces-newLikes)*10)/100;
	}
	public int getNewRegister() {
		return newRegister;
	}
	public int getNewLikes() {
		return newLikes;
	}
	public int getNewCommentsInMicroBlogin() {
		return newCommentsInMicroBlogin;
	}
	public int getNewCommentsNotInMicroBlogin() {
		return newCommentsNotInMicroBlogin;
	}
	public int getTotalMicroBloginPlaces() {
		return totalMicroBloginPlaces;
	}
	public int getTotalLikesPlaces() {
		return totalLikesPlaces;
	}
	public int getTotalCommentsPlaces() {
		return totalCommentsPlaces;
	}
	public int getUsersInLast7Dates() {
		return usersInLast7Dates;
	}
	public int getLikesInLast7Dates() {
		return likesInLast7Dates;
	}
	public int getCommentsInLast7Dates() {
		return commentsInLast7Dates;
	}
	public int getMicroBloginPoints() {
		return microBloginPoints;
	}
	public int getLikesPoints() {
		return likesPoints;
	}
	public int getCommentsPoints() {
		return commentsPoints;
	}
	public int getMicroBloginPorcent() {
		return microBloginPorcent;
	}
	public int getLikesPorcent() {
		return likesPorcent;
	}
	public int getCommentsPorcent() {
		return commentsPorcent;
	}
	
	
	
}
