package es.fdi.iw.model;


import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@NamedQueries({ 
 	@NamedQuery(name = "allHashIdentificator", query = "select o from HashIdentificator o ORDER BY o.creationDate DESC"),
 	@NamedQuery(name = "allTweetHashIdentificators", query = "select DISTINCT o from HashIdentificator o WHERE UPPER(o.hashtag)=:hasparam ORDER BY o.creationDate"),
 	@NamedQuery(name = "getHashIdentificatorForUser", query = "select o from HashIdentificator o WHERE o.owner =:userParam ORDER BY o.creationDate DESC"),
	@NamedQuery(name = "HashIdentificatorById", query = "select u from HashIdentificator u where u.id = :idParam ORDER BY u.creationDate "),
	@NamedQuery(name = "delHashIdentificator", query = "delete from HashIdentificator o where o.id=:idParam ") })
public class HashIdentificator {
	private long id;
	private Tema subject;
	private String hashtag;
	private Date creationDate;	
	private User owner;
	private Action baseAction;
	

	public static HashIdentificator setAction(Tema subject, String hashtag, Date creationDate ,User owner, Action baseAction) {
		HashIdentificator a=new HashIdentificator();
		a.subject = subject;
		a.hashtag = hashtag;
		a.creationDate = creationDate;
		a.owner=owner;
		a.baseAction=baseAction;
		return a;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	@OneToOne(targetEntity = User.class)
	public User getOwner() {
		return owner;
	}
	
	public void setOwner(User owner) {
		this.owner = owner;
	}

	public void setId(long id) {
		this.id = id;
	}	
	
	

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@OneToOne(targetEntity = Tema.class)
	public Tema getSubject() {
		return subject;
	}

	public void setSubject(Tema subject) {
		this.subject = subject;
	}
	@Lob
	@Column(length = 10000, name = "hashtag")
	public String getHashtag() {
		return hashtag;
	}

	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
	@OneToOne(targetEntity = Action.class)
	public Action getBaseAction() {
		return baseAction;
	}

	public void setBaseAction(Action baseAction) {
		this.baseAction = baseAction;
	}

	

}