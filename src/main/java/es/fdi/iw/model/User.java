package es.fdi.iw.model;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Jose Luis
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name="allUsers",
            query="select u from User u"),
    @NamedQuery(name="userByLogin",
        query="select u from User u where u.login = :loginParam"),
    @NamedQuery(name="userBySessionTokenId",
    query="select u from User u where u.sessionId = :sessionId"),
    @NamedQuery(name="userById",
    query="select u from User u where u.id = :idParam"),
    @NamedQuery(name="lastUsersRegistered",
    query="select u from User u where u.creationDate >= :dateParam"),
    @NamedQuery(name="delUser",
    	query="delete from User u where u.id= :idParam")
})
@SequenceGenerator(name="seq", initialValue=32, allocationSize=15324)
@GenericGenerator(name="system-uuid", strategy = "uuid")
public class User {	
	
    private static BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();
	
	// do not change these fields - all web applications with user authentication need them
	private long id;
	private String login;
	private String role;
	private String hashedAndSalted;
		
	// change fields below here to suit your application
	
	private String email;
	private String userPicture;
	private String userState;
	private Date creationDate;
	private Date lastLoginDate;
	private String city;
	private String sessionId;
	public User() {}

	public static User createUser(String login, String pass, String role, String email) throws ParseException {
		User u = new User();
		
		
		u.login = login;
		u.hashedAndSalted = generateHashedAndSalted(pass);
		u.role = role;
		u.email= email;
		u.userPicture="not set";
		u.userState="";
		u.creationDate=new Date(new java.util.Date().getTime()+ TimeZone.getDefault().getRawOffset());
		System.out.println(u.creationDate);
		u.lastLoginDate=new Date(new java.util.Date().getTime()+ TimeZone.getDefault().getRawOffset());
		u.city="not set yet";
		return u;
	}
	public static User updateUser(String login, String pass, String role, String email, long id, String userPicture,String state, String creationDate, String lastLogin, String  city, String sessionId) throws ParseException {
		User u = new User();
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		u.id=id;
		u.login = login;
		u.hashedAndSalted = generateHashedAndSalted(pass);
		u.role = role;
		u.email= email;
		u.userPicture=userPicture;
		u.userState=state;
		java.util.Date date = sdf1.parse(creationDate);
		u.creationDate=new Date(date.getTime());
		System.out.println(u.creationDate);
		date = sdf1.parse(lastLogin);
		u.lastLoginDate=new Date(date.getTime());
		u.city=city;
		u.sessionId=sessionId;
		return u;
	}
	public boolean isPassValid(String pass) {
		return bcryptEncoder.matches(pass, hashedAndSalted);		
	}
	
	
	
	
	/**
	 * Generate a hashed&salted hex-string from a user's pass and salt
	 * @param pass to use; no length-limit!
	 * @param salt to use
	 * @return a string to store in the BD that does not reveal the password even
	 * if the DB is compromised. Note that brute-force is possible, but it will
	 * have to be targeted (ie.: use the same salt)
	 */
	public static String generateHashedAndSalted(String pass) {
		/*
		Código viejo: sólo 1 iteración de SHA-1. bCrypt es mucho más seguro (itera 1024 veces...)
		
		Además, bcryptEncoder guarda la sal junto a la contraseña
		byte[] saltBytes = hexStringToByteArray(user.salt);
		byte[] passBytes = pass.getBytes();
		byte[] toHash = new byte[saltBytes.length + passBytes.length];
		System.arraycopy(passBytes, 0, toHash, 0, passBytes.length);
		System.arraycopy(saltBytes, 0, toHash, passBytes.length, saltBytes.length);
		return byteArrayToHexString(sha1hash(toHash));
		*/
		return bcryptEncoder.encode(pass);
	}	

	/**
	 * Converts a byte array to a hex string
	 * @param b converts a byte array to a hex string; nice for storing
	 * @return the corresponding hex string
	 */
	public static String byteArrayToHexString(byte[] b) {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<b.length; i++) {
			sb.append(Integer.toString((b[i]&0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	/**
	 * Converts a hex string to a byte array
	 * @param hex string to convert
	 * @return equivalent byte array
	 */
	public static byte[] hexStringToByteArray(String hex) {
		byte[] r = new byte[hex.length()/2];
		for (int i=0; i<r.length; i++) {
			String h = hex.substring(i*2, (i+1)*2);
			r[i] = (byte)Integer.parseInt(h, 16);
		}
		return r;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id =id;
	}

	@Column(unique=true)
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getHashedAndSalted() {
		return hashedAndSalted;
	}

	public void setHashedAndSalted(String hashedAndSalted) {
		this.hashedAndSalted = hashedAndSalted;
	}



	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserPicture() {
		return userPicture;
	}

	public void setUserPicture(String userPicture) {
		this.userPicture = userPicture;
	}

	public String getUserState() {
		return userState;
	}

	public void setUserState(String userState) {
		this.userState = userState;
	}

	public String getCreationDate() {
		return creationDate.toString();
	}

	public void setCreationDate(String creationDate) throws ParseException {
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = sdf1.parse(creationDate);
		System.out.println(date);
		this.creationDate =new Date(date.getTime());
		
	}

	public String getLastLoginDate() {
		return lastLoginDate.toString();
	}

	public void setLastLoginDateBigFormat(String lastLoginDate) throws ParseException {
		
		this.lastLoginDate =  new Date(new java.util.Date().getTime()) ;
	}
	
	
	public void setLastLoginDate(String lastLoginDate) throws ParseException {
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = sdf1.parse(lastLoginDate);
		this.lastLoginDate =  new Date(date.getTime()) ;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public User toModel(User u){
		User toReturn=u;
		toReturn.setId(0000000);
		
		return toReturn;
		
		
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", login=" + login + ", role=" + role + ", hashedAndSalted=" + hashedAndSalted
				 + ", email=" + email + ", userPicture=" + userPicture + ", userState="
				+ userState + ", creationDate=" + creationDate + ", lastLoginDate=" + lastLoginDate + ", city=" + city
				+ ", sessionId=" + sessionId + "]";
	}
	
	


	
}
