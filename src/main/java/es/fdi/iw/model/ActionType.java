package es.fdi.iw.model;

public enum ActionType {
	STATE("Estado"),
	COMMENT("Comentario"),
	LIKE("Like"),
	POST("Post"),
	TOURNAMENT("TOURNAMENT"),
	TWEET("Tweet");
	
	private String content;
	private ActionType(String content) {
		this.content=content;
	}
	public String getContent() {
		return content;
	}
	public static ActionType fromString(String department) {
		if (department != null) {
			for (ActionType aux : ActionType.values()) {
				if (department.equalsIgnoreCase(aux.content)) {
					return aux;
				}
			}
		}
		return null;
	}
	
}

