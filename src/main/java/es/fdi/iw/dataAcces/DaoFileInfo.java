package es.fdi.iw.dataAcces;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import es.fdi.iw.model.FileInfo;
import es.fdi.iw.model.User;

public class DaoFileInfo {

	public DaoFileInfo(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	public List<FileInfo> getAllFileInfo(){
		return (List<FileInfo>)entityManager.createNamedQuery("allFiles", FileInfo.class).getResultList();
	}
	private EntityManager entityManager;
	@Transactional
	public void createFileInfo(FileInfo doc){
		
		entityManager.persist(doc);
		entityManager.flush();
	}

	public FileInfo getFileInfoById(String id) {
		FileInfo u = (FileInfo)entityManager.createNamedQuery("docById")
				.setParameter("idParam", Long.valueOf(id)).getSingleResult();
		return u;
	}

	public List<FileInfo> getFileInfoByTittle(String tittleParam) {
		List<FileInfo> u = (List<FileInfo>)entityManager.createNamedQuery("filesByTittle",FileInfo.class).setParameter("postTittleReferenceParam", tittleParam).getResultList();
		return u;
	}
	@Transactional
	public void updateFileInfo(FileInfo u){
		entityManager.merge(u);
		entityManager.flush();
	}
	@Transactional
	public void deleteFileInfo(String id){
		entityManager.createNamedQuery("delFileInfo")
		.setParameter("idParam", id).executeUpdate();//== 1) {
//		"Ok: FileInfo " + id + " removed",
//	ResponseEntity<String>("Error: no such FileInfo",HttpStatus.BAD_REQUEST);
	}
}
