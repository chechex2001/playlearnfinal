package es.fdi.iw.dataAcces;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.transaction.annotation.Transactional;

import es.fdi.iw.model.ActionType;
import es.fdi.iw.model.Notification;

public class DaoNotification {

	public DaoNotification(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	public Notification getAllTweetNotification(long id) {
		Notification retList=null;
		
		try{
		retList=entityManager.createNamedQuery("allNotificationsByActionId", Notification.class)
				.setParameter("typeParam",id).getSingleResult();
		
		}catch(Exception e){
			
			System.out.println(e.getMessage());
			
		}
		return retList;
	}
	
	
	public List<Notification>  getAallNotificationsByActionType(ActionType id) {
		List<Notification>  retList=null;
		
		try{
		retList=entityManager.createNamedQuery("allNotificationsByActionType", Notification.class)
				.setParameter("typeParam",id)	.getResultList();
		}catch(Exception e){
			
			System.out.println(e.getMessage());
			
		}
		return retList;
	}
	
	public List<Notification> getAllNotification(String start1, String end1) {
		List<Notification> list = (List<Notification>) entityManager.createNamedQuery("allNotification", Notification.class)				
				.getResultList();
			int start =Integer.valueOf(start1);
			int end=Integer.valueOf(end1);
			int totalCases = list.size();
			if(start==totalCases)
				return new ArrayList<Notification>(); 
			if(end>totalCases)
				end=totalCases;
				list.subList(start, end);
		return list; 
	}

	private EntityManager entityManager;

	@Transactional
	public void createNotification(Notification notification) {

		entityManager.persist(notification);
		entityManager.flush();
	}

	public Notification getNotificationById(String id) {
		Notification u = (Notification) entityManager.createNamedQuery("notificationById")
				.setParameter("idParam", Long.valueOf(id)).getSingleResult();
		return u;
	}


	
	public List<Notification> getNotificationByLogin(String userName) {
		List<Notification> allNotification = (List<Notification>) entityManager
				.createNamedQuery("getNotificationForUser", Notification.class).setParameter("userParam", userName).getResultList();

		return allNotification;
	}

	@Transactional
	public void updateNotification(Notification u) {
		entityManager.merge(u);
		entityManager.flush();
	}

	@Transactional
	public void deleteNotification(String id) {
		entityManager.createNamedQuery("delNotification").setParameter("idParam", id).executeUpdate();// ==						
	}
}
