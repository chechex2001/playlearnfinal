package es.fdi.iw.dataAcces;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.transaction.annotation.Transactional;

import es.fdi.iw.model.Tema;


public class DaoTema {

	public DaoTema(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	
	private EntityManager entityManager;
	@Transactional
	public void createTema(Tema Tema){
		
		entityManager.persist(Tema);
		entityManager.flush();
	}
	public List<Tema> getAllTemas(){
		return (List<Tema>)entityManager.createNamedQuery("allTemas", Tema.class).getResultList();
	}
	public Tema getTemaById(String id) {
		Tema u = (Tema)entityManager.createNamedQuery("temaById")
				.setParameter("idParam", Long.valueOf(id)).getSingleResult();
		return u;
	}
	
	@Transactional
	public void updateTema(Tema u){
		entityManager.merge(u);
		entityManager.flush();
	}
	@Transactional
	public void deleteTema(String id){
		entityManager.createNamedQuery("delTema")
		.setParameter("idParam", id).executeUpdate();
	}
	@Transactional
	public Tema getTemaByAction(String actionParam){
		try{
			List<Tema> u = (List<Tema>)entityManager.createNamedQuery("temaByActionId")
					.setParameter("actionParam",actionParam).getResultList();					
			
			
			
		return u.get(0);
		
		}catch(Exception e){
			System.out.println(e.getMessage());	
			
			return null;
		}
	}
	
}
