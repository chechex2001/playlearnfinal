package es.fdi.iw.dataAcces;
import org.apache.commons.lang.time.DateUtils;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import es.fdi.iw.model.Notification;
import es.fdi.iw.model.User;

public class DaoUser {

	public DaoUser(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	
	private EntityManager entityManager;
	@Transactional
	public void createUser(User user){
		
		entityManager.persist(user);
		entityManager.flush();
	}
	public List<User> getAllUser(){
		return (List<User>)entityManager.createNamedQuery("allUsers", User.class).getResultList();
	}
	public List<User> getAllLastUserRegistered(){
		Date date=DateUtils.addDays(new Date(), -7) ;
		System.out.println("EL DATE QUE BUSCAS"+ new java.sql.Date(date.getTime()).toString());
		return (List<User>)entityManager.createNamedQuery("lastUsersRegistered", User.class).setParameter("dateParam", new java.sql.Date(date.getTime()).toString()).getResultList();
	}
	public User getUserById(long id) {
		User u = (User)entityManager.createNamedQuery("userById")
				.setParameter("idParam", Long.valueOf(id)).getSingleResult();
		return u;
	}

	public User getUserBySessionKeyId(String keyId) throws NoResultException{
		
		User u = (User)entityManager.createNamedQuery("userBySessionTokenId")
				.setParameter("sessionId", keyId).getSingleResult();
		return u;	
		
	
		
	}
	
	public User getUserByLogin(String userName) {
		User u = (User)entityManager.createNamedQuery("userByLogin")
				.setParameter("loginParam", userName).getSingleResult();
		return u;
	}
	@Transactional
	public void updateUser(User u){
		entityManager.merge(u);
		entityManager.flush();
	}
	@Transactional
	public void deleteUser(String id){
		entityManager.createNamedQuery("delUser")
		.setParameter("idParam", id).executeUpdate();//== 1) {

	}
}
