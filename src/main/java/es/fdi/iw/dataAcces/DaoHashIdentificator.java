package es.fdi.iw.dataAcces;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.transaction.annotation.Transactional;

import es.fdi.iw.model.HashIdentificator;

public class DaoHashIdentificator {

	public DaoHashIdentificator(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	public List<HashIdentificator> getTweetByContent(String hashtags) {
		List<HashIdentificator>retList=null;
		List<HashIdentificator>retList3=new ArrayList<HashIdentificator>();
		try{
		retList=(List<HashIdentificator>) entityManager.createNamedQuery("allTweetHashIdentificators", HashIdentificator.class)				
				.setParameter("hasparam", hashtags.toUpperCase())
				.getResultList();
		
		}catch(Exception e){
			
			System.out.println(e.getMessage());
			
		}
		return retList;
	}
	public List<HashIdentificator> getAllHashIdentificator() {
		return (List<HashIdentificator>) entityManager.createNamedQuery("allHashIdentificator", HashIdentificator.class)
				.getResultList();
	}

	private EntityManager entityManager;

	@Transactional
	public void createHashIdentificator(HashIdentificator hashIdentificator) {

		entityManager.persist(hashIdentificator);
		entityManager.flush();
	}

	public HashIdentificator getHashIdentificatorById(String id) {
		HashIdentificator u = (HashIdentificator) entityManager.createNamedQuery("HashIdentificatorById")
				.setParameter("idParam", Long.valueOf(id)).getSingleResult();
		return u;
	}


	
	public List<HashIdentificator> getHashIdentificatorByLogin(String userName) {
		List<HashIdentificator> allHashIdentificator = (List<HashIdentificator>) entityManager
				.createNamedQuery("getHashIdentificatorForUser", HashIdentificator.class).setParameter("userParam", userName).getResultList();

		return allHashIdentificator;
	}

	@Transactional
	public void updateHashIdentificator(HashIdentificator u) {
		entityManager.merge(u);
		entityManager.flush();
	}

	@Transactional
	public void deleteHashIdentificator(String id) {
		entityManager.createNamedQuery("delHashIdentificator").setParameter("idParam", id).executeUpdate();// ==						
	}
}
