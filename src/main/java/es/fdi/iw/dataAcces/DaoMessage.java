package es.fdi.iw.dataAcces;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import es.fdi.iw.model.Action;
import es.fdi.iw.model.DocumentInfo;
import es.fdi.iw.model.MessageEmail;
import es.fdi.iw.model.User;

public class DaoMessage {

	public DaoMessage(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	public List<MessageEmail> getAllMessage() {
		return (List<MessageEmail>) entityManager.createNamedQuery("allMessage", MessageEmail.class)
				.getResultList();
	}

	private EntityManager entityManager;

	@Transactional
	public void createMessage(MessageEmail message) {
		entityManager.persist(message.getEmail());
		entityManager.persist(message);
		entityManager.flush();
	}

	private List<MessageEmail> getMessageByOwnerLogin(String ownerParam) {
		List<MessageEmail> u = (List<MessageEmail>) entityManager.createNamedQuery("messageByOwner" ,MessageEmail.class)
				.setParameter("ownerParam", Long.valueOf(ownerParam)).getResultList();
		return u;
	}

	public List<MessageEmail> getInboxMessage(String ownerParam){
		List<MessageEmail> u = getAllMessage();
		List<MessageEmail> returnList =new ArrayList<MessageEmail>();
		for (MessageEmail message : u) {			
			if(message.isInbox() & message.getToBeSend().equals(ownerParam)){
				returnList.add(message);
			}
		}
		return returnList;
		
//		return  entityManager.createNamedQuery("messageBytoOwner", MessageEmail.class)
//				.setParameter("toOwnerParam", ownerParam)//.setParameter("isInbox", false)
//				.getResultList();
	}

	public List<MessageEmail> getOutboxMessage(String ownerParam){
		List<MessageEmail> u = getAllMessage();
		List<MessageEmail> returnList =new ArrayList<MessageEmail>();
		for (MessageEmail message : u) {
			
			if(message.isInbox() & message.getEmail().getOwner().equals(ownerParam)){
				returnList.add(message);
			}
		}
		
		
		return returnList;
//		return  entityManager.createNamedQuery("messageBytoOwner", MessageEmail.class)
//				.setParameter("toOwnerParam", ownerParam)
//				.getResultList();
	}

	public List<MessageEmail> getMessageByUserId(String userId) {
		User u= (User)entityManager.createNamedQuery("userById")
				.setParameter("idParam", Long.valueOf(userId)).getSingleResult();
		List<MessageEmail> returnList =getMessageByOwnerLogin(u.getLogin());
		
		return returnList;
	}

	@Transactional
	public void updateMessage(MessageEmail u) {
		entityManager.merge(u);
		entityManager.flush();
	}

	@Transactional
	public void deleteMessage(String id) {
		entityManager.createNamedQuery("delMessage").setParameter("idParam", id).executeUpdate();// ==
		
	}
}
