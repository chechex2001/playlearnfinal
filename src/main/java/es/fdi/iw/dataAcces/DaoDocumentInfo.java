package es.fdi.iw.dataAcces;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import es.fdi.iw.model.Action;
import es.fdi.iw.model.DocumentInfo;

public class DaoDocumentInfo {

	public DaoDocumentInfo(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	
	private EntityManager entityManager;
	@Transactional
	public void createDocumentInfo(DocumentInfo doc){
		
		entityManager.persist(doc);
		entityManager.flush();
	}
	public List<DocumentInfo> getAllDocs(){
		return (List<DocumentInfo>)entityManager.createNamedQuery("allDocument", DocumentInfo.class).getResultList();
	}
	public DocumentInfo getDocumentInfoById(String id) {
		DocumentInfo u = (DocumentInfo)entityManager.createNamedQuery("docById")
				.setParameter("idParam", Long.valueOf(id)).getSingleResult();
		return u;
	}

	public List<DocumentInfo> getDocumentInfoByOwner(String ownerParam) {
		List<DocumentInfo> u = (List<DocumentInfo>)entityManager.createNamedQuery("docByOwner",DocumentInfo.class)
				.setParameter("ownerParam", ownerParam).getResultList();
		return u;
	}
	@Transactional
	public void updateDocumentInfo(DocumentInfo u){
		entityManager.merge(u);
		entityManager.flush();
	}
	@Transactional
	public void deleteDocumentInfo(String id){
		entityManager.createNamedQuery("delDocumentInfo")
		.setParameter("idParam", id).executeUpdate();//== 1) {
//		"Ok: DocumentInfo " + id + " removed",
//	ResponseEntity<String>("Error: no such DocumentInfo",HttpStatus.BAD_REQUEST);
	}
}
