package es.fdi.iw.dataAcces;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.fdi.iw.model.Notification;
import es.fdi.iw.model.StatusType;
import es.fdi.iw.model.TournamentStatus;

public class DaoTournament {

	
	private EntityManager entityManager;

	public DaoTournament(EntityManager entityManager){
		super();
		this.entityManager = entityManager;
	}	
	
	@Transactional
	public void createTournamentStatus(TournamentStatus tournamentStatus){		
		
		entityManager.persist(tournamentStatus);
		entityManager.flush();		
	}
	
	
	@Transactional
	public void updateUserPoints(TournamentStatus tournamentStatus){
		entityManager.merge(tournamentStatus);
		entityManager.flush();
	}
	
	@Transactional
	public List<TournamentStatus> getTournamentsAvailablesByStatus(StatusType type){
		List<TournamentStatus>  retList=null;
		
		try{
		retList=entityManager.createNamedQuery("getTournamentsAvailables", TournamentStatus.class)
				.setParameter("status",type)	.getResultList();
		}catch(Exception e){
			
			System.out.println(e.getMessage());
			
		}
		return retList;
		
		
	}
	@Transactional
	public TournamentStatus getTournamentsById(long id){
		
		TournamentStatus u = (TournamentStatus)entityManager.createNamedQuery("getTournamentsById").setParameter("id", id).getSingleResult();
//		entityManager.createNativeQuery("Select * from TournamentStatus where id="+id);
		return u;


		
	}
	@Transactional
	public TournamentStatus getTournamentsByUser(String string,StatusType type){
		
		TournamentStatus u = (TournamentStatus)entityManager.createNamedQuery("getTournamentsByUser").setParameter("user", string).setParameter("status",type).getSingleResult();

		return u;

	}
	@Transactional
	public List<TournamentStatus> getTournamentsByUser2(String id){
		
		
List<TournamentStatus>  retList=null;
		
		try{
		retList=entityManager.createNamedQuery("getTournamentsByUser2", TournamentStatus.class)
				.setParameter("user",id)	.getResultList();
		}catch(Exception e){
			
			System.out.println(e.getMessage());
			
		}
		return retList;
		
	}
    
	@Transactional
	public List<TournamentStatus> getTournamentsByWinner(long id){
		
		List<TournamentStatus>  retList=null;
		
		try{
		retList=entityManager.createNamedQuery("getTournamentsByWinner", TournamentStatus.class)
				.setParameter("user",id)	.getResultList();
		}catch(Exception e){
			
			System.out.println(e.getMessage());
			
		}
		return retList;
		
	}
    
}
