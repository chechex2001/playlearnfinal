package es.fdi.iw.dataAcces;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.transaction.annotation.Transactional;

import es.fdi.iw.model.TemaResponse;


public class DaoResponseTema {

	public DaoResponseTema(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	
	private EntityManager entityManager;
	@Transactional
	public void createTemaResponse(TemaResponse tema){
		
		entityManager.persist(tema);
		entityManager.flush();
	}
	public List<TemaResponse> getAllTemaResponses(){
		return (List<TemaResponse>)entityManager.createNamedQuery("allTemaResponses", TemaResponse.class).getResultList();
	}
	public TemaResponse getTemaResponseById(String id) {
		TemaResponse u = (TemaResponse)entityManager.createNamedQuery("temaResponseById")
				.setParameter("idParam", Long.valueOf(id)).getSingleResult();
		return u;
	}
	
	public TemaResponse getTemaResponseByTemaId(String id) {
		TemaResponse u = (TemaResponse)entityManager.createNamedQuery("allTemaResponsesByTema")
				.setParameter("temaParam", Long.valueOf(id)).getSingleResult();
		return u;
	}
	
	
	
	
	@Transactional
	public void updateTemaResponse(TemaResponse u){
		entityManager.merge(u);
		entityManager.flush();
	}
	@Transactional
	public void deleteTemaResponse(String id){
		entityManager.createNamedQuery("delTemaResponse")
		.setParameter("idParam", id).executeUpdate();
	}

	
}
