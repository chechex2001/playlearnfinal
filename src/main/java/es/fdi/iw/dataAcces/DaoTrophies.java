package es.fdi.iw.dataAcces;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.transaction.annotation.Transactional;

import es.fdi.iw.model.Action;
import es.fdi.iw.model.TrophieType;
import es.fdi.iw.model.Trophies;

public class DaoTrophies {

	public DaoTrophies(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	
	private EntityManager entityManager;
	@Transactional
	public void createTrophies(Trophies Trophies){
		
		entityManager.persist(Trophies);
		entityManager.flush();
	}
	public List<Trophies> getAllTrophiess(){
		return (List<Trophies>)entityManager.createNamedQuery("allTrophies", Trophies.class).getResultList();
	}
	public Trophies getTrophiesById(String id) {
		Trophies u = (Trophies)entityManager.createNamedQuery("trophiesById")
				.setParameter("idParam", Long.valueOf(id)).getSingleResult();
		return u;
	}
	public Trophies getTrophiesById(long id) {
		Trophies u = (Trophies)entityManager.createNamedQuery("trophiesById")
				.setParameter("idParam", id).getSingleResult();
		return u;
	}
	
	public List<Trophies> getTrophiesByPlaceContentType(Action id,TrophieType trophiesType) {
		@SuppressWarnings("unchecked")
		List<Trophies> u = (List<Trophies>)entityManager.createNamedQuery("trophiesByPlace")
				.setParameter("idParam", id)
		.setParameter("TrophieTypeParam",trophiesType).getResultList();
		return u;
	}
	public List<Trophies> getTrophiesByPlaceContentTypeAndOwner(long id,TrophieType trophieTypeParam, String ownerId) {
		@SuppressWarnings("unchecked")
		List<Trophies> u = (List<Trophies>)entityManager.createNamedQuery("trophiesByPlaceTypeOwner")
				.setParameter("idParam", id)
				.setParameter("ownerId", ownerId)
		.setParameter("TrophieTypeParam",trophieTypeParam).getResultList();
		return u;
	}
	
	
	@Transactional
	public void updateTrophies(Trophies u){
		entityManager.merge(u);
		entityManager.flush();
	}
	@Transactional
	public void deleteTrophies(String id){
		entityManager.createNamedQuery("delTrophies")
		.setParameter("idParam", id).executeUpdate();
	}
	public List<Trophies> getAllMedals() {
		return  entityManager.createNamedQuery("getLikesComents", Trophies.class)
				.setParameter("actionParam", TrophieType.MEDALLA)
				.getResultList();
		
	}
	public List<Trophies> getAllTrophies() {
		return  entityManager.createNamedQuery("getLikesComents", Trophies.class)
				.setParameter("actionParam", TrophieType.TROFEO)
				.getResultList();
		
	}
	public List<Trophies> getAllAwards() {
		return  entityManager.createNamedQuery("getLikesComents", Trophies.class)
				.setParameter("actionParam", TrophieType.LOGRO)
				.getResultList();
		
	}
	

	
	public int getAllMyMedals(String userName) {

		return  entityManager.createNamedQuery("getByOwnerTrophiesType", Trophies.class)
				.setParameter("actionParam", TrophieType.MEDALLA)
				.setParameter("ownerParam", userName)
				.getResultList().size();
	}
	
	public int getAllMyAwards(String userName) {

		return  entityManager.createNamedQuery("getByOwnerTrophiesType", Trophies.class)
				.setParameter("actionParam", TrophieType.LOGRO)
				.setParameter("ownerParam", userName)
				.getResultList().size();
	}
	
	public int getAllMyTrophies(String userName) {

		return  entityManager.createNamedQuery("getByOwnerTrophiesType", Trophies.class)
				.setParameter("actionParam", TrophieType.TROFEO)
				.setParameter("ownerParam", userName)
				.getResultList().size();
	}
	
}
