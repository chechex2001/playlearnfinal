package es.fdi.iw.dataAcces;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.transaction.annotation.Transactional;

import es.fdi.iw.model.Action;
import es.fdi.iw.model.ActionType;
import es.fdi.iw.model.Stadistics;
import es.fdi.iw.model.StatusType;
import es.fdi.iw.model.User;

public class DaoAction {

	public DaoAction(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	
	private EntityManager entityManager;
	@Transactional
	public void createAction(Action Action){
		
		entityManager.persist(Action);
		entityManager.flush();
	}
	public List<Action> getAllActions(){
		return (List<Action>)entityManager.createNamedQuery("allAction", Action.class).getResultList();
	}
	public Action getActionById(String id) {
		Action u = (Action)entityManager.createNamedQuery("actionById")
				.setParameter("idParam", Long.valueOf(id)).getSingleResult();
		return u;
	}
	public Action getActionById(long id) {
		Action u = (Action)entityManager.createNamedQuery("actionById")
				.setParameter("idParam", id).getSingleResult();
		return u;
	}
	
	public List<Action> getActionByPlaceContentType(long id,ActionType likeOrCommentOrTweet) {
		@SuppressWarnings("unchecked")
		List<Action> u = (List<Action>)entityManager.createNamedQuery("actionByPlace")
				.setParameter("idParam", id)
		.setParameter("actionTypeParam",likeOrCommentOrTweet).getResultList();
		return u;
	}
	public List<Action> getActionByPlaceContentTypeAndOwner(long id,ActionType likeOrCommentOrTweet, String ownerId) {
		@SuppressWarnings("unchecked")
		List<Action> u = (List<Action>)entityManager.createNamedQuery("actionByPlaceTypeOwner")
				.setParameter("idParam", id)
				.setParameter("ownerId", ownerId)
		.setParameter("actionTypeParam",likeOrCommentOrTweet).getResultList();
		return u;
	}
	
	public Action getActionByLogin(String ownerName) {
		Action u = (Action)entityManager.createNamedQuery("actionByTittle")
				.setParameter("ownerParam", ownerName).getSingleResult();
		return u;
	}
	@Transactional
	public void updateAction(Action u){
		entityManager.merge(u);
		entityManager.flush();
	}
	@Transactional
	public void deleteAction(String id){
		entityManager.createNamedQuery("delAction")
		.setParameter("idParam", id).executeUpdate();
	}
	public List<Action> getAllLikes() {
		return  entityManager.createNamedQuery("getLikesComents", Action.class)
				.setParameter("actionParam", ActionType.LIKE)
				.getResultList();
		
	}
	
	public List<Action> getAllTweets() {
		return  entityManager.createNamedQuery("getLikesComents", Action.class)
				.setParameter("actionParam", ActionType.TWEET)
				.getResultList();
		
	}
	public int getAllMyLikesCount(String userName) {

		return  entityManager.createNamedQuery("getByOwnerLikesComents", Action.class)
				.setParameter("actionParam", ActionType.LIKE)
				.setParameter("ownerParam", userName)
				.getResultList().size();
	}

	
	public int getAllMyTweetsCount(String userName) {

		return  entityManager.createNamedQuery("getByOwnerLikesComents", Action.class)
				.setParameter("actionParam", ActionType.TWEET)
				.setParameter("ownerParam", userName)
				.getResultList().size();
	}
	
	public int getAllMyTweetsStates(String userName) {

		return  entityManager.createNamedQuery("getByOwnerLikesComents", Action.class)
				.setParameter("actionParam", ActionType.STATE)
				.setParameter("ownerParam", userName)
				.getResultList().size();
	}
	
	public int getAllMyStatesCount(String userName) {

		return  entityManager.createNamedQuery("getByOwnerLikesComents", Action.class)
				.setParameter("actionParam", ActionType.STATE)
				.setParameter("ownerParam", userName)
				.getResultList().size();
	}
	public int getAllPosts() {

		return  entityManager.createNamedQuery("getLikesComents", Action.class)
				.setParameter("actionParam", ActionType.POST)
				
				.getResultList().size();
	}
	
	public int getAllMyComments(String userName) {
		return  entityManager.createNamedQuery("getByOwnerLikesComents", Action.class)
				.setParameter("actionParam", ActionType.COMMENT)
				.setParameter("ownerParam", userName)
				.getResultList().size();
	}
	

	public List<Action> getAllComments() {
		return  entityManager.createNamedQuery("getLikesComents", Action.class)
				.setParameter("actionParam", ActionType.COMMENT)
				.getResultList();
	}
	public int getAllMyTournaments(String userName) {

		return  entityManager.createNamedQuery("getByOwnerLikesComents", Action.class)
				.setParameter("actionParam", ActionType.TOURNAMENT)
				.setParameter("ownerParam", userName)
				.getResultList().size();
		}
	public Stadistics getUserStadistics(User u){
		Stadistics stats = new Stadistics();
		DaoFactory dataConector= new DaoFactory(entityManager);
		int numberLikes= getAllMyLikesCount(u.getLogin());
		int numberComments= getAllMyComments(u.getLogin());		
		int numberTweets=getAllMyTweetsCount(u.getLogin());
		int numberOfTournaments=getAllMyTournaments(u.getLogin());
		//
		int numberCommentsGeneral= getAllComments().size();
		int numberTweetsGeneral=getAllTweets().size();
		int numberLikesGeneral=getAllLikes().size();
		
		stats.setNumberOfLikes(numberLikes);
		stats.setNumberOfComments(numberComments);
		stats.setNumberOfTweets(numberTweets);
		int tournamentSize=dataConector.getDaoTournamentInfo().getTournamentsAvailablesByStatus(StatusType.ENDED).size();
		if(tournamentSize>0)
		stats.setProgress((numberComments+numberOfTournaments)*100/(numberTweetsGeneral+tournamentSize));
		if(numberTweetsGeneral>0){
		stats.setMicroblogging(getAllMyTweetsCount(u.getLogin())*100/numberTweetsGeneral);
		stats.setActivities((numberTweets+numberComments+numberLikes)*100/(numberTweetsGeneral+numberCommentsGeneral+numberLikesGeneral));
		}
		else{
			
			stats.setMicroblogging(0);
			stats.setActivities(0);
			
		}
		return stats;
	
	}
}
