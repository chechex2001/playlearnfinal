package es.fdi.iw.dataAcces;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

public class DaoFactory {

	public DaoFactory(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}


	private EntityManager entityManager;
	
	public DaoAction getDaoAction(){
		return new DaoAction(this.entityManager);
	}
	
	public DaoTrophies getDaoTropie(){
		return new DaoTrophies(this.entityManager);
	}
	
	
	public DaoDocumentInfo getDaoDocumentInfo(){
		return new DaoDocumentInfo(this.entityManager);
	}
	
	public DaoFileInfo getDaoFileInfo(){
		return new DaoFileInfo(this.entityManager);
	}
	
	public DaoNotification getDaoNotification(){
		return new DaoNotification(this.entityManager);
	}
	
	public DaoUser getDaoUser(){
		return new DaoUser(this.entityManager);
	}
	
	public DaoMessage getDaoMessage(){
		return new DaoMessage(this.entityManager);
	}
	
	public DaoTema getDaoTema(){
		return new DaoTema (this.entityManager);
		
	}
	
	public DaoResponseTema getDaoResponseTema(){
		return new DaoResponseTema(this.entityManager);
	}
	public DaoHashIdentificator getDaoHashIdentificator(){
	return new DaoHashIdentificator(this.entityManager);
	}
	
	public DaoTournament getDaoTournamentInfo(){
		return new DaoTournament(this.entityManager);	
	}
	public DaoPreguntas getDaoPreguntas(){
		return new DaoPreguntas(this.entityManager);	
	}
	
	
}
