<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib  uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e" %>
<!DOCTYPE html >
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PlayLearn Login second option</title>

    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="resources/css/animate.css" rel="stylesheet">
    <link href="resources/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">

            <div class="col-md-6">
            <img src="resources/assets/img/brand-white.png" alt="brand">
                <h2 class="font-bold">Bienvenido </h2>

                <p>
                   PlayLearn Es una red social de aprendizaje.
                </p>

                <p>
                    Con el fin de interactuar sobre documentos compartidos por vosotros,
					acerca de temas propuestos 
                </p>

                <p>
                    Se les puntuara la participación en ella con el fin de ser el mas inteligente de esta aplicacion a nivel mundial
                </p>

                <p>
                    <small>estos datos seran luego evaluados por expertos en e-learning.</small>
                </p>

            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <form class="m-t" role="form" action="/login" method="POST">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Username" name="userName" id="userName" required="">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password"  name="pass" id="pass" required="">
                            <input type="hidden" class="form-control"   name="source"  value="login2" required="">
                        </div>
                        <button type="submit" id="log" class="btn btn-primary block full-width m-b">Login</button>

                        <a href="#">
                            <small>Forgot password?</small>
                        </a>

                        <p class="text-muted text-center">
                            <small>No tienes cuenta?</small>
                        </p>
                        <a class="btn btn-sm btn-white btn-block" href="/register">Crear cuenta</a>
                    </form>
                    <p class="m-t">
                        <small>Esta comunidad cuenta contigo! </small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright PlayLearn Company
            </div>
            <div class="col-md-6 text-right">
               <small>2015-2016</small>
            </div>
        </div>
    </div>

</body>



</html>
