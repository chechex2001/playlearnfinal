<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib
	uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project"
	prefix="e"%>
<!DOCTYPE html>
<html>



<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Pragma" content="no-cache">

<title>PlayLearn | Profile</title>

<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/font-awesome/css/font-awesome.css"
	rel="stylesheet">
<link href="resources/css/animate.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">
<link href="resources/css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="resources/css/plugins/dropzone/dropzone.css"
	rel="stylesheet">
<link href="resources/css/plugins/cropper/cropper.min.css"
	rel="stylesheet">

<link href="resources/css/plugins/summernote/summernote.css"
	rel="stylesheet">
<link href="resources/css/plugins/summernote/summernote-bs3.css"
	rel="stylesheet">
</head>

<body>

	<div id="wrapper">

		<nav class="navbar-default navbar-static-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav metismenu" id="side-menu">
					<li class="nav-header">
						<div class="dropdown profile-element">
							<span> <img alt="image" class="img-circle" id="usrImgSmall"
								src=""
								style="width: 33%;" />
							</span> <a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<span class="clear"> <span class="block m-t-xs"> <strong
										class="font-bold"><label id="usrName"></label></strong>
								</span> <span class="text-muted text-xs block"><label id="usrRole"></label><b
										class="caret"></b></span>
							</span>
							</a>

							<ul class="dropdown-menu animated fadeInRight m-t-xs">
								<li><a id="editProfileButton">Editar perfil</a></li>
								<li><a id="trophies">Ver Trofeos</a></li>
								<li><a id="usersRankButton">Usuarios</a></li>
								<li><a id="messageShowButton">Mensajes</a></li>
								
								<li class="divider"></li>
								<li><button href="/logout">Logout</button></li>
							</ul>
						</div>
						<div class="logo-element">PL+</div>
					</li>
					<li><a id="homeShow"><i class="fa fa-th-large"></i> <span
							class="nav-label">Home</span> <span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<li><a id="editProfileShow"><i class="fa fa-magic"></i>Editar
									Perfil</a></li>
<li><a id="trophies"><i class="fa fa-trophy fa-1x"></i>Ver Trofeos</a></li>
						</ul></li>
<!-- 					<li><a id="socialShow"><i class="fa fa-diamond"></i> <span -->
<!-- 							class="nav-label">Social</span></a></li> -->
					<li><a><i class="fa fa-bar-chart-o"></i> <span
							class="nav-label" id="socialShow">Subir un tweet</span></a></li>
					<li><a id="messageShow"><i class="fa fa-envelope"></i> <span
							class="nav-label">Mensajes </span> <span
							class="label label-warning pull-right" id="inboxNumber"></span> </a></li>
					<li><a id="workspaces"><i class="fa fa-envelope"></i> <span
							class="nav-label">Grupo de trabajo</span> <span
							class="label label-warning pull-right" id="inboxNumber"></span> </a></li>
					<li>
						<a id="tournamentShowButton"><i class="fa fa-trophy fa-1x"></i>
						 	<span	class="nav-label">Torneos
						 	</span>
						 </a>
					</li>
					<li><a id="rankShow"><i class="fa fa-pie-chart"></i> <span
							class="nav-label">Ranking usuarios</span> </a></li>
					<li><a id="adminShow"><i class="fa fa-pie-chart"></i> <span
							class="nav-label">Administracion</span> </a></li>
				</ul>

			</div>
		</nav>
		<%-- user login status + logout / login box --%>




		<div id="page-wrapper" class="gray-bg">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" role="navigation"
					style="margin-bottom: 0">
					<div class="navbar-header">
						<a class="navbar-minimalize minimalize-styl-2 btn btn-primary "
							href="#"><i class="fa fa-bars"></i> </a>
						<form role="search" class="navbar-form-custom" action="">
							<div class="form-group">
								
							</div>
						</form>
					</div>
					<ul class="nav navbar-top-links navbar-right">
						<li><span class="m-r-sm text-muted welcome-message">Welcome
								to PlayLearn<strong> <label id="usrName1"></label>. </strong>
						</span></li>
						<li class="dropdown"><a class="dropdown-toggle count-info"
							data-toggle="dropdown" href="#"> <i class="fa fa-envelope"></i>
								<span class="label label-warning" id="inboxNumber2"></span>
						</a>
							
						


						<li><button  id="closeSession" onClick="eraseCookie('userID')" href="/logout"><i class="fa fa-sign-out"></i> Log
								out
						</button></li>
					</ul>

				</nav>
			</div>
			
			<div class="">
				<div class="row animated fadeInRight" id="stage0">
					<div class="col-md-4">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Profile Detail</h5>
<!-- 								//TODO -->
							</div>
							<div>
								<div class="ibox-content no-padding border-left-right">
									<img alt="image" class="img-responsive" id="usrImgBig"
										src="">
								</div>
								<div class="ibox-content profile-content">
									<h4>
										<strong><label id="usrName2"></label></strong>
									</h4>
									<p>
										<i class="fa fa-map-marker"></i><label id="usrCity"></label> 
									</p>
									<h5>About me</h5>
									<div class="col-md4 " id="normalPost">	
								<form class="m-t" role="form" 
										id="stateChange" >

										<div class="input-group">
											<input type="hidden" class="form-control" name="id" id="postUsrId"
												value="" required> <input
												type="text" class="form-control" name="state"  id="contentState"required>
											<span class="input-group-btn">
												<button type="button" onClick="doPost()" id="sendChangeState" class="btn btn-primary">Publicar!
												
												</button>
											</span>
											
										</div>
										<div class="input-group">
										<input id="the-file" name="file" type="file" >
											 
										</div>

									</form>
								</div></br>
								<div class="col-md-6 ibox float-e-margins"> 
				
                                    <div>
                                        <span>Progreso</span>
                                        <small class="pull-right"></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 60%;" id="progressBar" class="progress-bar"></div>
                                    </div>

                                    <div>
                                        <span>Microblogging</span>
                                        <small class="pull-right"></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 50%;"  id="tweetsBar" class="progress-bar"></div>
                                    </div>

                                    <div>
                                        <span>Actividad</span>
                                        <small class="pull-right"></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 40%;" id="activitiesBar" class="progress-bar"></div>
                                    </div>                                 
                        
					</div>
									<p><label id="usrState"></label></p>
									<div class="row m-t-lg">
										<div class="col-md-4">
											<span class="bar">5,3,9,6,5,9,7,3,5,2</span>
											<h5>
												<label id="commentsCount"><strong>169</strong></label> Comments
											</h5>
										</div>
										<div class="col-md-4">
											<span class="line">5,3,9,6,5,9,7,3,5,2</span>
											<h5>
												<label id="likesCount"><strong>28</strong> </label> Likes
											</h5>
										</div>


									</div>
									<div class="user-button">
										<div class="row">
											<div class="col-md-6">
												<button type="button"
													class="btn btn-primary btn-sm btn-block" id="createMessageHome">
													<i class="fa fa-envelope"></i> Enviar Mensaje
												</button>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Activites</h5>
								<div class="ibox-tools">
									<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
									</a> <a class="dropdown-toggle" data-toggle="dropdown" href="#">
										<i class="fa fa-wrench"></i>
									</a>
									<ul class="dropdown-menu dropdown-user">
										<li><a href="#">Config option 1</a></li>
										<li><a href="#">Config option 2</a></li>
									</ul>
									<a class="close-link"> <i class="fa fa-times"></i>
									</a>
								</div>
							</div>
							<div class="ibox-content">

								<div>
									<div class="feed-activity-list" id="notification4me"></div>

									<button class="btn btn-primary btn-block m">
										<i class="fa fa-arrow-down"></i> Show More
									</button>

								</div>

							</div>
						</div>

					</div>
					
					<div class="col-lg-3 ibox float-e-margins">
                <div class="widget style1">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <i class="fa fa-trophy fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> Today income </span>
                                <h2 class="font-bold">$ 4,232</h2>
                            </div>
                        </div>
                </div>
            </div>
				</div>
			</div>

			<!--        TODO:social -->

			
				<div class=" animated fadeInRight" id="stage1">
					<div class="row">

						<div class="col-lg-12">
						
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h3>Social feeds</h3>
							</div>
										<div class="ibox-content " style=" padding: 15px 20px 271px 20px;">
								<div class="col-xs-12 col-xl-12" id="normalPost">	
									<form class="m-t" role="form" 
										id="stateChange" 
										>

										<div class="input-group">
											<input type="hidden" class="form-control" name="id" id="postUsrId"
												value="" required> <input id="contentTweet"
												type="text" class="form-control" name="state" required>
											<span class="input-group-btn">
												<button type="button" onClick="doPost()" id="publicarTweet2" class="btn btn-primary">Publicar!
												</button>
											</span>
											
										</div>
										<div class="input-group">
										
											<input id="the-file" name="file" type="file" > 
										</div>

									</form>
									<div id="socialFeedRow">				

							</div>
								</div>


							
						</div>

						</div>
					</div>
					</div>
				</div>
			
			
			
			<div class=" animated fadeInRight" id="temaLoad">
					<div class="row">

						<div class="col-lg-12">
						
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h3 id="temaTittle"><button class="btn btn-primary btn-sm demo1" id="reloadWorkgroups">Volver a grupos</button></h3>
							</div>
							<div class="ibox-content " style=" padding: 15px 20px 271px 20px;">
								<div class="col-xs-12 col-xl-12" id="normalPost">	
									
									<div id="socialFeedTemaRow">				
									</div>
								</div>


							
						</div>

						</div>
					</div>
					</div>
				</div>
			
			
			
			
			
			
<div class=' animated fadeIn' id='stageStadistic'>
				<div class='row'>
					<div class='col-lg-12'>
						<div class='ibox float-e-margins'>
							<div class='ibox-title'>
								<h5>Estadisticas de los usuarios</h5>
								<div class='ibox-tools'>
									<a class='collapse-link'> <i class='fa fa-chevron-up'></i>
									</a> <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
										<i class='fa fa-wrench'></i>
									</a> <a class='close-link'> <i class='fa fa-times'></i>
									</a>
								</div>
							</div>
							<div class='ibox-content'></div>


							<div class="row">
								<div class="col-lg-2">
									<div class="ibox float-e-margins">
										<div class="ibox-title">
											<span class="label label-success pull-right">Mensual</span>
											<h5>Registros</h5>
										</div>
										<div class="ibox-content">
											<h1 class="no-margins">
												<label id=newRegister></label>
											</h1>

											<small>Total nuevos</small>
										</div>
									</div>
								</div>
								<div class="col-lg-2">
									<div class="ibox float-e-margins">
										<div class="ibox-title">
											<span class="label label-info pull-right">Mensual</span>
											<h5>Likes</h5>
										</div>
										<div class="ibox-content">
											<h1 class="no-margins">
												<label id=newLikes></label>
											</h1>

											<small>Total nuevos</small>
										</div>
									</div>
								</div>

								<div class="col-lg-4">
									<div class="ibox float-e-margins">
										<div class="ibox-title">
											<span class="label label-primary pull-right">Mensual</span>
											<h5>Comentarios</h5>
										</div>
										<div class="ibox-content">

											<div class="row">
												<div class="col-md-6">
													<h1 class="no-margins">
														<label id=totalCommentsPlaces></label>
													</h1>

												</div>

											</div>


										</div>
									</div>
								</div>
							</div>
							<div class="row">

								<div class="col-lg-4">
									<div class="ibox float-e-margins">
										<div class="ibox-title">
											<span class="label label-warning pull-right">Data has
												changed</span>
											<h5>User activity</h5>
										</div>
										<div class="ibox-content">
											<div class="row">
												<div class="col-xs-4">
													<small class="stats-label">Points / MicroBlogins
														Places</small>
													<h4><label id="totalMicroBloginPlaces"></label></h4>
												</div>

												<div class="col-xs-4">
													<small class="stats-label">% New Points</small>
													<h4><label id="microBloginPorcent">%</label></h4>
												</div>
												
											</div>
										</div>
										<div class="ibox-content">
											<div class="row">
												<div class="col-xs-4">
													<small class="stats-label">Likes / Places to likes</small>
													<h4><label id="totalLikesPlaces"></label></h4>
												</div>

												<div class="col-xs-4">
													<small class="stats-label">% New Likes</small>
													<h4><label id="likesPorcent">%</label></h4>
												</div>
												
											</div>
										</div>
										<div class="ibox-content">
											<div class="row">
												<div class="col-xs-4">
													<small class="stats-label">Coments / Places to
														comment</small>
													<h4><label id="totalCommentsPlaces"></label></h4>
												</div>

												<div class="col-xs-4">
													<small class="stats-label">% New Comments</small>
													<h4><label id="commentsPorcent">%</label></h4>
												</div>
											
											</div>
										</div>
									</div>
								</div>
<!-- 								<div class="col-lg-8"> -->
<!-- 									<div class="ibox"> -->



<!-- 										<div class="ibox-content"> -->

											
<!-- 											<div class="table-responsive"> -->
<!-- 												<table class="table table-striped" id="usersData"> -->

<!-- 													<tbody> -->
<!-- 														<tr> -->
<!-- 															<td>1</td> -->
<!-- 															<td>Master project</td> -->
<!-- 															<td>Patrick Smith</td> -->
<!-- 															<td>$892,074</td> -->
<!-- 															<td>Inceptos Hymenaeos Ltd</td> -->
<!-- 															<td><strong>20%</strong></td> -->
<!-- 															<td>Jul 14, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>2</td> -->
<!-- 															<td>Alpha project</td> -->
<!-- 															<td>Alice Jackson</td> -->
<!-- 															<td>$963,486</td> -->
<!-- 															<td>Nec Euismod In Company</td> -->
<!-- 															<td><strong>40%</strong></td> -->
<!-- 															<td>Jul 16, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>3</td> -->
<!-- 															<td>Betha project</td> -->
<!-- 															<td>John Smith</td> -->
<!-- 															<td>$996,824</td> -->
<!-- 															<td>Erat Volutpat</td> -->
<!-- 															<td><strong>75%</strong></td> -->
<!-- 															<td>Jul 18, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>4</td> -->
<!-- 															<td>Gamma project</td> -->
<!-- 															<td>Anna Jordan</td> -->
<!-- 															<td>$105,192</td> -->
<!-- 															<td>Tellus Ltd</td> -->
<!-- 															<td><strong>18%</strong></td> -->
<!-- 															<td>Jul 22, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>2</td> -->
<!-- 															<td>Alpha project</td> -->
<!-- 															<td>Alice Jackson</td> -->
<!-- 															<td>$674,803</td> -->
<!-- 															<td>Nec Euismod In Company</td> -->
<!-- 															<td><strong>40%</strong></td> -->
<!-- 															<td>Jul 16, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>1</td> -->
<!-- 															<td>Master project</td> -->
<!-- 															<td>Patrick Smith</td> -->
<!-- 															<td>$174,729</td> -->
<!-- 															<td>Inceptos Hymenaeos Ltd</td> -->
<!-- 															<td><strong>20%</strong></td> -->
<!-- 															<td>Jul 14, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>4</td> -->
<!-- 															<td>Gamma project</td> -->
<!-- 															<td>Anna Jordan</td> -->
<!-- 															<td>$823,198</td> -->
<!-- 															<td>Tellus Ltd</td> -->
<!-- 															<td><strong>18%</strong></td> -->
<!-- 															<td>Jul 22, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>1</td> -->
<!-- 															<td>Project <small>This is example of -->
<!-- 																	project</small></td> -->
<!-- 															<td>Patrick Smith</td> -->
<!-- 															<td>$778,696</td> -->
<!-- 															<td>Inceptos Hymenaeos Ltd</td> -->
<!-- 															<td><strong>20%</strong></td> -->
<!-- 															<td>Jul 14, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>2</td> -->
<!-- 															<td>Alpha project</td> -->
<!-- 															<td>Alice Jackson</td> -->
<!-- 															<td>$861,063</td> -->
<!-- 															<td>Nec Euismod In Company</td> -->
<!-- 															<td><strong>40%</strong></td> -->
<!-- 															<td>Jul 16, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>3</td> -->
<!-- 															<td>Betha project</td> -->
<!-- 															<td>John Smith</td> -->
<!-- 															<td>$109,125</td> -->
<!-- 															<td>Erat Volutpat</td> -->
<!-- 															<td><strong>75%</strong></td> -->
<!-- 															<td>Jul 18, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>4</td> -->
<!-- 															<td>Gamma project</td> -->
<!-- 															<td>Anna Jordan</td> -->
<!-- 															<td>$600,978</td> -->
<!-- 															<td>Tellus Ltd</td> -->
<!-- 															<td><strong>18%</strong></td> -->
<!-- 															<td>Jul 22, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>2</td> -->
<!-- 															<td>Alpha project</td> -->
<!-- 															<td>Alice Jackson</td> -->
<!-- 															<td>$150,161</td> -->
<!-- 															<td>Nec Euismod In Company</td> -->
<!-- 															<td><strong>40%</strong></td> -->
<!-- 															<td>Jul 16, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>1</td> -->
<!-- 															<td>Project <small>This is example of -->
<!-- 																	project</small></td> -->
<!-- 															<td>Patrick Smith</td> -->
<!-- 															<td>$160,586</td> -->
<!-- 															<td>Inceptos Hymenaeos Ltd</td> -->
<!-- 															<td><strong>20%</strong></td> -->
<!-- 															<td>Jul 14, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<td>4</td> -->
<!-- 															<td>Gamma project</td> -->
<!-- 															<td>Anna Jordan</td> -->
<!-- 															<td>$110,612</td> -->
<!-- 															<td>Tellus Ltd</td> -->
<!-- 															<td><strong>18%</strong></td> -->
<!-- 															<td>Jul 22, 2015</td> -->
<!-- 															<td><a href="#"><i class="fa fa-check text-navy"></i></a></td> -->
<!-- 														</tr> -->
<!-- 													</tbody> -->
<!-- 												</table> -->
<!-- 											</div> -->

<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</div> -->
							</div>
							<div class="row"><</div>
						</div>
					</div>
				</div>
			</div>

			<div class=' animated fadeIn' id='stage2'>
				<div class='row'>
					<div class='col-lg-12'>
						<div class='ibox float-e-margins'>
							<div class='ibox-title'>
								<h5>Mensajes Recibidos</h5>
								<div class='ibox-tools'>
									<a class='collapse-link'> <i class='fa fa-chevron-up'></i>
									</a> <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
										<i class='fa fa-wrench'></i>
									</a> <a class='close-link'> <i class='fa fa-times'></i>
									</a>
								</div>
							</div>
							<div class='ibox-content'>
								<div class="wrapper wrapper-content">
									<div class="row">
										<div class="col-lg-3">
											<div class="ibox float-e-margins">
												<div class="ibox-content mailbox-content">
													<div class="file-manager">
														<a class="btn btn-block btn-primary compose-mail"
															id="createMail">Compose Mail</a>
														<div class="space-25"></div>
														<h5>Folders</h5>
														<ul class="folder-list m-b-md" style="padding: 0">
															<li> <i class="fa fa-inbox "></i>
																	Inbox <span class="label label-warning pull-right"
																	id="inboxNumber3">16</span>
															</li>
															
														</ul>

														<div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
										<div class='col-lg-9 animated fadeInRight' id="mainMail"></div>
										<div class="col-lg-9 animated fadeInRight" id="mailInbox">
											<div class="mail-box-header">

												<form method="get" action="" class="pull-right mail-search">
													<div class="input-group">
														<input type="text" class="form-control input-sm"
															name="search" placeholder="Search email">
														<div class="input-group-btn">
															<button type="submit" class="btn btn-sm btn-primary">
																Search</button>
														</div>
													</div>
												</form>
												<h2>Inbox (16)</h2>
												<div class="mail-tools tooltip-demo m-t-md">
													<div class="btn-group pull-right">
														<button class="btn btn-white btn-sm">
															<i class="fa fa-arrow-left"></i>
														</button>
														<button class="btn btn-white btn-sm">
															<i class="fa fa-arrow-right"></i>
														</button>

													</div>
													<button class="btn btn-white btn-sm" data-toggle="tooltip"
														data-placement="left" title="Refresh inbox" onclick="CALLERAJAX.callUserInbox();">
														<i class="fa fa-refresh"></i> Refresh
													</button>
													<button class="btn btn-white btn-sm" data-toggle="tooltip"
														data-placement="top" title="Mark as read">
														<i class="fa fa-eye"></i>
													</button>
													<button class="btn btn-white btn-sm" data-toggle="tooltip"
														data-placement="top" title="Mark as important">
														<i class="fa fa-exclamation"></i>
													</button>
													<button class="btn btn-white btn-sm" data-toggle="tooltip"
														data-placement="top" title="Move to trash">
														<i class="fa fa-trash-o"></i>
													</button>

												</div>
											</div>
											<div class="mail-box">

												<table class="table table-hover table-mail"
													id="messageInboxs">
													<tbody>
														<tr class="unread">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Anna
																	Smith</a></td>
															<td class="mail-subject"><a href="mail_detail.html">Lorem
																	ipsum dolor noretek imit set.</a></td>
															<td class=""><i class="fa fa-paperclip"></i></td>
															<td class="text-right mail-date">6.10 AM</td>
														</tr>
														<tr class="unread">
															<td class="check-mail">
																<div class="icheckbox_square-green checked"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks" checked=""
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Jack
																	Nowak</a></td>
															<td class="mail-subject"><a href="mail_detail.html">Aldus
																	PageMaker including versions of Lorem Ipsum.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">8.22 PM</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Facebook</a>
																<span class="label label-warning pull-right">Clients</span>
															</td>
															<td class="mail-subject"><a href="mail_detail.html">Many
																	desktop publishing packages and web page editors.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">Jan 16</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Mailchip</a></td>
															<td class="mail-subject"><a href="mail_detail.html">There
																	are many variations of passages of Lorem Ipsum.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">Mar 22</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Alex
																	T.</a> <span class="label label-danger pull-right">Documents</span></td>
															<td class="mail-subject"><a href="mail_detail.html">Lorem
																	ipsum dolor noretek imit set.</a></td>
															<td class=""><i class="fa fa-paperclip"></i></td>
															<td class="text-right mail-date">December 22</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Monica
																	Ryther</a></td>
															<td class="mail-subject"><a href="mail_detail.html">The
																	standard chunk of Lorem Ipsum used.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">Jun 12</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Sandra
																	Derick</a></td>
															<td class="mail-subject"><a href="mail_detail.html">Contrary
																	to popular belief.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">May 28</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Patrick
																	Pertners</a> <span class="label label-info pull-right">Adv</span></td>
															<td class="mail-subject"><a href="mail_detail.html">If
																	you are going to use a passage of Lorem </a></td>
															<td class=""></td>
															<td class="text-right mail-date">May 28</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Michael
																	Fox</a></td>
															<td class="mail-subject"><a href="mail_detail.html">Humour,
																	or non-characteristic words etc.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">Dec 9</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Damien
																	Ritz</a></td>
															<td class="mail-subject"><a href="mail_detail.html">Oor
																	Lorem Ipsum is that it has a more-or-less normal.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">Jun 11</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Anna
																	Smith</a></td>
															<td class="mail-subject"><a href="mail_detail.html">Lorem
																	ipsum dolor noretek imit set.</a></td>
															<td class=""><i class="fa fa-paperclip"></i></td>
															<td class="text-right mail-date">6.10 AM</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Jack
																	Nowak</a></td>
															<td class="mail-subject"><a href="mail_detail.html">Aldus
																	PageMaker including versions of Lorem Ipsum.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">8.22 PM</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Mailchip</a></td>
															<td class="mail-subject"><a href="mail_detail.html">There
																	are many variations of passages of Lorem Ipsum.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">Mar 22</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Alex
																	T.</a> <span class="label label-warning pull-right">Clients</span></td>
															<td class="mail-subject"><a href="mail_detail.html">Lorem
																	ipsum dolor noretek imit set.</a></td>
															<td class=""><i class="fa fa-paperclip"></i></td>
															<td class="text-right mail-date">December 22</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Monica
																	Ryther</a></td>
															<td class="mail-subject"><a href="mail_detail.html">The
																	standard chunk of Lorem Ipsum used.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">Jun 12</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Sandra
																	Derick</a></td>
															<td class="mail-subject"><a href="mail_detail.html">Contrary
																	to popular belief.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">May 28</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Patrick
																	Pertners</a></td>
															<td class="mail-subject"><a href="mail_detail.html">If
																	you are going to use a passage of Lorem </a></td>
															<td class=""></td>
															<td class="text-right mail-date">May 28</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Michael
																	Fox</a></td>
															<td class="mail-subject"><a href="mail_detail.html">Humour,
																	or non-characteristic words etc.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">Dec 9</td>
														</tr>
														<tr class="read">
															<td class="check-mail">
																<div class="icheckbox_square-green"
																	style="position: relative;">
																	<input type="checkbox" class="i-checks"
																		style="position: absolute; opacity: 0;">
																	<ins class="iCheck-helper"
																		style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
																</div>
															</td>
															<td class="mail-ontact"><a href="mail_detail.html">Damien
																	Ritz</a></td>
															<td class="mail-subject"><a href="mail_detail.html">Oor
																	Lorem Ipsum is that it has a more-or-less normal.</a></td>
															<td class=""></td>
															<td class="text-right mail-date">Jun 11</td>
														</tr>
													</tbody>
												</table>


											</div>
										</div>

										<div class="col-lg-9 animated fadeInRight" id="composeMailBox">
											<div class="mail-box-header">
												
												<h2>Compose mail</h2>
											</div>
											<div class="mail-box">


												<div class="mail-body">

													<form class="m-t" role="form" action="/createEmail"
														method="POST" enctype="multipart/form-data" id="emailForm">
														<div class="form-group">
															<label class="col-sm-2 control-label">To:</label>

															<div class="col-sm-10">
																<select class="form-control m-b" name="to"
																	id="usersToSendEmail"></select> <input type="hidden"
																	class="form-control" name="id" id="usersOwner"
																	value="">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label">Subject:</label>

															<div class="col-sm-10">
																<input type="text" class="form-control" name="subject"
																	id="usersEmailSubject" value="">
															</div>
														</div>


													</form>

												</div>

												<div class="mail-text h-200">

													<div class="summernote" style="display: none;">
														<h3>Hello Jonathan!</h3>
														dummy text of the printing and typesetting industry. <strong>Lorem
															Ipsum has been the industry's</strong> standard dummy text ever
														since the 1500s, when an unknown printer took a galley of
														type and scrambled it to make a type specimen book. It has
														survived not only five centuries, but also the leap into
														electronic typesetting, remaining essentially unchanged.
														It was popularised in the 1960s with the release of
														Letraset sheets containing Lorem Ipsum passages, and more
														recently with <br> <br>

													</div>
													<div class="note-editor">
														<div class="note-dropzone">
															<div class="note-dropzone-message"></div>
														</div>
														<div class="note-dialog">
															<div class="note-image-dialog modal" aria-hidden="false">
																<div class="modal-dialog">
																	<div class="modal-content">
																		<div class="modal-header">
																			<button type="button" class="close"
																				aria-hidden="true" tabindex="-1">×</button>
																			<h4>Insert Image</h4>
																		</div>
																		<div class="modal-body">
																			<div class="row-fluid">
																				<h5>Select from files</h5>
																				<input class="note-image-input" type="file"
																					name="files" accept="image/*">
																				<h5>Image URL</h5>
																				<input class="note-image-url form-control span12"
																					type="text">
																			</div>
																		</div>
																		<div class="modal-footer">
																			<button href="#"
																				class="btn btn-primary note-image-btn disabled"
																				disabled="disabled">Insert Image</button>
																		</div>
																	</div>
																</div>
															</div>
															<div class="note-link-dialog modal" aria-hidden="false">
																<div class="modal-dialog">
																	<div class="modal-content">
																		<div class="modal-header">
																			<button type="button" class="close"
																				aria-hidden="true" tabindex="-1">×</button>
																			<h4>Insert Link</h4>
																		</div>
																		<div class="modal-body">
																			<div class="row-fluid">
																				<div class="form-group">
																					<label>Text to display</label><input
																						class="note-link-text form-control span12"
																						disabled="" type="text">
																				</div>
																				<div class="form-group">
																					<label>To what URL should this link go?</label><input
																						class="note-link-url form-control span12"
																						type="text">
																				</div>
																				<div class="checkbox">
																					<label><input type="checkbox" checked="">
																						Open in new window</label>
																				</div>
																			</div>
																		</div>
																		<div class="modal-footer">
																			<button href="#"
																				class="btn btn-primary note-link-btn disabled"
																				disabled="disabled">Insert Link</button>
																		</div>
																	</div>
																</div>
															</div>
															<div class="note-video-dialog modal" aria-hidden="false">
																<div class="modal-dialog">
																	<div class="modal-content">
																		<div class="modal-header">
																			<button type="button" class="close"
																				aria-hidden="true" tabindex="-1">×</button>
																			<h4>Insert Video</h4>
																		</div>
																		<div class="modal-body">
																			<div class="row-fluid">
																				<div class="form-group">
																					<label>Video URL?</label>&nbsp;<small
																						class="text-muted">(YouTube, Vimeo, Vine,
																						Instagram, or DailyMotion)</small><input
																						class="note-video-url form-control span12"
																						type="text">
																				</div>
																			</div>
																		</div>
																		<div class="modal-footer">
																			<button href="#"
																				class="btn btn-primary note-video-btn disabled"
																				disabled="disabled">Insert Video</button>
																		</div>
																	</div>
																</div>
															</div>
															<div class="note-help-dialog modal" aria-hidden="false">
																<div class="modal-dialog">
																	<div class="modal-content">
																		<div class="modal-body">
																			<a class="modal-close pull-right" aria-hidden="true"
																				tabindex="-1">Close</a>
																			<div class="title">Keyboard shortcuts</div>
																			<p class="text-center">
																				<a href="//hackerwins.github.io/summernote/"
																					target="_blank">Summernote 0.5.2</a> · <a
																					href="//github.com/HackerWins/summernote"
																					target="_blank">Project</a> · <a
																					href="//github.com/HackerWins/summernote/issues"
																					target="_blank">Issues</a>
																			</p>
																			<table class="note-shortcut-layout">
																				<tbody>
																					<tr>
																						<td><table class="note-shortcut">
																								<thead>
																									<tr>
																										<th></th>
																										<th>Action</th>
																									</tr>
																								</thead>
																								<tbody>
																									<tr>
																										<td>Ctrl + Z</td>
																										<td>Undo</td>
																									</tr>
																									<tr>
																										<td>Ctrl + Shift + Z</td>
																										<td>Redo</td>
																									</tr>
																									<tr>
																										<td>Ctrl + ]</td>
																										<td>Indent</td>
																									</tr>
																									<tr>
																										<td>Ctrl + [</td>
																										<td>Outdent</td>
																									</tr>
																									<tr>
																										<td>Ctrl + ENTER</td>
																										<td>Insert Horizontal Rule</td>
																									</tr>
																								</tbody>
																							</table></td>
																						<td><table class="note-shortcut">
																								<thead>
																									<tr>
																										<th></th>
																										<th>Text formatting</th>
																									</tr>
																								</thead>
																								<tbody>
																									<tr>
																										<td>Ctrl + B</td>
																										<td>Bold</td>
																									</tr>
																									<tr>
																										<td>Ctrl + I</td>
																										<td>Italic</td>
																									</tr>
																									<tr>
																										<td>Ctrl + U</td>
																										<td>Underline</td>
																									</tr>
																									<tr>
																										<td>Ctrl + Shift + S</td>
																										<td>Strike</td>
																									</tr>
																									<tr>
																										<td>Ctrl + \</td>
																										<td>Remove Font Style</td>
																									</tr>
																								</tbody>
																							</table></td>
																					</tr>
																					<tr>
																						<td><table class="note-shortcut">
																								<thead>
																									<tr>
																										<th></th>
																										<th>Document Style</th>
																									</tr>
																								</thead>
																								<tbody>
																									<tr>
																										<td>Ctrl + NUM0</td>
																										<td>Normal</td>
																									</tr>
																									<tr>
																										<td>Ctrl + NUM1</td>
																										<td>Header 1</td>
																									</tr>
																									<tr>
																										<td>Ctrl + NUM2</td>
																										<td>Header 2</td>
																									</tr>
																									<tr>
																										<td>Ctrl + NUM3</td>
																										<td>Header 3</td>
																									</tr>
																									<tr>
																										<td>Ctrl + NUM4</td>
																										<td>Header 4</td>
																									</tr>
																									<tr>
																										<td>Ctrl + NUM5</td>
																										<td>Header 5</td>
																									</tr>
																									<tr>
																										<td>Ctrl + NUM6</td>
																										<td>Header 6</td>
																									</tr>
																								</tbody>
																							</table></td>
																						<td><table class="note-shortcut">
																								<thead>
																									<tr>
																										<th></th>
																										<th>Paragraph formatting</th>
																									</tr>
																								</thead>
																								<tbody>
																									<tr>
																										<td>Ctrl + Shift + L</td>
																										<td>Align left</td>
																									</tr>
																									<tr>
																										<td>Ctrl + Shift + E</td>
																										<td>Align center</td>
																									</tr>
																									<tr>
																										<td>Ctrl + Shift + R</td>
																										<td>Align right</td>
																									</tr>
																									<tr>
																										<td>Ctrl + Shift + J</td>
																										<td>Justify full</td>
																									</tr>
																									<tr>
																										<td>Ctrl + Shift + NUM7</td>
																										<td>Ordered list</td>
																									</tr>
																									<tr>
																										<td>Ctrl + Shift + NUM8</td>
																										<td>Unordered list</td>
																									</tr>
																								</tbody>
																							</table></td>
																					</tr>
																				</tbody>
																			</table>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="note-handle">
															<div class="note-control-selection">
																<div class="note-control-selection-bg"></div>
																<div class="note-control-holder note-control-nw"></div>
																<div class="note-control-holder note-control-ne"></div>
																<div class="note-control-holder note-control-sw"></div>
																<div class="note-control-sizing note-control-se"></div>
																<div class="note-control-selection-info"></div>
															</div>
														</div>
														<div class="note-popover">
															<div class="note-link-popover popover bottom in"
																style="display: none;">
																<div class="arrow"></div>
																<div class="popover-content note-link-content">
																	<a href="http://www.google.com" target="_blank">www.google.com</a>&nbsp;&nbsp;
																	<div class="note-insert btn-group">
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-event="showLinkDialog" tabindex="-1"
																			data-original-title="Edit">
																			<i class="fa fa-edit icon-edit"></i>
																		</button>
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-event="unlink" tabindex="-1"
																			data-original-title="Unlink">
																			<i class="fa fa-unlink icon-unlink"></i>
																		</button>
																	</div>
																</div>
															</div>
															<div class="note-image-popover popover bottom in"
																style="display: none;">
																<div class="arrow"></div>
																<div class="popover-content note-image-content">
																	<div class="btn-group">
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-event="resize" data-value="1" tabindex="-1"
																			data-original-title="Resize Full">
																			<span class="note-fontsize-10">100%</span>
																		</button>
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-event="resize" data-value="0.5" tabindex="-1"
																			data-original-title="Resize Half">
																			<span class="note-fontsize-10">50%</span>
																		</button>
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-event="resize" data-value="0.25" tabindex="-1"
																			data-original-title="Resize Quarter">
																			<span class="note-fontsize-10">25%</span>
																		</button>
																	</div>
																	<div class="btn-group">
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-event="floatMe" data-value="left" tabindex="-1"
																			data-original-title="Float Left">
																			<i class="fa fa-align-left icon-align-left"></i>
																		</button>
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-event="floatMe" data-value="right" tabindex="-1"
																			data-original-title="Float Right">
																			<i class="fa fa-align-right icon-align-right"></i>
																		</button>
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-event="floatMe" data-value="none" tabindex="-1"
																			data-original-title="Float None">
																			<i class="fa fa-align-justify icon-align-justify"></i>
																		</button>
																	</div>
																	<div class="btn-group">
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-event="removeMedia" data-value="none"
																			tabindex="-1" data-original-title="Remove Image">
																			<i class="fa fa-trash-o icon-trash"></i>
																		</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="note-toolbar btn-toolbar">
															<div class="note-style btn-group">
																<button type="button"
																	class="btn btn-default btn-sm btn-small dropdown-toggle"
																	title="" data-toggle="dropdown" tabindex="-1"
																	data-original-title="Style">
																	<i class="fa fa-magic icon-magic"></i> <span
																		class="caret"></span>
																</button>
																<ul class="dropdown-menu">
																	<li><a data-event="formatBlock" data-value="p">Normal</a></li>
																	<li><a data-event="formatBlock"
																		data-value="blockquote"><blockquote>Quote</blockquote></a></li>
																	<li><a data-event="formatBlock" data-value="pre">Code</a></li>
																	<li><a data-event="formatBlock" data-value="h1"><h1>Header
																				1</h1></a></li>
																	<li><a data-event="formatBlock" data-value="h2"><h2>Header
																				2</h2></a></li>
																	<li><a data-event="formatBlock" data-value="h3"><h3>Header
																				3</h3></a></li>
																	<li><a data-event="formatBlock" data-value="h4"><h4>Header
																				4</h4></a></li>
																	<li><a data-event="formatBlock" data-value="h5"><h5>Header
																				5</h5></a></li>
																	<li><a data-event="formatBlock" data-value="h6"><h6>Header
																				6</h6></a></li>
																</ul>
															</div>
															<div class="note-font btn-group">
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-shortcut="Ctrl+B" data-mac-shortcut="⌘+B"
																	data-event="bold" tabindex="-1"
																	data-original-title="Bold (Ctrl+B)">
																	<i class="fa fa-bold icon-bold"></i>
																</button>
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-shortcut="Ctrl+I" data-mac-shortcut="⌘+I"
																	data-event="italic" tabindex="-1"
																	data-original-title="Italic (Ctrl+I)">
																	<i class="fa fa-italic icon-italic"></i>
																</button>
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-shortcut="Ctrl+U" data-mac-shortcut="⌘+U"
																	data-event="underline" tabindex="-1"
																	data-original-title="Underline (Ctrl+U)">
																	<i class="fa fa-underline icon-underline"></i>
																</button>
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-shortcut="Ctrl+\" data-mac-shortcut="⌘+\"
																	data-event="removeFormat" tabindex="-1"
																	data-original-title="Remove Font Style (Ctrl+\)">
																	<i class="fa fa-eraser icon-eraser"></i>
																</button>
															</div>
															<div class="note-fontname btn-group">
																<button type="button"
																	class="btn btn-default btn-sm btn-small dropdown-toggle"
																	data-toggle="dropdown" title="" tabindex="-1"
																	data-original-title="Font Family">
																	<span class="note-current-fontname">Arial</span> <b
																		class="caret"></b>
																</button>
																<ul class="dropdown-menu">
																	<li><a data-event="fontName" data-value="Serif"><i
																			class="fa fa-check icon-ok"></i> Serif</a></li>
																	<li><a data-event="fontName" data-value="Sans"><i
																			class="fa fa-check icon-ok"></i> Sans</a></li>
																	<li><a data-event="fontName" data-value="Arial"><i
																			class="fa fa-check icon-ok"></i> Arial</a></li>
																	<li><a data-event="fontName"
																		data-value="Arial Black"><i
																			class="fa fa-check icon-ok"></i> Arial Black</a></li>
																	<li><a data-event="fontName" data-value="Courier"><i
																			class="fa fa-check icon-ok"></i> Courier</a></li>
																	<li><a data-event="fontName"
																		data-value="Courier New"><i
																			class="fa fa-check icon-ok"></i> Courier New</a></li>
																	<li><a data-event="fontName"
																		data-value="Comic Sans MS"><i
																			class="fa fa-check icon-ok"></i> Comic Sans MS</a></li>
																	<li><a data-event="fontName"
																		data-value="Helvetica"><i
																			class="fa fa-check icon-ok"></i> Helvetica</a></li>
																	<li><a data-event="fontName" data-value="Impact"><i
																			class="fa fa-check icon-ok"></i> Impact</a></li>
																	<li><a data-event="fontName"
																		data-value="Lucida Grande"><i
																			class="fa fa-check icon-ok"></i> Lucida Grande</a></li>
																	<li><a data-event="fontName"
																		data-value="Lucida Sans"><i
																			class="fa fa-check icon-ok"></i> Lucida Sans</a></li>
																	<li><a data-event="fontName" data-value="Tahoma"><i
																			class="fa fa-check icon-ok"></i> Tahoma</a></li>
																	<li><a data-event="fontName" data-value="Times"><i
																			class="fa fa-check icon-ok"></i> Times</a></li>
																	<li><a data-event="fontName"
																		data-value="Times New Roman"><i
																			class="fa fa-check icon-ok"></i> Times New Roman</a></li>
																	<li><a data-event="fontName" data-value="Verdana"><i
																			class="fa fa-check icon-ok"></i> Verdana</a></li>
																</ul>
															</div>
															<div class="note-color btn-group">
																<button type="button"
																	class="btn btn-default btn-sm btn-small note-recent-color"
																	title="" data-event="color"
																	data-value="{&quot;backColor&quot;:&quot;yellow&quot;}"
																	tabindex="-1" data-original-title="Recent Color">
																	<i class="fa fa-font icon-font"
																		style="color: black; background-color: yellow;"></i>
																</button>
																<button type="button"
																	class="btn btn-default btn-sm btn-small dropdown-toggle"
																	title="" data-toggle="dropdown" tabindex="-1"
																	data-original-title="More Color">
																	<span class="caret"></span>
																</button>
																<ul class="dropdown-menu">
																	<li><div class="btn-group">
																			<div class="note-palette-title">BackColor</div>
																			<div class="note-color-reset" data-event="backColor"
																				data-value="inherit" title="Transparent">Set
																				transparent</div>
																			<div class="note-color-palette"
																				data-target-event="backColor">
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #000000;"
																						data-event="backColor" data-value="#000000"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#000000"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #424242;"
																						data-event="backColor" data-value="#424242"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#424242"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #636363;"
																						data-event="backColor" data-value="#636363"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#636363"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #9C9C94;"
																						data-event="backColor" data-value="#9C9C94"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#9C9C94"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #CEC6CE;"
																						data-event="backColor" data-value="#CEC6CE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#CEC6CE"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #EFEFEF;"
																						data-event="backColor" data-value="#EFEFEF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#EFEFEF"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #F7F7F7;"
																						data-event="backColor" data-value="#F7F7F7"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#F7F7F7"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFFFFF;"
																						data-event="backColor" data-value="#FFFFFF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFFFFF"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FF0000;"
																						data-event="backColor" data-value="#FF0000"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FF0000"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FF9C00;"
																						data-event="backColor" data-value="#FF9C00"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FF9C00"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFFF00;"
																						data-event="backColor" data-value="#FFFF00"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFFF00"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #00FF00;"
																						data-event="backColor" data-value="#00FF00"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#00FF00"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #00FFFF;"
																						data-event="backColor" data-value="#00FFFF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#00FFFF"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #0000FF;"
																						data-event="backColor" data-value="#0000FF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#0000FF"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #9C00FF;"
																						data-event="backColor" data-value="#9C00FF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#9C00FF"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FF00FF;"
																						data-event="backColor" data-value="#FF00FF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FF00FF"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #F7C6CE;"
																						data-event="backColor" data-value="#F7C6CE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#F7C6CE"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFE7CE;"
																						data-event="backColor" data-value="#FFE7CE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFE7CE"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFEFC6;"
																						data-event="backColor" data-value="#FFEFC6"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFEFC6"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #D6EFD6;"
																						data-event="backColor" data-value="#D6EFD6"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#D6EFD6"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #CEDEE7;"
																						data-event="backColor" data-value="#CEDEE7"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#CEDEE7"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #CEE7F7;"
																						data-event="backColor" data-value="#CEE7F7"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#CEE7F7"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #D6D6E7;"
																						data-event="backColor" data-value="#D6D6E7"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#D6D6E7"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #E7D6DE;"
																						data-event="backColor" data-value="#E7D6DE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#E7D6DE"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #E79C9C;"
																						data-event="backColor" data-value="#E79C9C"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#E79C9C"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFC69C;"
																						data-event="backColor" data-value="#FFC69C"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFC69C"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFE79C;"
																						data-event="backColor" data-value="#FFE79C"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFE79C"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #B5D6A5;"
																						data-event="backColor" data-value="#B5D6A5"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#B5D6A5"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #A5C6CE;"
																						data-event="backColor" data-value="#A5C6CE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#A5C6CE"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #9CC6EF;"
																						data-event="backColor" data-value="#9CC6EF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#9CC6EF"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #B5A5D6;"
																						data-event="backColor" data-value="#B5A5D6"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#B5A5D6"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #D6A5BD;"
																						data-event="backColor" data-value="#D6A5BD"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#D6A5BD"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #E76363;"
																						data-event="backColor" data-value="#E76363"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#E76363"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #F7AD6B;"
																						data-event="backColor" data-value="#F7AD6B"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#F7AD6B"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFD663;"
																						data-event="backColor" data-value="#FFD663"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFD663"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #94BD7B;"
																						data-event="backColor" data-value="#94BD7B"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#94BD7B"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #73A5AD;"
																						data-event="backColor" data-value="#73A5AD"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#73A5AD"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #6BADDE;"
																						data-event="backColor" data-value="#6BADDE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#6BADDE"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #8C7BC6;"
																						data-event="backColor" data-value="#8C7BC6"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#8C7BC6"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #C67BA5;"
																						data-event="backColor" data-value="#C67BA5"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#C67BA5"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #CE0000;"
																						data-event="backColor" data-value="#CE0000"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#CE0000"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #E79439;"
																						data-event="backColor" data-value="#E79439"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#E79439"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #EFC631;"
																						data-event="backColor" data-value="#EFC631"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#EFC631"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #6BA54A;"
																						data-event="backColor" data-value="#6BA54A"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#6BA54A"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #4A7B8C;"
																						data-event="backColor" data-value="#4A7B8C"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#4A7B8C"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #3984C6;"
																						data-event="backColor" data-value="#3984C6"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#3984C6"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #634AA5;"
																						data-event="backColor" data-value="#634AA5"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#634AA5"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #A54A7B;"
																						data-event="backColor" data-value="#A54A7B"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#A54A7B"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #9C0000;"
																						data-event="backColor" data-value="#9C0000"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#9C0000"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #B56308;"
																						data-event="backColor" data-value="#B56308"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#B56308"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #BD9400;"
																						data-event="backColor" data-value="#BD9400"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#BD9400"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #397B21;"
																						data-event="backColor" data-value="#397B21"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#397B21"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #104A5A;"
																						data-event="backColor" data-value="#104A5A"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#104A5A"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #085294;"
																						data-event="backColor" data-value="#085294"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#085294"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #311873;"
																						data-event="backColor" data-value="#311873"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#311873"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #731842;"
																						data-event="backColor" data-value="#731842"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#731842"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #630000;"
																						data-event="backColor" data-value="#630000"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#630000"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #7B3900;"
																						data-event="backColor" data-value="#7B3900"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#7B3900"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #846300;"
																						data-event="backColor" data-value="#846300"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#846300"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #295218;"
																						data-event="backColor" data-value="#295218"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#295218"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #083139;"
																						data-event="backColor" data-value="#083139"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#083139"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #003163;"
																						data-event="backColor" data-value="#003163"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#003163"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #21104A;"
																						data-event="backColor" data-value="#21104A"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#21104A"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #4A1031;"
																						data-event="backColor" data-value="#4A1031"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#4A1031"></button>
																				</div>
																			</div>
																		</div>
																		<div class="btn-group">
																			<div class="note-palette-title">FontColor</div>
																			<div class="note-color-reset" data-event="foreColor"
																				data-value="inherit" title="Reset">Reset to
																				default</div>
																			<div class="note-color-palette"
																				data-target-event="foreColor">
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #000000;"
																						data-event="foreColor" data-value="#000000"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#000000"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #424242;"
																						data-event="foreColor" data-value="#424242"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#424242"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #636363;"
																						data-event="foreColor" data-value="#636363"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#636363"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #9C9C94;"
																						data-event="foreColor" data-value="#9C9C94"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#9C9C94"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #CEC6CE;"
																						data-event="foreColor" data-value="#CEC6CE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#CEC6CE"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #EFEFEF;"
																						data-event="foreColor" data-value="#EFEFEF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#EFEFEF"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #F7F7F7;"
																						data-event="foreColor" data-value="#F7F7F7"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#F7F7F7"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFFFFF;"
																						data-event="foreColor" data-value="#FFFFFF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFFFFF"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FF0000;"
																						data-event="foreColor" data-value="#FF0000"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FF0000"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FF9C00;"
																						data-event="foreColor" data-value="#FF9C00"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FF9C00"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFFF00;"
																						data-event="foreColor" data-value="#FFFF00"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFFF00"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #00FF00;"
																						data-event="foreColor" data-value="#00FF00"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#00FF00"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #00FFFF;"
																						data-event="foreColor" data-value="#00FFFF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#00FFFF"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #0000FF;"
																						data-event="foreColor" data-value="#0000FF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#0000FF"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #9C00FF;"
																						data-event="foreColor" data-value="#9C00FF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#9C00FF"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FF00FF;"
																						data-event="foreColor" data-value="#FF00FF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FF00FF"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #F7C6CE;"
																						data-event="foreColor" data-value="#F7C6CE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#F7C6CE"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFE7CE;"
																						data-event="foreColor" data-value="#FFE7CE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFE7CE"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFEFC6;"
																						data-event="foreColor" data-value="#FFEFC6"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFEFC6"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #D6EFD6;"
																						data-event="foreColor" data-value="#D6EFD6"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#D6EFD6"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #CEDEE7;"
																						data-event="foreColor" data-value="#CEDEE7"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#CEDEE7"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #CEE7F7;"
																						data-event="foreColor" data-value="#CEE7F7"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#CEE7F7"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #D6D6E7;"
																						data-event="foreColor" data-value="#D6D6E7"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#D6D6E7"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #E7D6DE;"
																						data-event="foreColor" data-value="#E7D6DE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#E7D6DE"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #E79C9C;"
																						data-event="foreColor" data-value="#E79C9C"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#E79C9C"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFC69C;"
																						data-event="foreColor" data-value="#FFC69C"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFC69C"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFE79C;"
																						data-event="foreColor" data-value="#FFE79C"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFE79C"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #B5D6A5;"
																						data-event="foreColor" data-value="#B5D6A5"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#B5D6A5"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #A5C6CE;"
																						data-event="foreColor" data-value="#A5C6CE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#A5C6CE"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #9CC6EF;"
																						data-event="foreColor" data-value="#9CC6EF"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#9CC6EF"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #B5A5D6;"
																						data-event="foreColor" data-value="#B5A5D6"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#B5A5D6"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #D6A5BD;"
																						data-event="foreColor" data-value="#D6A5BD"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#D6A5BD"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #E76363;"
																						data-event="foreColor" data-value="#E76363"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#E76363"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #F7AD6B;"
																						data-event="foreColor" data-value="#F7AD6B"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#F7AD6B"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #FFD663;"
																						data-event="foreColor" data-value="#FFD663"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#FFD663"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #94BD7B;"
																						data-event="foreColor" data-value="#94BD7B"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#94BD7B"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #73A5AD;"
																						data-event="foreColor" data-value="#73A5AD"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#73A5AD"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #6BADDE;"
																						data-event="foreColor" data-value="#6BADDE"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#6BADDE"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #8C7BC6;"
																						data-event="foreColor" data-value="#8C7BC6"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#8C7BC6"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #C67BA5;"
																						data-event="foreColor" data-value="#C67BA5"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#C67BA5"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #CE0000;"
																						data-event="foreColor" data-value="#CE0000"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#CE0000"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #E79439;"
																						data-event="foreColor" data-value="#E79439"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#E79439"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #EFC631;"
																						data-event="foreColor" data-value="#EFC631"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#EFC631"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #6BA54A;"
																						data-event="foreColor" data-value="#6BA54A"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#6BA54A"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #4A7B8C;"
																						data-event="foreColor" data-value="#4A7B8C"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#4A7B8C"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #3984C6;"
																						data-event="foreColor" data-value="#3984C6"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#3984C6"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #634AA5;"
																						data-event="foreColor" data-value="#634AA5"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#634AA5"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #A54A7B;"
																						data-event="foreColor" data-value="#A54A7B"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#A54A7B"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #9C0000;"
																						data-event="foreColor" data-value="#9C0000"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#9C0000"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #B56308;"
																						data-event="foreColor" data-value="#B56308"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#B56308"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #BD9400;"
																						data-event="foreColor" data-value="#BD9400"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#BD9400"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #397B21;"
																						data-event="foreColor" data-value="#397B21"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#397B21"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #104A5A;"
																						data-event="foreColor" data-value="#104A5A"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#104A5A"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #085294;"
																						data-event="foreColor" data-value="#085294"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#085294"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #311873;"
																						data-event="foreColor" data-value="#311873"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#311873"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #731842;"
																						data-event="foreColor" data-value="#731842"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#731842"></button>
																				</div>
																				<div>
																					<button type="button" class="note-color-btn"
																						style="background-color: #630000;"
																						data-event="foreColor" data-value="#630000"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#630000"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #7B3900;"
																						data-event="foreColor" data-value="#7B3900"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#7B3900"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #846300;"
																						data-event="foreColor" data-value="#846300"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#846300"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #295218;"
																						data-event="foreColor" data-value="#295218"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#295218"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #083139;"
																						data-event="foreColor" data-value="#083139"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#083139"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #003163;"
																						data-event="foreColor" data-value="#003163"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#003163"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #21104A;"
																						data-event="foreColor" data-value="#21104A"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#21104A"></button>
																					<button type="button" class="note-color-btn"
																						style="background-color: #4A1031;"
																						data-event="foreColor" data-value="#4A1031"
																						title="" data-toggle="button" tabindex="-1"
																						data-original-title="#4A1031"></button>
																				</div>
																			</div>
																		</div></li>
																</ul>
															</div>
															<div class="note-para btn-group">
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-shortcut="Ctrl+Shift+8" data-mac-shortcut="⌘+⇧+7"
																	data-event="insertUnorderedList" tabindex="-1"
																	data-original-title="Unordered list (Ctrl+Shift+8)">
																	<i class="fa fa-list-ul icon-list-ul"></i>
																</button>
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-shortcut="Ctrl+Shift+7" data-mac-shortcut="⌘+⇧+8"
																	data-event="insertOrderedList" tabindex="-1"
																	data-original-title="Ordered list (Ctrl+Shift+7)">
																	<i class="fa fa-list-ol icon-list-ol"></i>
																</button>
																<button type="button"
																	class="btn btn-default btn-sm btn-small dropdown-toggle"
																	title="" data-toggle="dropdown" tabindex="-1"
																	data-original-title="Paragraph">
																	<i class="fa fa-align-left icon-align-left"></i> <span
																		class="caret"></span>
																</button>
																<div class="dropdown-menu">
																	<div class="note-align btn-group">
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-shortcut="Ctrl+Shift+L"
																			data-mac-shortcut="⌘+⇧+L" data-event="justifyLeft"
																			tabindex="-1"
																			data-original-title="Align left (Ctrl+Shift+L)">
																			<i class="fa fa-align-left icon-align-left"></i>
																		</button>
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-shortcut="Ctrl+Shift+E"
																			data-mac-shortcut="⌘+⇧+E" data-event="justifyCenter"
																			tabindex="-1"
																			data-original-title="Align center (Ctrl+Shift+E)">
																			<i class="fa fa-align-center icon-align-center"></i>
																		</button>
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-shortcut="Ctrl+Shift+R"
																			data-mac-shortcut="⌘+⇧+R" data-event="justifyRight"
																			tabindex="-1"
																			data-original-title="Align right (Ctrl+Shift+R)">
																			<i class="fa fa-align-right icon-align-right"></i>
																		</button>
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-shortcut="Ctrl+Shift+J"
																			data-mac-shortcut="⌘+⇧+J" data-event="justifyFull"
																			tabindex="-1"
																			data-original-title="Justify full (Ctrl+Shift+J)">
																			<i class="fa fa-align-justify icon-align-justify"></i>
																		</button>
																	</div>
																	<div class="note-list btn-group">
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-shortcut="Ctrl+[" data-mac-shortcut="⌘+["
																			data-event="outdent" tabindex="-1"
																			data-original-title="Outdent (Ctrl+[)">
																			<i class="fa fa-outdent icon-indent-left"></i>
																		</button>
																		<button type="button"
																			class="btn btn-default btn-sm btn-small" title=""
																			data-shortcut="Ctrl+]" data-mac-shortcut="⌘+]"
																			data-event="indent" tabindex="-1"
																			data-original-title="Indent (Ctrl+])">
																			<i class="fa fa-indent icon-indent-right"></i>
																		</button>
																	</div>
																</div>
															</div>
															<div class="note-height btn-group">
																<button type="button"
																	class="btn btn-default btn-sm btn-small dropdown-toggle"
																	data-toggle="dropdown" title="" tabindex="-1"
																	data-original-title="Line Height">
																	<i class="fa fa-text-height icon-text-height"></i>&nbsp;
																	<b class="caret"></b>
																</button>
																<ul class="dropdown-menu">
																	<li><a data-event="lineHeight" data-value="1.0"><i
																			class="fa fa-check icon-ok"></i> 1.0</a></li>
																	<li><a data-event="lineHeight" data-value="1.2"><i
																			class="fa fa-check icon-ok"></i> 1.2</a></li>
																	<li><a data-event="lineHeight" data-value="1.4"><i
																			class="fa fa-check icon-ok"></i> 1.4</a></li>
																	<li><a data-event="lineHeight" data-value="1.5"><i
																			class="fa fa-check icon-ok"></i> 1.5</a></li>
																	<li><a data-event="lineHeight" data-value="1.6"><i
																			class="fa fa-check icon-ok"></i> 1.6</a></li>
																	<li><a data-event="lineHeight" data-value="1.8"><i
																			class="fa fa-check icon-ok"></i> 1.8</a></li>
																	<li><a data-event="lineHeight" data-value="2.0"><i
																			class="fa fa-check icon-ok"></i> 2.0</a></li>
																	<li><a data-event="lineHeight" data-value="3.0"><i
																			class="fa fa-check icon-ok"></i> 3.0</a></li>
																</ul>
															</div>
															<div class="note-table btn-group">
																<button type="button"
																	class="btn btn-default btn-sm btn-small dropdown-toggle"
																	title="" data-toggle="dropdown" tabindex="-1"
																	data-original-title="Table">
																	<i class="fa fa-table icon-table"></i> <span
																		class="caret"></span>
																</button>
																<ul class="dropdown-menu">
																	<div class="note-dimension-picker">
																		<div class="note-dimension-picker-mousecatcher"
																			data-event="insertTable" data-value="1x1"></div>
																		<div class="note-dimension-picker-highlighted"></div>
																		<div class="note-dimension-picker-unhighlighted"></div>
																	</div>
																	<div class="note-dimension-display">1 x 1</div>
																</ul>
															</div>
															<div class="note-insert btn-group">
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-event="showLinkDialog" tabindex="-1"
																	data-original-title="Link">
																	<i class="fa fa-link icon-link"></i>
																</button>
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-event="showImageDialog" tabindex="-1"
																	data-original-title="Picture">
																	<i class="fa fa-picture-o icon-picture"></i>
																</button>
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-event="showVideoDialog" tabindex="-1"
																	data-original-title="Video">
																	<i class="fa fa-youtube-play icon-play"></i>
																</button>
															</div>
															<div class="note-view btn-group">
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-event="fullscreen" tabindex="-1"
																	data-original-title="Full Screen">
																	<i class="fa fa-arrows-alt icon-fullscreen"></i>
																</button>
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-event="codeview" tabindex="-1"
																	data-original-title="Code View">
																	<i class="fa fa-code icon-code"></i>
																</button>
															</div>
															<div class="note-help btn-group">
																<button type="button"
																	class="btn btn-default btn-sm btn-small" title=""
																	data-event="showHelpDialog" tabindex="-1"
																	data-original-title="Help">
																	<i class="fa fa-question icon-question"></i>
																</button>
															</div>
														</div>
														<textarea class="note-codable"></textarea>
														<div class="note-editable" contenteditable="true"
															id="textToConvert">
															<h3>Hello Jonathan!</h3>
															dummy text of the printing and typesetting industry. <strong>Lorem
																Ipsum has been the industry's</strong> standard dummy text ever
															since the 1500s, when an unknown printer took a galley of
															type and scrambled it to make a type specimen book. It
															has survived not only five centuries, but also the leap
															into electronic typesetting, remaining essentially
															unchanged. It was popularised in the 1960s with the
															release of Letraset sheets containing Lorem Ipsum
															passages, and more recently with <br> <br>

														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="mail-body text-right tooltip-demo">
													<button class="btn btn-sm btn-primary" id="sendEmail"
														 data-toggle="tooltip" data-placement="top"
														title="Send"><i class="fa fa-reply"></i> Enviar</button> 
														<button  class="btn btn-white btn-sm" id="discardEmail"
														data-toggle="tooltip" data-placement="top"
														title="Discard email"><i class="fa fa-times"></i>
														Discard</button>
												</div>
												<div class="clearfix"></div>



											</div>
										</div>










									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>



			<!-- ranking usuarios -->
			<div class="row" id="stage3">
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row" id="userRankingGrid"></div>
				</div>

			</div>
			<div class="row" id="verTrofeos">
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row" id="userTrofeos"></div>
				</div>

			</div>
				<div class="row" id="workgroups">
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row" id="userWorkgroups"></div>
					<div class="row" id="userTemaDetail"></div>
				</div>

			</div>

			<div class='wrapper wrapper-content animated fadeIn' id='stage12'>
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title  back-change">
								<h5>Editar Imagen de perfil</h5>
								<div class="ibox-tools">
									<a class="close-link"> <i class="fa fa-times"></i>
									</a>
								</div>
							</div>
							<div class="ibox-content">
								<p>Seleccione la parte de la imagen que desea actualizar</p>
								<div class="row">
									<div class="col-md-6">
										<div class="image-crop">
											<img src="resources/img/p3.jpg">
										</div>
									</div>
									<div class="col-md-6">
										<h4>Preview de la imagen seleccionada</h4>
										<div class="img-preview img-preview-sm"></div>
										<h4>Descargar</h4>
										<p>Puedes descargar la imagen recortada presionando
											descargar</p>
										<div class="btn-group">
											<label title="Upload image file" for="inputImage"
												class="btn btn-primary"> <input type="file"
												accept="image/*" name="file" id="inputImage" class="hide">
												Subir Imagen
											</label> <label title="Donload image" id="download"
												class="btn btn-primary">Descargar</label>
										</div>
										<h4>Transformaciones</h4>
										<p>puede cambiar la orientacion de la imagen aqui.</p>
										<div class="btn-group">
											<button class="btn btn-white" id="zoomIn" type="button">Zoom
												In</button>
											<button class="btn btn-white" id="zoomOut" type="button">Zoom
												Out</button>
											<button class="btn btn-white" id="rotateLeft" type="button">Rotate
												Left</button>
											<button class="btn btn-white" id="rotateRight" type="button">Rotate
												Right</button>

										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Editar campos del perfil</h5>
								<div class="ibox-tools">
									<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
									</a> <a class="close-link"> <i class="fa fa-times"></i>
									</a>
								</div>
							</div>
							<div class="ibox-content">
								<form class="m-t" role="form" action="/uptUser" method="POST"
									enctype="multipart/form-data">
									<div class="form-group">
										<label class="col-sm-2 control-label">Nombre</label>

										<div class="col-sm-10">
											<input value="" id="usrName3" disabled
												name="userName" type="text" class="form-control">
										</div>
										<input type="hidden" class="form-control" name="userName" id="usrName4" 
											value="" required>
									</div>
									<div class="hr-line-dashed"></div>
									<input type="hidden" class="form-control" name="userImg"
										value="" id="userImg">
									<div class="form-group">
										<label class="col-sm-2 control-label">Email</label>
										<div class="col-sm-10">
											<input type="email" class="form-control" name="email">
											<span class="help-block m-b-none">Debes utilizar el
												formato de correo correcto.</span>
										</div>
									</div>
									<div class="hr-line-dashed"></div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Password</label>
										<div class="col-sm-10">
											<input type="password" class="form-control" name="pass">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Ciudad</label>
										<div class="col-sm-10">
											<input type="text" name="ciudad" id="usrCity2" class="form-control">
										</div>
									</div>
									<input type="hidden" class="form-control" name="source"
										value="profile" required="">
									<!--                                 <div class="hr-line-dashed"></div> -->
									<!--                                 <div class="form-group"><label class="col-sm-2 control-label">Placeholder</label> -->

									<!--                                     <div class="col-sm-10"><input type="text" placeholder="placeholder" class="form-control"></div> -->
									<!--                                 </div> -->

									<button class="btn btn-warning" id="setDrag" type="submit">Actualizar
										Campos</button>
								</form>
							</div>
						</div>
					</div>
				</div>

			</div>

<script>
// TODO
function selectedValue(){
	if ($('#example2').is(":checked"))
	{
		alert("check");//microblogin
	}else 
		alert("no check");//cambio de estado

	
	

}

function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
        //alert(expires); 
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function cargarCommentsTema(temaId){
	
	$("#userWorkgroups").hide();
	$("#temaLoad").show();
	$('#loadMoreLocation').val("socialFeedTemaRow");
	$('#loadMoreTema').val(temaId);
	CALLERAJAX.callAllNotifications("socialFeedTemaRow", temaId, true);
	$("#temaLoad").show();
	
	
	
}

function eraseCookie(name) {
    createCookie(name, "", -1);
    window.location.replace("/login2");
}
</script>
			
			
			<div class=" wrapper-content animated fadeIn" id="stage51">
				<div class="row show-grid">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
							<h3>Publicar tweet</h3>
<!-- 								<h5>Dropzone de documentos</h5> -->
<!-- 								<div class="ibox-tools"> -->
<!-- 									<a class="collapse-link"> <i class="fa fa-chevron-up"></i> -->
<!-- 									</a> <a class="dropdown-toggle" data-toggle="dropdown" href="#"> -->
<!-- 										<i class="fa fa-wrench"></i> -->
<!-- 									</a> -->
<!-- 									<ul class="dropdown-menu dropdown-user"> -->
<!-- 										<li><a href="#">Config option 1</a></li> -->
<!-- 										<li><a href="#">Config option 2</a></li> -->
<!-- 									</ul> -->
<!-- 									<a class="close-link"> <i class="fa fa-times"></i> -->
<!-- 									</a> -->
<!-- 								</div> -->

</div>

		




			
							

	
							
							<div class="ibox-content " style=" padding: 15px 20px 271px 20px;">
<!-- 								<div class="col-sm-4">									   -->
<!--                             </div> -->
                            
									<!-- 										<label class="col-sm-2 control-label">Estado</label> -->
								<div class="col-xs-12 col-xl-12" id="normalPost">	
								<form class="m-t" role="form" 
										id="stateChange" >

										<div class="input-group">
											<input type="hidden" class="form-control" name="id" id="postUsrId"
												value="" required> <input
												type="text" class="form-control" name="state"  id="contentState"required>
											<span class="input-group-btn">
												<button type="button" onClick="doPost()" id="publicarTweet" class="btn btn-primary">Publicar!
												
												</button>
											</span>
											
										</div>
										<div class="input-group">
										<input id="the-file" name="file" type="file" >
											 
										</div>

									</form>
								</div>
<!-- 								<div class="form-group"> -->
<!-- 									<label class="col-sm-2 control-label">Titulo</label> -->
<!-- 									<div class="col-sm-10"> -->
<!-- 									<input type="text" class="form-control" id="postTittle" required autofocus="autofocus">											 -->
<!-- 									</div> -->
									
<!-- 									</div> -->

<!-- 							</div>	 -->
<!-- 										<div class="ibox-content "> -->

<!-- 								<div class="form-group">	 -->
<!-- 								<label class="col-sm-2 control-label">Description</label>		 -->
<!-- 									<div class="col-sm-10"> -->
<!-- 									<input type="text" class="form-control" id="postDescription" required> -->
<!-- 									</div> -->
<!-- 								</div> -->

<!-- 							</div> -->
<!-- 							<div class="ibox-content "> -->

<!-- 								<form id="my-awesome-dropzone" class="dropzone " -->
<!-- 									action="/uploadFile"> -->
<!-- 									<input type="hidden" class="form-control" name="id" id="userID" -->
<!-- 										value=""> <input -->
<!-- 										type="hidden" class="form-control" name="tittle" -->
<!-- 										id="tittleName"> <input type="hidden" -->
<!-- 										class="form-control" name="description" id="description"> -->
<!-- 									<div class="dropzone-previews"></div> -->
<!-- 									<button type="submit" class="btn btn-primary pull-right" -->
<!-- 										id="submitFilePost">Subir archivo!</button> -->
<!-- 									<div class="dz-default dz-message"> -->
<!-- 										<span>Drop files here to upload</span> -->
<!-- 									</div> -->

<!-- 								</form> -->

							
						</div>
					</div>
				</div></div>
</div>
			

<div class="modal inmodal fade" id="myModal6" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title">Evaluar Tweet</h4>
                                        </div>
                                        <div class="modal-body">
                <input type="hidden" id="actionResult">
                
                                        <div><label> <input type="radio"  id="optionsRadios1"  value="bronce" name="optionsRadios"> <img style="
    width: 30%;
" src="resources/img/medallaBronce.png"> Puede mejorar</label></div>
                                        <div><label> <input type="radio" checked="" value="plata" id="optionsRadios2" name="optionsRadios"> <img  style="
    width: 36%;
"src="resources/img/medallaPlata.png"> Aceptable</label></div>
                                        <div><label> <input type="radio" value="oro" id="optionsRadios3" name="optionsRadios"><img  style="
    width: 42%;
"src="resources/img/medallaOro.png">Destaca</label></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <button type="button" onClick="savePointsToUser()" class="btn btn-primary" data-dismiss="modal">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

			<div class="footer">
<div class="pull-right">
                <a class="btn btn-primary btn-rounded" href="" id="loadMoreResults">cargar mas</a>
                <input type="hidden" id="loadMoreLocation">
                <input type="hidden" id="loadMoreTema">
                <input type="hidden" id="beforePage">
                <input type="hidden" id="currentPage">
            </div>
				<div>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal6">
                                    Small Modal
                                </button>
					<strong>Copyright</strong> PlayLearn Company &copy; 2015-2016
					
					
				</div>
			</div>

		</div>

	</div>


	<!-- Mainly scripts -->
<!-- 	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.js" type="text/javascript"></script> -->
	<script src="resources/js/jquery-2.1.1.js"></script>
	<script src="resources/js/bootstrap.js"></script>
	<script src="resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="resources/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

	<!-- Custom and plugin javascript -->
	<script src="resources/js/inspinia.js"></script>
	<script src="resources/js/plugins/pace/pace.min.js"></script>

	<!-- iCheck -->
	<script src="resources/js/plugins/iCheck/icheck.min.js"></script>

	<!-- Peity -->
	<script src="resources/js/plugins/peity/jquery.peity.min.js"></script>
	<script src="resources/js/demo/peity-demo.js"></script>
	<!-- DROPZONE -->
	<script src="resources/js/plugins/dropzone/dropzone.js"></script>
	<!-- Image cropper -->
	<script src="resources/js/plugins/cropper/cropper.min.js"></script>
	<!-- SUMMERNOTE -->
	<script src="resources/js/plugins/summernote/summernote.min.js"></script>
	<script>
		var emailArray = [];
		function setSubjectData(subject){
			//TODO:
// 			if(!id)
// 				window.location.replace("/login2");
			var usrPic="/user/photo/?id="+id;	
			$('#usrImgSmall').attr("src",usrPic);
			$('#usrImgBig').attr("src", usrPic);
			$('#usrName').text(subject.login);
			$('#usrName1').text(subject.login);
			$('#postUsrId').val(id);
			$('#usersOwner').val(id);
			$('#userID').val(id);
			createCookie("userID",id,1);
			$('#usrName2').text(subject.login);
							$('#usrName3').val(subject.login);
							$('#usrCity2').val(subject.city);
							$('#usrName4').val(id);
									$('#usrRole').text(subject.role);
											$('#usrState').text(subject.userState);
													$('#usrCity').text(subject.city);
			
			
			
			
		};
		function  puntuarTweet(actionId,target,temaId){
			
			document.getElementById("actionResult").value=actionId;
			
		};
		function savePointsToUser(){
			
			var tweetId=document.getElementById("actionResult").value;
			var tropie="";
			
			if(document.getElementById("optionsRadios1").checked)				
			tropie=document.getElementById("optionsRadios1").value;
			if(document.getElementById("optionsRadios2").checked)
				tropie=document.getElementById("optionsRadios2").value;
			if(document.getElementById("optionsRadios3").checked)
				tropie=document.getElementById("optionsRadios3").value;
			
			
			var jForm = new FormData();
			jForm.append("tweetId",  tweetId);
			jForm.append("tropie", tropie);
			
			

			$.ajax({
				type : "POST",
				contentType : false,
				url : "http://localhost:8080/createPoint",
				data : jForm,
				processData : false,
				cache : false,
				async : true,
				dataType : 'text',
				timeout : 3000,
				success : function(data) {
//						CALLERAJAX.getStats();
//						var where="#"+getCurrentStageVisible();
					
//						hideNavElements();
//						hideSubElements();
					
//						$('#'+target).show();
					CALLERAJAX.callAllNotifications(target.id,temaId, true);
				},
				error : function(jqXHR, textStatus, errorThrown) {

					var responseText = jqXHR.responseText;
					console.log("Fail responseText: " + responseText);
					var statusC = jqXHR.status;
					console.log("Fail Status Code: " + statusC);
					console.log("Fail textStatus: " + textStatus);
					console.log("Fail error: " + errorThrown);
					alert("se produjo un error enviando el comentario");
				}

			});
		
		
		
		}
		
		function createLike(placeId, target,temaId) {
			
			
			var jForm = new FormData();
			jForm.append("user", id);
			jForm.append("placeId", placeId);

			$.ajax({
				type : "POST",
				contentType : false,
				url : "http://localhost:8080/like",
				data : jForm,
				processData : false,
				cache : false,
				async : true,
				dataType : 'text',
				timeout : 3000,
				success : function(data) {
					
				},
				error : function(jqXHR, textStatus, errorThrown) {

					var responseText = jqXHR.responseText;
					console.log("Fail responseText: " + responseText);
					var statusC = jqXHR.status;
					console.log("Fail Status Code: " + statusC);
					console.log("Fail textStatus: " + textStatus);
					console.log("Fail error: " + errorThrown);
				}

			});
			CALLERAJAX.callAllNotifications(target.id,temaId, true);
			
		}
function getCurrentStageVisible(){
	if($('#stage0').css('display')=="block")
		return "stage0";
	else if($('#stage1').css('display')=="block")
		return "stage1";
	else if($('#stage2').css('display')=="block")
		return "stage2";
	else if($('#stage3').css('display')=="block")
		return "stage3";
	else if($('#stage4').css('display')=="block")
		return "stage4";
	else if($('#stage5').css('display')=="block")
		return "stage5";
	else if($('#socialFeedRow').css('display')=="block")
		return "socialFeedRow";
	else if($('#socialFeedTemaRow').css('display')=="block")
		return "socialFeedTemaRow";
	
	
}
		function openEmail(number) {

			var emailElements = emailArray[number].email;

			$('#composeMailBox').hide();
			$('#mailInbox').hide();
			$('#mainMail').empty();
			emailToShow = " <div class='col-lg-9 animated fadeInRight' id='readedEmail'><div class='mail-box-header'><div class='pull-right tooltip-demo'>"
					+ "<a href='mail_compose.html' class='btn btn-white btn-sm' data-toggle='tooltip' data-placement='top' title='Reply'><i class='fa fa-reply'></i> Reply</a>"
					+ "<a href='#' class='btn btn-white btn-sm' data-toggle='tooltip' data-placement='top' title='Print email'><i class='fa fa-print'></i> </a>"
					+ "<a href='mailbox.html' class='btn btn-white btn-sm' data-toggle='tooltip' data-placement='top' title='Move to trash'><i class='fa fa-trash-o'></i> </a>"
					+ "</div>"
					+ "<div class='mail-tools tooltip-demo m-t-md'><h3>"
					+ "<span class='font-noraml'>Subject: </span>"
					+ emailElements.subject
					+ "</h3><h5>"
					+ "<span class='pull-right font-noraml'>"
					+ emailElements.creationDate
					+ "</span><span class='font-noraml'>From: </span>"
					+ emailArray[number].fromOwner
					+ "</h5></div></div><div class='mail-box'><div class='mail-body'>"
					+ emailElements.content
					+ "</div><div class='clearfix'></div></div></div>"
					+ "<div class='clearfix'></div></div>";

			$('#mainMail').append(emailToShow);
			//TODO mark as readed the current opened email if it status is not readed

		};

		$(window).bind("beforeunload", function() {
			//TODO
alert("esto funciona");
			
			$.ajax({
				type : "GET",
				contentType : "application/x-www-form-urlencoded;charset=utf-8",
				url : "http://localhost:8080/logout",
				dataType : 'JSONP',
				async : true,
				cache : false,
				dataType : 'text',
				timeout : 3000,
				success : function(data2) {
				},

				error : function(jqXHR, textStatus, errorThrown) {

					var responseText = jqXHR.responseText;
					console.log("Fail responseText: " + responseText);
					var statusC = jqXHR.status;
					console.log("Fail Status Code: " + statusC);
					console.log("Fail textStatus: " + textStatus);
					console.log("Fail error: " + errorThrown);
				}

			});
				
		});
		function getDocsElementsSocial(temaId,target,tittle, id, cabeceraPost1, owner,
				ownerId, actionId) {
			var jForm = new FormData();
			jForm.append("owner", owner);			
			jForm.append("id", id);
			$.ajax({
						type : "POST",
						contentType : false,
						url : "http://localhost:8080/documents/",
						data : jForm,
						processData : false,
						cache : false,
						async : true,
						dataType : 'text',
						timeout : 3000,
						
						success : function(data2) {
							docs = JSON.parse(data2);
							var containsPicture = false;
							for ( var item1 in docs) {
								//TODO
								if (tittle == docs[item1].title) {
									containsPicture = true;
									var elements = docs[item1].files;
									var targetImageAppender = "";
									for ( var item2 in elements) {

										targetImageAppender = targetImageAppender
										+ "<div class='photos'><a target='_blank'href='/files/"+id+"/"+owner+"/"+elements[item2].fileName+"'>"
												+ "<img alt='image' class='feed-photo' src='/files/"+id+"/"+owner+"/"+elements[item2].fileName+"'></a></div>"
									}								
									getLikesIteractionsSocial(temaId,target,cabeceraPost1,
											targetImageAppender, id, actionId);
								}
							}

							if (containsPicture == false) {
									
								photosPost1 = "";
								getLikesIteractionsSocial(temaId,target,cabeceraPost1,
										photosPost1, id, actionId);
							}

						},

						error : function(jqXHR, textStatus, errorThrown) {

							var responseText = jqXHR.responseText;
							console.log("Fail responseText: " + responseText);
							var statusC = jqXHR.status;
							console.log("Fail Status Code: " + statusC);
							console.log("Fail textStatus: " + textStatus);
							console.log("Fail error: " + errorThrown);
						}

					});
		};
		function getLikesIteractionsSocial(temaId,target,cabeceraPost1, photosPost1, id,
				actionId) {
			$
					.ajax({
						type : "GET",
						contentType : "application/x-www-form-urlencoded;charset=utf-8",
						url : "http://localhost:8080/likes/all/" + id,
						dataType : 'JSONP',
						async : true,
						cache : false,
						dataType : 'text',
						timeout : 3000,
						success : function(data2) {
							docs = JSON.parse(data2);
							var likeable = true;
							itemCounter=0;
							for ( var item1 in docs) {
								if (docs[item1].place == actionId
										&& docs[item1].ownerId == subject.id) {

									likeable = false;
									footerPost1 = "<div class='btn-group'>"
											+ "<a class='btn btn-xs btn-blue' disabled ><i class='fa fa-thumbs-up'></i> Like </a> "
											+ "<a class='btn btn-xs btn-white' onclick=CALLERAJAX.createComment("
											+ actionId+","+target+","+temaId
											+ ");><i class='fa fa-comments'></i>Comment</a> "
											+ "</div></div>";
								}
							}

							if (likeable == true) {

								footerPost1 = "<div class='btn-group'>"
										+ "<a class='btn btn-xs btn-white' id='createLike"+itemCounter+"' onclick=createLike("
										+ actionId+","+target+","+temaId
										+ ")><i class='fa fa-thumbs-up'></i> Like </a> "
										+ "<a class='btn btn-xs btn-white' onclick=CALLERAJAX.createComment("
										+ actionId+","+target+","+temaId
										+ ");><i class='fa fa-comments'></i>Comment</a> "
										+ "</div></div>";
								itemCounter+=1;
							}

			getCommentsIteractionsSocial(temaId,target,cabeceraPost1, photosPost1, id, actionId ,footerPost1);


						},

						error : function(jqXHR, textStatus, errorThrown) {

							var responseText = jqXHR.responseText;
							console.log("Fail responseText: " + responseText);
							var statusC = jqXHR.status;
							console.log("Fail Status Code: " + statusC);
							console.log("Fail textStatus: " + textStatus);
							console.log("Fail error: " + errorThrown);
						}

					});
		};

		function getCommentsIteractionsSocial(temaId,target,cabeceraPost1, photosPost1, id, actionId ,footerPost1) {
			$
					.ajax({
						type : "GET",
						contentType : "application/x-www-form-urlencoded;charset=utf-8",
						url : "http://localhost:8080/comments/all/" + id,
						dataType : 'JSONP',
						async : true,
						cache : false,
						dataType : 'text',
						timeout : 3000,
						success : function(data2) {
							docs = JSON.parse(data2);
							comment="";
							var likeable = true;
							for ( var item1 in docs) {
								var colors=[];
								colors.push("rgba(178, 232, 172, 0.62);");
								colors.push("rgba(226, 175, 220, 0.25);");								
								colors.push("rgba(133, 175, 212, 0.41);");
								colors.push("rgba(226, 175, 220, 0.45);");
								colors.push("rgba(220, 131, 164, 0.23);");
								colors.push("#E4CEE4");								
								colors.push("rgba(152, 249, 218, 0.37);");
								colors.push("#98D1F9");
								colors.push("rgba(167, 249, 152, 0.61);");
								
								var currentColor=(item1%(colors.length));
								if( docs[item1].place==actionId){
								comment =comment+ "<div class='social-avatar' style='background-color: "+ colors[currentColor]+";'>"
										+ "<a href='#' class='pull-left'>"
										+ "<img alt='image' src='/user/profile2/?loginName="+docs[item1].owner+"'>"
										+ "</a><div class='media-body'>"
										+ "<a href='#'>"
										+ docs[item1].owner+"</a><p>"
										+ docs[item1].content
										+ "</p><br/>"
// 										+ "<a href='#' class='small'><i class='fa fa-thumbs-up'></i>"
// 										+ "<small>26 Like this!</small></a> -"
										+ "<small class='text-muted'>"
										+ docs[item1].creationDate
										+ "</small></div></div>";
										}

							}
							iteractionalFooter = "<div class='social-avatar'>"
									+ "<a href='#' class='pull-left'> "
									+ "<img alt='image' src='/user/photo/?id="
									+ id
									+ "'>"
									+

									"</a><div class='media-body'>"
									+ "<textarea class='form-control' placeholder='Write comment...' id=commentOn"+actionId+"></textarea>"
									+ "</div></div>"+footerPost1+comment;

									$("#"+target).append(
									cabeceraPost1 + photosPost1
											+ iteractionalFooter +"</div>");
						

						},

						error : function(jqXHR, textStatus, errorThrown) {

							var responseText = jqXHR.responseText;
							console.log("Fail responseText: " + responseText);
							var statusC = jqXHR.status;
							console.log("Fail Status Code: " + statusC);
							console.log("Fail textStatus: " + textStatus);
							console.log("Fail error: " + errorThrown);
						}

					});
		};
		function sendEmail(){
			
			myDivObj = document
						.getElementById("textToConvert");
			
				var subject = $(
						'#usersEmailSubject')
						.val();
				var content = myDivObj.innerHTML;
				var to = $(
						'#usersToSendEmail option:selected')
						.val();
				id = $('#usersOwner').val();
			
				var jForm = new FormData();
				jForm
						.append("subject",
								subject);
				jForm
						.append("content",
								content);
				jForm.append("to", to);
				jForm.append("id", id);
			
				$
						.ajax({
							type : "POST",
							contentType : false,
							url : "http://localhost:8080/createEmail",
							data : jForm,
							processData : false,
							cache : false,
							async : true,
							dataType : 'text',
							timeout : 3000,
							success : function(
									data) {
								
							},
							error : function(
									jqXHR,
									textStatus,
									errorThrown) {
			
								var responseText = jqXHR.responseText;
								console
										.log("Fail responseText: "
												+ responseText);
								var statusC = jqXHR.status;
								console
										.log("Fail Status Code: "
												+ statusC);
								console
										.log("Fail textStatus: "
												+ textStatus);
								console
										.log("Fail error: "
												+ errorThrown);
							}
			
						});
				$('#messageShow').click();
						
		}
		function getDocsElements(tittle, id, cabeceraPost, owner) {
			var jForm = new FormData();
			jForm.append("owner", owner);
			jForm.append("id", id);
			$.ajax({
						type : "POST",
						contentType : false,
						url : "http://localhost:8080/documents/",
						data : jForm,
						processData : false,
						cache : false,
						async : true,
						dataType : 'text',
						timeout : 3000,
						
						success : function(data2) {
							docs = JSON.parse(data2);
							var containsPicture = false;
							for ( var item1 in docs) {
								if (tittle == docs[item1].title) {
									var elements = docs[item1].files;
									var targetImageAppender = "";
									containsPicture = true;
									for ( var item2 in elements) {

										targetImageAppender = targetImageAppender
												+ "<a target='_blank'href='/files/"+owner+"/"+elements[item2].fileName+"'>"
												+ "<img alt='image' class='feed-photo' src='/files/"+id+"/"+owner+"/"+elements[item2].fileName+"'>"
									}
									photosPost = "<div class='photos'>"
											+ targetImageAppender + "</div>";
									footerPost = "<div class='media-body'>"
											+ "<textarea class='form-control' placeholder='Write comment...'></textarea>"
											+ "</div><div class='pull-right'>"
											+ "<a class='btn btn-xs btn-primary'><i class='fa fa-thumbs-up'></i> Like </a> "
											+ "<a class='btn btn-xs btn-white'><i class='fa fa-pencil'></i>Comment</a> "
											+ "</div></div></div>";

									$("#notification4me").append(
											cabeceraPost + photosPost
													);
								}

							}

							if (containsPicture == false) {
								footerPost = "<div class='media-body'>"
										+ "<textarea class='form-control' placeholder='Write comment...'></textarea>"
										+ "</div><div class='pull-right'>"
										+ "<a class='btn btn-xs btn-primary'><i class='fa fa-thumbs-up'></i> Like </a> "
										+ "<a class='btn btn-xs btn-white'><i class='fa fa-pencil'></i>Comment</a> "
										+ "</div></div></div>";

								$("#notification4me").append(
										cabeceraPost );
							}

						},

						error : function(jqXHR, textStatus, errorThrown) {

							var responseText = jqXHR.responseText;
							console.log("Fail responseText: " + responseText);
							var statusC = jqXHR.status;
							console.log("Fail Status Code: " + statusC);
							console.log("Fail textStatus: " + textStatus);
							console.log("Fail error: " + errorThrown);
						}

					});
	
	
	
		}
		function doPost(){
			
			var jForm = new FormData();
			var url="/state";
			if(getCurrentStageVisible()==="stage1"){
				//tweet
				url="/tweet";
				jForm
				.append("state",
						document.getElementById("contentTweet").value);
					
			}
			else{
				jForm
				.append("state",
						document.getElementById("contentState").value);
			}
			//chechexAqui
			
			
			var fileInput=document.getElementById("the-file");
			var file = fileInput.files[0];
			
			jForm
					.append("statePict", 
							file);
			
			$.ajax({
				type : "POST",
				contentType : false,
				url : url,
				data : jForm,
				processData : false,
				cache : false,
				async : true,
				dataType : 'text',
				
				success : function(
						data) {
					$('#socialShow').click();
					document.getElementById('stateChange').reset();
				},
				error : function(
						jqXHR,
						textStatus,
						errorThrown) {

					var responseText = jqXHR.responseText;
					console
							.log("Fail responseText: "
									+ responseText);
					var statusC = jqXHR.status;
					console
							.log("Fail Status Code: "
									+ statusC);
					console
							.log("Fail textStatus: "
									+ textStatus);
					console
							.log("Fail error: "
									+ errorThrown);
				}

			});


			
			
			
			
		};
		
		var id;
		var subject;
		$(document)
				.ready(
						function() {
							
							$('.summernote').summernote();
							
// 							id = $("#userID").val();
							sessionId=window.location.href.substring(45);
							id=sessionId;
							if(!readCookie("userID"))
							createCookie("userID",sessionId,1);
							
// 							if(id=="")
// 								window.location.replace("http://localhost:8080/login2");
							hideNavElements();
							hideSubElements();
							var jForm = new FormData();
							
							$('#stage0').show();

							$("#stateChange").submit(function(event) {
								
							});
							
							
							$('#discardEmail').click(function() {
								
								$('#mailInbox').show();
								$('#readedEmail').hide();
								$('#composeMailBox').hide();
								CALLERAJAX.callUsersRank();
							});
							$('#createMail').click(function() {
								$('#composeMailBox').show();
								$('#mailInbox').hide();
								$('#readedEmail').hide();
								CALLERAJAX.callUsersRank();
							});
							
							$('#sendEmail')
									.click(
											function() {
												myDivObj = document
														.getElementById("textToConvert");

												var subject = $(
														'#usersEmailSubject')
														.val();
												var content = myDivObj.innerHTML;
												var to = $(
														'#usersToSendEmail option:selected')
														.val();
												id = $('#usersOwner').val();

												var jForm = new FormData();
												jForm
														.append("subject",
																subject);
												jForm
														.append("content",
																content);
												jForm.append("to", to);
												jForm.append("id", id);

												$.ajax({
															type : "POST",
															contentType : false,
															url : "http://localhost:8080/createEmail",
															data : jForm,
															processData : false,
															cache : false,
															async : true,
															dataType : 'text',
															
															success : function(
																	data) {
																
															},
															error : function(
																	jqXHR,
																	textStatus,
																	errorThrown) {

																var responseText = jqXHR.responseText;
																console
																		.log("Fail responseText: "
																				+ responseText);
																var statusC = jqXHR.status;
																console
																		.log("Fail Status Code: "
																				+ statusC);
																console
																		.log("Fail textStatus: "
																				+ textStatus);
																console
																		.log("Fail error: "
																				+ errorThrown);
															}

														});
												$('#messageShow').click();
											});

							$('#submitFilePost')
									.click(
											function() {
												if ($('#postTittle').val() != "")
													$('#tittleName').val(
															$('#postTittle')
																	.val());
												if ($('#postDescription').val() != "")
													$('#description')
															.val(
																	$(
																			'#postDescription')
																			.val());

											});
							$('#setDrag').click(
									function() {
										// 				var image = new Image();
										// 				image.src =$image.cropper("getDataURL");				
										$('#userImg').val(
												$image.cropper("getDataURL"));
									})
							$('#fileUploadNew').hide();
							
							
							//TODO
							
							$('#adminShow').click(function() {
								hideSubElements();
								hideNavElements();
								$('#stageStadistic').show();
								CALLERAJAX.callStadistics();
							});
							
							
							
							
							$('#editProfileButton').click(function() {
								hideSubElements();
								hideNavElements();
								$('#stage12').show();
							});
							$('#messageShowButton').click(function() {
								hideNavElements();
								$('#stage2').show();
								$('#composeMailBox').hide();
								$('#mailInbox').show();

								hideSubElements();
							});
							$('#createMessageHome').click(function (){
										
								hideNavElements();
								$('#mailInbox').hide();
								$('#stage2').show();
								$('#readedEmail').hide();
								CALLERAJAX.callUsersRank();
								hideSubElements();
								CALLERAJAX.callUserInbox();
								$('#composeMailBox').show();
								
									});
							$('#deleteElementSubmit')
									.click(
											function() {
												var text = $(
														'#deleteSelect option:selected')
														.val();
												$('#elementToDeleteId').val(
														text);
												$('#fileUploadNew').hide();
											});

							$('#findSearchElement')
									.click(
											function() {
												var text = $(
														'#findSearchSelect option:selected')
														.val();
												$('#elementToFindId').val(text);
												$('#fileUploadNew').hide();
											});
							$('#mod').click(
									function() {
										$('#modForm').show();
										var text = $(
												'#findSelect option:selected')
												.val();
										$('#modId').val(text);
										text = $('#findSelect option:selected')
												.text();
										$('#modName').val(text);
										$('#fileUploadNew').hide();
									});
							function hideNavElements() {
								$('#stage0').hide();
								$('#stage1').hide();
								$('#stage2').hide();
								$('#stage3').hide();
								$('#stage12').hide();
								$('#modForm').hide();
								$('#composeMailBox').hide();
								$('#workgroups').hide();
								$("#temaLoad").hide();
								$('#verTrofeos').hide();
								
								
								
								$('#stageStadistic').hide();
							}
							;
							function hideSubElements() {
								$('#stage51').hide();
								$('#stage52').hide();
								$('#stage53').hide();
								$('#loadMoreResults').hide();

							}
							;

							$('#editProfileShow').click(function() {
								hideSubElements();
								hideNavElements();
								$('#stage12').show();
							});

							$('#modifyUploadedShow').click(function() {
								hideSubElements();

								hideNavElements();
							});
							$('#deleteUploadedShow').click(function() {
								hideSubElements();
								$('#stage53').show();
								hideNavElements();
							});
							$('#seeUploadedShow').click(function() {
								hideSubElements();
								$('#stage52').show();
								hideNavElements();
							});

							$('#uploadShow').click(function() {
								hideSubElements();
								$('#stage51').show();
								hideNavElements();
							});

							$('#homeShow').click(function() {
								hideNavElements();
								$('#stage0').show();
								hideSubElements();
								CALLERAJAX.callNotificationsForMe();
								CALLERAJAX.getStats();
							});
							$("#messageShow").click(function() {
								hideNavElements();
								$('#mailInbox').show();
								$('#stage2').show();
								$('#readedEmail').hide();
								CALLERAJAX.callUsersRank();
								hideSubElements();
								CALLERAJAX.callUserInbox();
							});
							$('#rankShow').click(function() {
								hideNavElements();
								$('#stage3').show();
								hideSubElements();
								CALLERAJAX.callUsersRank();

							});
							$('#loadMoreResults').click(function() {
								
								
								
								CALLERAJAX.callAllNotifications($('#loadMoreLocation').val(), $('#loadMoreTema').val(), false);
							});
							
							$('#workspaces').click(function() {
								hideNavElements();
								$('#workgroups').show();
								$('#userWorkgroups').show();
								hideSubElements();
								CALLERAJAX.callUsersWorkgroups();

							});
							
							$('#trophies').click(function() {
								hideNavElements();
								$('#verTrofeos').show();
								hideSubElements();
								CALLERAJAX.callUsersTrofeos();

							});
							$('#usersRankButton').click(function() {
								CALLERAJAX.callUsersRank();
								hideNavElements();
								$('#stage3').show();
								hideSubElements();
							});
							$('#socialShow').click(function() {
								hideNavElements();
								$('#stage1').show();
								hideSubElements();
								CALLERAJAX.callAllNotifications("socialFeedRow",null, true);
								$('#loadMoreLocation').val("socialFeedRow");
// 								$('#beforePage').val("0");
// 								$('#currentPage').val("20");
								$('#loadMoreResults').show();
								
								
							});
							var cabeceraPost;
							var photosPost;
							var footerPost;//socialFeedRow

							CALLERAJAX = {

								callAllNotifications : function(target,temaId,flag) {
									cabeceraPost1 = "";
									photosPost1 = "";
									footerPost1 = "";
									iteractionalFooter = "";
									

									var start=$('#beforePage').val();
									var current=$('#currentPage').val();
									if(flag==true){
									$("#"+target).empty();
									start=0;
									current=20;
									}
									
									if(target=="socialFeedRow")
										url ="http://localhost:8080/notifications/feed/"+start+"/"+current;
									else
										url="http://localhost:8080/notifications/tweets/"+temaId+"/"+start+"/"+current;
// 									id = $("#userID").val();
									event.preventDefault();
									$
											.ajax({
												type : "GET",
												contentType : "application/x-www-form-urlencoded;charset=utf-8",
												url : url,

												dataType : 'JSONP',
												async : true,
												cache : false,
												dataType : 'text',
												timeout : 3000,
												success : function(data) {
													subjects = JSON.parse(data);
													for ( var item in subjects) {
														$('#beforePage').val(parseInt(subjects.length));
														$('#currentPage').val(parseInt(subjects.length)+20);
														
														medalla="";
														
														if(subjects[item].tropieType)
															if(subjects[item].tropieType=="PLATA")
																medalla=" <img alt='image'class='img-circle' src='resources/img/medallaPlata.png' style='border-radius: 1%;'/>";
																else
																if(subjects[item].tropieType=="ORO")
																	medalla=" <img alt='image'class='img-circle' src='resources/img/medallaOro.png' style='border-radius: 1%;'/>";
																	else
																	if(subjects[item].tropieType=="BRONCE")
																		medalla=" <img alt='image'class='img-circle' src='resources/img/medallaBronce.png' style='border-radius: 1%;'/>";
														
														cabeceraPost = 
															"<div class='social-feed-box' style='background-color: rgba(26, 179, 148, 0.15);'><div class='pull-right social-action dropdown'><button data-toggle='dropdown'class='dropdown-toggle btn-white'><i class='fa fa-angle-down'></i></button><ul class='dropdown-menu m-t-xs'><li><a href='#'>Config</a></li></ul></div>"
															
															
															
															+"<div class='feed-element'>"
															+ "<a href='#' class='pull-left'> "
															+ "<img alt='image'class='img-circle' src='/user/profile2/?loginName="
															+ subjects[item].notifi.action.owner
															+ "'></a>"
															+ "<div class='media-body '>"
															+ "<small class='pull-right'>"
															+ subjects[item].notifi.action.creationDate
															+ "</small> <strong>"
															+ subjects[item].notifi.action.owner
															+ "</strong> ";
													if (subjects[item].notifi.action.actionType == "POST")
														cabeceraPost = cabeceraPost
																+ "posted his album <strong>"
																+ subjects[item].notifi.description+"</strong>"
													else if (subjects[item].notifi.action.actionType == "COMMENT")														
															cabeceraPost = cabeceraPost
																		+ "commented on  <img alt='image'class='img-circle' src='resources/img/comment.png' style='border-radius: 1%;'/>";
													else if (subjects[item].notifi.action.actionType == "LIKE")														
														cabeceraPost = cabeceraPost
																			+ "likes <img alt='image'class='img-circle' src='resources/img/like.png' style='border-radius: 1%;'/>";
													else if (subjects[item].notifi.action.actionType == "STATE")														
														cabeceraPost = cabeceraPost
																+ "changed his state to <img alt='image'class='img-circle' src='resources/img/state.png' style='border-radius: 1%;'/>";
													else if (subjects[item].notifi.action.actionType == "TWEET")
														cabeceraPost = cabeceraPost
														+ "posted a tweet <img alt='image'class='img-circle' src='resources/img/tweet.png' style='border-radius: 1%;'/>"
													cabeceraPost = cabeceraPost+medalla
															+ "<br> <small class='text-muted'>"
															+ subjects[item].notifi.date+"</small>"
															+ "<div class='well'><strong>"
															
															+ subjects[item].notifi.action.content
															+ ".</strong></div>";
														
														
														
														
														
													
														//TODO
														
														
														likeable=subjects[item].likeable;
														actionId=subjects[item].notifi.action.id;
														
														tittle=subjects[item].notifi.description;
														owner=subjects[item].notifi.action.owner;
														ownerId=subjects[item].notifi.action.ownerId;
														actionId=subjects[item].notifi.action.id;
														
														
														
if (likeable == true) {

								footerPost1 = "<div class='btn-group'>"
										+ "<a class='btn btn-xs btn-white' id='createLike"+actionId+"' onclick=createLike("
										+ actionId+","+target+","+temaId
										+ ")><i class='fa fa-thumbs-up'></i> Like </a> "
										+ "<a class='btn btn-xs btn-white' onclick=CALLERAJAX.createComment("
										+ actionId+","+target+","+temaId
										+ ");><i class='fa fa-comments'></i>Comment</a> "
										+"<a class='btn btn-xs btn-white'  data-toggle='modal' data-target='#myModal6' onclick=puntuarTweet("+ actionId+","+target+","+temaId
										+");><i class='fa fa-check-circle-o'></i>Puntuar</a> "
										+ "</div></div>";
								
							}
							
else
footerPost1 = "<div class='btn-group'>"
											+ "<a class='btn btn-xs btn-blue' disabled ><i class='fa fa-thumbs-up'></i> Like </a> "
											+ "<a class='btn btn-xs btn-white' onclick=CALLERAJAX.createComment("
											+ actionId+","+target+","+temaId
											+ ");><i class='fa fa-comments'></i>Comment</a> "
											+"<a class='btn btn-xs btn-white' onclick=puntuarTweet("+ actionId+","+target+","+temaId
											+");><i class='fa fa-check-circle-o'></i>Puntuar</a> "
											+ "</div></div>";
											
docs=subjects[item].comments;											
var comment="";					
for ( var item1 in docs) {
								var colors=[];
								colors.push("rgba(178, 232, 172, 0.62);");
								colors.push("rgba(226, 175, 220, 0.25);");								
								colors.push("rgba(133, 175, 212, 0.41);");
								colors.push("rgba(226, 175, 220, 0.45);");
								colors.push("rgba(220, 131, 164, 0.23);");
								colors.push("#E4CEE4");								
								colors.push("rgba(152, 249, 218, 0.37);");
								colors.push("#98D1F9");
								colors.push("rgba(167, 249, 152, 0.61);");
								
								var currentColor=(item1%(colors.length));
								if( docs[item1].place==actionId){
								comment =comment+ "<div class='social-avatar' style='background-color: "+ colors[currentColor]+";'>"
										+ "<a href='#' class='pull-left'>"
										+ "<img alt='image' src='/user/profile2/?loginName="+docs[item1].owner+"'>"
										+ "</a><div class='media-body'>"
										+ "<a href='#'>"
										+ docs[item1].owner+"</a><p>"
										+ docs[item1].content
										+ "</p><br/>"
// 										+ "<a href='#' class='small'><i class='fa fa-thumbs-up'></i>"
// 										+ "<small>26 Like this!</small></a> -"
										+ "<small class='text-muted'>"
										+ docs[item1].creationDate
										+ "</small></div></div>";
										}

							}
							iteractionalFooter = "<div class='social-avatar'>"
									+ "<a href='#' class='pull-left'> "
									+ "<img alt='image' src='/user/photo/?id="
									+ id
									+ "'>"
									+

									"</a><div class='media-body'>"
									+ "<textarea class='form-control' placeholder='Write comment...' id=commentOn"+actionId+"></textarea>"
									+ "</div></div>"+footerPost1+comment;

									$("#"+target).append(
									cabeceraPost + photosPost1
											+ iteractionalFooter +"</div>");		
									
									
														
									
													
													}
													if(target=="socialFeedRow"){
													var where="#"+getCurrentStageVisible();
													hideNavElements();
													hideSubElements();
													$('#loadMoreResults').show();
													$(where).show();}
												},
												error : function(jqXHR,
														textStatus, errorThrown) {

													var responseText = jqXHR.responseText;
													console
															.log("Fail responseText: "
																	+ responseText);
													var statusC = jqXHR.status;
													console
															.log("Fail Status Code: "
																	+ statusC);
													console
															.log("Fail textStatus: "
																	+ textStatus);
													console.log("Fail error: "
															+ errorThrown);
												}

											});

								},
								loadProfile: function(){ 
									$.ajax({
										type : "GET",
										contentType : "application/x-www-form-urlencoded;charset=utf-8",
										url : "http://localhost:8080/getbytoken/"
												+ id,

										dataType : 'JSONP',
										async : true,
										cache : false,
										dataType : 'text',
										timeout : 3000,
										success : function(data) {
											if(data.indexOf("redirect:login2")==-1){
											subject = JSON.parse(data);
											
											setSubjectData(subject);
											var where="#"+getCurrentStageVisible();
											hideNavElements();
											hideSubElements();
											
											$(where).show();
											}
											else{
												
												eraseCookie("userID");
												
// 												window.location.replace("/login2");
												
											}
											
										},
										error : function(jqXHR,
												textStatus, errorThrown) {

											var responseText = jqXHR.responseText;
											console
													.log("Fail responseText: "
															+ responseText);
											var statusC = jqXHR.status;
											console
													.log("Fail Status Code: "
															+ statusC);
											console
													.log("Fail textStatus: "
															+ textStatus);
											console.log("Fail error: "
													+ errorThrown);
										}

									});
								},
								callStadistics: function() {
									$
									.ajax({
										type : "GET",
										contentType : "application/x-www-form-urlencoded;charset=utf-8",
										url : "http://localhost:8080/getstats/"
												+ id,

										dataType : 'JSONP',
										async : true,
										cache : false,
										dataType : 'text',
										
										success : function(data) {
											subject = JSON.parse(data);
											$("#newRegister").empty();
											$("#newLikes").empty();
											$("#totalCommentsPlaces").empty();
											$("#totalMicroBloginPlaces").empty();
											$("#microBloginPorcent").empty();
											$("#totalLikesPlaces").empty();
											$("#likesPorcent").empty();
											$("#totalCommentsPlaces").empty();
											$("#commentsPorcent").empty();

											
											$("#newRegister").append( subject.newRegister);
											$("#newLikes").append( subject.newLikes);
											$("#totalCommentsPlaces").append( subject.totalCommentsPlaces);
											$("#totalMicroBloginPlaces").append( subject.totalMicroBloginPlaces);
											$("#microBloginPorcent").append( subject.microBloginPorcent);
											$("#totalLikesPlaces").append( subject.totalLikesPlaces);
											$("#likesPorcent").append( subject.likesPorcent);
											$("#totalCommentsPlaces").append( subject.totalCommentsPlaces);
											$("#commentsPorcent").append( subject.commentsPorcent);
										},
										error : function(jqXHR,
												textStatus, errorThrown) {

											var responseText = jqXHR.responseText;
											console
													.log("Fail responseText: "
															+ responseText);
											var statusC = jqXHR.status;
											console
													.log("Fail Status Code: "
															+ statusC);
											console
													.log("Fail textStatus: "
															+ textStatus);
											console.log("Fail error: "
													+ errorThrown);
										}

									});
								},
								getStats: function(){
									
// 									id=$("#userID").val();
									
									$.ajax({
										type : "GET",
										contentType : "application/x-www-form-urlencoded;charset=utf-8",
										url : "http://localhost:8080/stats/"
												+ id,

										dataType : 'JSONP',
										async : true,
										cache : false,
										dataType : 'text',
										timeout : 3000,
										success : function(data) {
											subjects = JSON.parse(data);
											
											$('#commentsCount').text(subjects.numberOfComments);
											$('#likesCount').text(subjects.numberOfLikes);
											var where="#"+getCurrentStageVisible();
											
											document.getElementById('progressBar').style.width=subjects.progress+'%'
											document.getElementById('tweetsBar').style.width=subjects.microblogging+'%'
											document.getElementById('activitiesBar').style.width=subjects.activities+'%'
											
											hideNavElements();
											hideSubElements();
											$(where).show();
											
										},
										error : function(jqXHR,
												textStatus, errorThrown) {

											var responseText = jqXHR.responseText;
											console
													.log("Fail responseText: "
															+ responseText);
											var statusC = jqXHR.status;
											console
													.log("Fail Status Code: "
															+ statusC);
											console
													.log("Fail textStatus: "
															+ textStatus);
											console.log("Fail error: "
													+ errorThrown);
										}

									});
								},
								
								
								
								createComment:	function (placeId,target,temaId) {
									
									var place ='#'+ "commentOn" + placeId ;
									var value=$(place).val();
									//TODO
									 
									var jForm = new FormData();
									jForm.append("user",  readCookie($('#usrName1').text()));
									jForm.append("placeId", placeId);
									jForm.append("content", value);
									

									$.ajax({
										type : "POST",
										contentType : false,
										url : "http://localhost:8080/comment",
										data : jForm,
										processData : false,
										cache : false,
										async : true,
										dataType : 'text',
										timeout : 3000,
										success : function(data) {
// 											CALLERAJAX.getStats();
// 											var where="#"+getCurrentStageVisible();
											
// 											hideNavElements();
// 											hideSubElements();
											
// 											$('#'+target).show();
											
										},
										error : function(jqXHR, textStatus, errorThrown) {

											var responseText = jqXHR.responseText;
											console.log("Fail responseText: " + responseText);
											var statusC = jqXHR.status;
											console.log("Fail Status Code: " + statusC);
											console.log("Fail textStatus: " + textStatus);
											console.log("Fail error: " + errorThrown);
											alert("se produjo un error enviando el comentario");
										}

									});
									CALLERAJAX.callAllNotifications(target.id, temaId, true);
								
								},
								
								
								callUsersTrofeos : function() {
									$("#userTrofeos").empty();

									$
											.ajax({
												type : "GET",
												contentType : "application/x-www-form-urlencoded;charset=utf-8",
												url : "http://localhost:8080/trophies/",

												dataType : 'JSONP',
												async : true,
												cache : false,
												dataType : 'text',
												timeout : 3000,
												success : function(data) {
													subjects = JSON.parse(data);
													
													for ( var item in subjects) {
														
														
														$("#userTrofeos")
																.append(
																		"<div class='col-lg-4'>"
																				+ "<div class='contact-box'>"
																				
																				+ "<div class='col-sm-4'>"
																				+ "<div class='text-center'>"
																				+ "<img alt='image' class='img-circle m-t-xs img-responsive' src=/user/rank/photo?id="
																				+ subjects[item].id
																				+"&sessionId="
																				+ id
																				+ ">"
																				+ "<div class='m-t-xs font-bold'>"
																				+ subjects[item].role
																				+ "</div>"
																				+ "</div>"
																				+

																				"</div>"
																				+ "<div class='col-sm-8'>"
																				+ "<h3>"
																				+ "<strong>"
																				+ subjects[item].login
																				+ "</strong>"
																				+ "</h3>"
																				+ "<p><i class='fa fa-map-marker'></i> "+subjects[item].city+"</p>"
																				+ "<address>"
																				+ "<strong>Puntos acumulados</strong>"
																				+ "<br>Posts<br> "
																				+ "trofeos<br> "
																				+ "<abbr title='Phone'>P:</abbr> estado</address>"
																				+ "</div>"
																				+ "<div class='clearfix'>"
																				+ "</div>"
																				
																				+ "</div>"
																				+ "</div>")

													}
												},
												error : function(jqXHR,
														textStatus, errorThrown) {

													var responseText = jqXHR.responseText;
													console
															.log("Fail responseText: "
																	+ responseText);
													var statusC = jqXHR.status;
													console
															.log("Fail Status Code: "
																	+ statusC);
													console
															.log("Fail textStatus: "
																	+ textStatus);
													console.log("Fail error: "
															+ errorThrown);
												}

											});

								},
								
								callUsersWorkgroups : function() {
									$("#userWorkgroups").empty();
									
									$
											.ajax({
												type : "GET",
												contentType : "application/x-www-form-urlencoded;charset=utf-8",
												url : "http://localhost:8080/workgroups/",

												dataType : 'JSONP',
												async : true,
												cache : false,
												dataType : 'text',
												timeout : 3000,
												success : function(data) {
													subjects = JSON.parse(data);
													var colors=[];
													colors.push("#E8E8AC");
													colors.push("#B2E8AC");
													colors.push("#85AFD4");
													colors.push("#E2AFDC");
													colors.push("#DC83A4");
													
													
													
													
													for ( var item in subjects) {
														
														var currentColor=(item%(colors.length));
														$("#userWorkgroups")
																.append(
																		"<div class='col-lg-4'>"
																				+ "<div class='contact-box' style=' background-color: "
																			    /* background-color: #E8E8AC; */
																			    + colors[currentColor]
																			    +";'>"																				
																				+ "<div class='col-sm-4'>"
																				+ "<div class='text-center'>"
																				+ "<img alt='image' class='img-circle m-t-xs img-responsive' src=/user/rank/photo?id="
																				+ subjects[item].baseAction.ownerId
																				+"&sessionId="
																				+ id
																				+ ">"
																				+ "<div class='m-t-xs font-bold'>"
																				+ subjects[item].baseAction.owner
																				+ "</div>"
																				+ "</div>"
																				+

																				"</div>"
																				+ "<div class='col-sm-8'>"
																				+ "<h3>"
																				+ "<strong>"
																				+ subjects[item].tittle
																				+ "</strong>"
																				+ "</h3>"
																				+ "<p> "+subjects[item].hastags+"</p>"
																				+ "<address>"
																				+ "<strong>Comentarios y likes</strong>"
																				+ "<br>Comentarios "+subjects[item].numberOfComments
																				+ "<br>Likes "+subjects[item].numberOfLikes
																				+"<br> </address>"
																				+ "</div>"
																				+ "<div class='clearfix'>"
																				+"<button class='btn btn-primary btn-sm demo1' onclick='cargarCommentsTema("+subjects[item].id+")')>Ir a tema</button>"
																				+ "</div>"
																				
																				+ "</div>"
																				+ "</div>")

													}
												},
												error : function(jqXHR,
														textStatus, errorThrown) {

													var responseText = jqXHR.responseText;
													console
															.log("Fail responseText: "
																	+ responseText);
													var statusC = jqXHR.status;
													console
															.log("Fail Status Code: "
																	+ statusC);
													console
															.log("Fail textStatus: "
																	+ textStatus);
													console.log("Fail error: "
															+ errorThrown);
												}

											});

								},
								callUsersRank : function() {
									$("#userRankingGrid").empty();

									$
											.ajax({
												type : "GET",
												contentType : "application/x-www-form-urlencoded;charset=utf-8",
												url : "http://localhost:8080/aboutjson2/"+id,

												dataType : 'JSONP',
												async : true,
												cache : false,
												dataType : 'text',
												timeout : 3000,
												success : function(data) {
													subjects = JSON.parse(data);
													$("#usersToSendEmail").empty();
													for ( var item in subjects) {
														
														$("#usersToSendEmail")
																.append(
																		$(
																				'<option>',
																				{
																					value : subjects[item].login,
																					text : subjects[item].login
																				}));
														$("#userRankingGrid")
																.append(
																		"<div class='col-lg-4'>"
																				+ "<div class='contact-box'>"
																				
																				+ "<div class='col-sm-4'>"
																				+ "<div class='text-center'>"
																				+ "<img alt='image' class='img-circle m-t-xs img-responsive' src=/user/rank/photo?id="
																				+ subjects[item].id
																				+"&sessionId="
																				+ id
																				+ ">"
																				+ "<div class='m-t-xs font-bold'>"
																				+ subjects[item].role
																				+ "</div>"
																				+ "</div>"
																				+

																				"</div>"
																				+ "<div class='col-sm-8'>"
																				+ "<h3>"
																				+ "<strong>"
																				+ subjects[item].login
																				+ "</strong>"
																				+ "</h3>"
																				+ "<p><i class='fa fa-map-marker'></i> "+subjects[item].city+"</p>"
																				+ "<address>"
																				+ "<strong>Puntos acumulados</strong>"
																				+ "<br>Posts<br> "
																				+ "trofeos<br> "
																				
																				+ "</div>"
																				+ "<div class='clearfix'>"
																				+ "</div>"
																				
																				+ "</div>"
																				+ "</div>")

													}
												},
												error : function(jqXHR,
														textStatus, errorThrown) {

													var responseText = jqXHR.responseText;
													console
															.log("Fail responseText: "
																	+ responseText);
													var statusC = jqXHR.status;
													console
															.log("Fail Status Code: "
																	+ statusC);
													console
															.log("Fail textStatus: "
																	+ textStatus);
													console.log("Fail error: "
															+ errorThrown);
												}

											});

								},
								callUserInbox : function() {
									$("#messageInboxs").empty();
// 									id = $("#userID").val();
									var emailsIncoming = "<tbody>";
									$
											.ajax({
												type : "GET",
												contentType : "application/x-www-form-urlencoded;charset=utf-8",
												url : "http://localhost:8080/emails/inbox/"
														+ id,

												dataType : 'JSONP',
												async : true,
												cache : false,
												dataType : 'text',
												timeout : 3000,
												success : function(data) {
													mailIn = JSON.parse(data);
													if (mailIn.length == 0)

														document
																.getElementById("inboxNumber").innerHTML = mailIn.length
													document
															.getElementById("inboxNumber2").innerHTML = mailIn.length
													document
															.getElementById("inboxNumber3").innerHTML = mailIn.length
													iterElementsCount = 0;
													for ( var item in mailIn) {
														var elements = mailIn[item];//.email
														emailArray
																.push(elements)
														var elements2 = mailIn[item].email;//
														var isReadLabel = "<tr class='unread'>";
														if (mailIn[item].isRead == true)
															isReadLabel = "<tr class='read'>";
														emailsIncoming = emailsIncoming
																+ ""
																+ isReadLabel
																+ "<td class='check-mail'>"
																+ "<div class='icheckbox_square-green' style='position: relative;'>"
																+ "<input type='checkbox' class='i-checks' style='position: absolute; opacity: 0;'>"
																+ "<ins class='iCheck-helper' style='position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);'>"
																+ "</ins></div></td>"
																+ "<td class='mail-ontact'>"
																+ "<a id="
																+ iterElementsCount
																+ " onclick=openEmail("
																+ iterElementsCount
																+ ")>"
																+ elements2.owner
																+ "</a></td><td class='mail-subject'>"
																+ "<a href='mail_detail.html'>"
																+ elements2.subject
																+ "</a></td><td class='text-right mail-date'>"
																+ elements2.creationDate
																+ "</td></tr>";

														iterElementsCount = iterElementsCount + 1;
													}
													$("#messageInboxs")
															.append(
																	emailsIncoming
																			+ "</tbody>");
												},
												error : function(jqXHR,
														textStatus, errorThrown) {

													var responseText = jqXHR.responseText;
													console
															.log("Fail responseText: "
																	+ responseText);
													var statusC = jqXHR.status;
													console
															.log("Fail Status Code: "
																	+ statusC);
													console
															.log("Fail textStatus: "
																	+ textStatus);
													console.log("Fail error: "
															+ errorThrown);
												}

											});

								},
								callNotificationsForMe : function() {
// 									id = $("#userID").val();
									cabeceraPost = "";
									photosPost = "";
									footerPost = "";
									$("#notification4me").empty();
									$
											.ajax({
												type : "GET",
												contentType : "application/x-www-form-urlencoded;charset=utf-8",
												url : "http://localhost:8080/notifications/",

												dataType : 'JSONP',
												async : true,
												cache : false,
												dataType : 'text',
												timeout : 3000,
												success : function(data) {
													$("#notification4me")
															.empty();
													subjects = JSON.parse(data);
													for ( var item in subjects) {
														// 														$("#userRankingGrid").append(
														cabeceraPost = "<div class='feed-element'>"
																+ "<a href='#' class='pull-left'> "
																+ "<img alt='image'class='img-circle' src='/user/photo/?id="
																+ id
																+ "'>"
																+ "</a><div class='media-body '>"
																+ "<small class='pull-right'>"
																+ subjects[item].action.creationDate
																+ "</small> <strong>"
																+ subjects[item].action.owner
																+ "</strong> ";
														if (subjects[item].action.actionType == "POST")
															cabeceraPost = cabeceraPost
																	+ "posted his album "
																	+ subjects[item].action.content
																	
														else if (subjects[item].action.actionType == "COMMENT")														
																		cabeceraPost = cabeceraPost
																					+ "commented on  <img alt='image'class='img-circle' src='resources/img/comment.png' style='border-radius: 1%;'/>";
																else if (subjects[item].action.actionType == "LIKE")														
																	cabeceraPost = cabeceraPost
																						+ "likes <img alt='image'class='img-circle' src='resources/img/like.jpg' style='border-radius: 1%;'/>";
																else if (subjects[item].action.actionType == "STATE")														
																	cabeceraPost = cabeceraPost
																			+ "changed his state to <img alt='image'class='img-circle' src='resources/img/state.png' style='border-radius: 1%;'/>";
																else if (subjects[item].action.actionType == "TWEET")
																	cabeceraPost = cabeceraPost
																	+ "posted a tweet <img alt='image'class='img-circle' src='resources/img/tweet.jpg' style='border-radius: 1%;'/>"
// 														else if (subjects[item].action.actionType == "STATE")
// 															cabeceraPost = cabeceraPost
// 																	+ "changed his state to ";
// 														else if (subjects[item].action.actionType == "TWEET")
// 															cabeceraPost = cabeceraPost
// 															+ "posted a tweet";
														cabeceraPost = cabeceraPost
																+ "<br> <small class='text-muted'>"
																+subjects[item].date
																+ "pm - 12.06.2014</small>"
																+ "<div class='well'>"
																+ subjects[item].description
																+ ".</div>";

														getDocsElements(
																subjects[item].description,
																id,
																cabeceraPost,
																subjects[item].action.owner);
														
													}
													var where="#"+getCurrentStageVisible();
													hideNavElements();
													hideSubElements();
													
													$(where).show();
												},
												error : function(jqXHR,
														textStatus, errorThrown) {

													var responseText = jqXHR.responseText;
													console
															.log("Fail responseText: "
																	+ responseText);
													var statusC = jqXHR.status;
													console
															.log("Fail Status Code: "
																	+ statusC);
													console
															.log("Fail textStatus: "
																	+ textStatus);
													console.log("Fail error: "
															+ errorThrown);
												}

											});

								}
							}

							$('.i-checks').iCheck({
								checkboxClass : 'icheckbox_square-green',
								radioClass : 'iradio_square-green',
							});
							CALLERAJAX.callNotificationsForMe();
							Dropzone.options.myAwesomeDropzone = {

								autoProcessQueue : false,
								uploadMultiple : true,
								parallelUploads : 100,
								maxFiles : 100,

								// Dropzone settings
								init : function() {
									var myDropzone = this;

									this.element
											.querySelector(
													"button[type=submit]")
											.addEventListener(
													"click",
													function(e) {
														e.preventDefault();
														e.stopPropagation();
														myDropzone
																.processQueue();
													});
									this.on("sendingmultiple", function() {
									});
									this.on("successmultiple", function(files,
											response) {
									});
									this.on("errormultiple", function(files,
											response) {
									});
								}

							}
							CALLERAJAX.callUserInbox();
							var $image = $(".image-crop > img")
							$($image).cropper({
								aspectRatio : 1.618,
								preview : ".img-preview",
								done : function(data) {
									// Output the result data for cropping image.
								}
							});

							var $inputImage = $("#inputImage");
							if (window.FileReader) {
								$inputImage
										.change(function() {
											var fileReader = new FileReader(), files = this.files, file;

											if (!files.length) {
												return;
											}

											file = files[0];

											if (/^image\/\w+$/.test(file.type)) {
												fileReader.readAsDataURL(file);
												fileReader.onload = function() {
													$inputImage.val("");
													$image.cropper("reset",
															true).cropper(
															"replace",
															this.result);
												};
											} else {
												showMessage("Please choose an image file.");
											}
										});
							} else {
								$inputImage.addClass("hide");
							}
							CALLERAJAX.getStats();
							CALLERAJAX.loadProfile();
							$("#download").click(function() {

								window.open($image.cropper("getDataURL"));
							});

							$("#zoomIn").click(function() {
								$image.cropper("zoom", 0.1);
							});

							$("#zoomOut").click(function() {
								$image.cropper("zoom", -0.1);
							});

							$("#rotateLeft").click(function() {
								$image.cropper("rotate", 45);
							});

							$("#rotateRight").click(function() {
								$image.cropper("rotate", -45);
							});

							$("#setDrag").click(function() {
								$image.cropper("setDragMode", "crop");
							});

							$('#data_1 .input-group.date').datepicker({
								todayBtn : "linked",
								keyboardNavigation : false,
								forceParse : false,
								calendarWeeks : true,
								autoclose : true
							});

							$('#data_2 .input-group.date').datepicker({
								startView : 1,
								todayBtn : "linked",
								keyboardNavigation : false,
								forceParse : false,
								autoclose : true,
								format : "dd/mm/yyyy"
							});

							$('#data_3 .input-group.date').datepicker({
								startView : 2,
								todayBtn : "linked",
								keyboardNavigation : false,
								forceParse : false,
								autoclose : true
							});

							$('#data_4 .input-group.date').datepicker({
								minViewMode : 1,
								keyboardNavigation : false,
								forceParse : false,
								autoclose : true,
								todayHighlight : true
							});

							$('#data_5 .input-daterange').datepicker({
								keyboardNavigation : false,
								forceParse : false,
								autoclose : true
							});

							var elem = document.querySelector('.js-switch');
							var switchery = new Switchery(elem, {
								color : '#1AB394'
							});
							
							var elem_2 = document.querySelector('.js-switch_2');
							var switchery_2 = new Switchery(elem_2, {
								color : '#ED5565'
							});

							var elem_3 = document.querySelector('.js-switch_3');
							var switchery_3 = new Switchery(elem_3, {
								color : '#1AB394'
							});

							$('.i-checks').iCheck({
								checkboxClass : 'icheckbox_square-green',
								radioClass : 'iradio_square-green'
							});

							$('.demo1').colorpicker();

							var divStyle = $('.back-change')[0].style;
							$('#demo_apidemo').colorpicker({
								color : divStyle.backgroundColor
							}).on('changeColor', function(ev) {
								divStyle.backgroundColor = ev.color.toHex();
							});

							$('.clockpicker').clockpicker();

							$('input[name="daterange"]').daterangepicker();

							$('#reportrange span').html(
									moment().subtract(29, 'days').format(
											'MMMM D, YYYY')
											+ ' - '
											+ moment().format('MMMM D, YYYY'));

							$('#reportrange')
									.daterangepicker(
											{
												format : 'MM/DD/YYYY',
												startDate : moment().subtract(
														29, 'days'),
												endDate : moment(),
												minDate : '01/01/2012',
												maxDate : '12/31/2015',
												dateLimit : {
													days : 60
												},
												showDropdowns : true,
												showWeekNumbers : true,
												timePicker : false,
												timePickerIncrement : 1,
												timePicker12Hour : true,
												ranges : {
													'Today' : [ moment(),
															moment() ],
													'Yesterday' : [
															moment().subtract(
																	1, 'days'),
															moment().subtract(
																	1, 'days') ],
													'Last 7 Days' : [
															moment().subtract(
																	6, 'days'),
															moment() ],
													'Last 30 Days' : [
															moment().subtract(
																	29, 'days'),
															moment() ],
													'This Month' : [
															moment().startOf(
																	'month'),
															moment().endOf(
																	'month') ],
													'Last Month' : [
															moment()
																	.subtract(
																			1,
																			'month')
																	.startOf(
																			'month'),
															moment()
																	.subtract(
																			1,
																			'month')
																	.endOf(
																			'month') ]
												},
												opens : 'right',
												drops : 'down',
												buttonClasses : [ 'btn',
														'btn-sm' ],
												applyClass : 'btn-primary',
												cancelClass : 'btn-default',
												separator : ' to ',
												locale : {
													applyLabel : 'Submit',
													cancelLabel : 'Cancel',
													fromLabel : 'From',
													toLabel : 'To',
													customRangeLabel : 'Custom',
													daysOfWeek : [ 'Su', 'Mo',
															'Tu', 'We', 'Th',
															'Fr', 'Sa' ],
													monthNames : [ 'January',
															'February',
															'March', 'April',
															'May', 'June',
															'July', 'August',
															'September',
															'October',
															'November',
															'December' ],
													firstDay : 1
												}
											},
											function(start, end, label) {
												console.log(
														start.toISOString(),
														end.toISOString(),
														label);
												$('#reportrange span')
														.html(
																start
																		.format('MMMM D, YYYY')
																		+ ' - '
																		+ end
																				.format('MMMM D, YYYY'));
											});

							$(".select2_demo_1").select2();
							$(".select2_demo_2").select2();
							$(".select2_demo_3").select2({
								placeholder : "Select a state",
								allowClear : true
							});

						});

		$('.file-box').each(function() {

			animationHover(this, 'pulse');
		});
		var config = {
			'.chosen-select' : {},
			'.chosen-select-deselect' : {
				allow_single_deselect : true
			},
			'.chosen-select-no-single' : {
				disable_search_threshold : 10
			},
			'.chosen-select-no-results' : {
				no_results_text : 'Oops, nothing found!'
			},
			'.chosen-select-width' : {
				width : "95%"
			}
		}
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
		}

		$("#ionrange_1").ionRangeSlider({
			min : 0,
			max : 5000,
			type : 'double',
			prefix : "$",
			maxPostfix : "+",
			prettify : false,
			hasGrid : true
		});

		$("#ionrange_2").ionRangeSlider({
			min : 0,
			max : 10,
			type : 'single',
			step : 0.1,
			postfix : " carats",
			prettify : false,
			hasGrid : true
		});

		$("#ionrange_3").ionRangeSlider({
			min : -50,
			max : 50,
			from : 0,
			postfix : "°",
			prettify : false,
			hasGrid : true
		});

		$("#ionrange_4").ionRangeSlider(
				{
					values : [ "January", "February", "March", "April", "May",
							"June", "July", "August", "September", "October",
							"November", "December" ],
					type : 'single',
					hasGrid : true
				});

		$("#ionrange_5").ionRangeSlider({
			min : 10000,
			max : 100000,
			step : 100,
			postfix : " km",
			from : 55000,
			hideMinMax : true,
			hideFromTo : false
		});

		$(".dial").knob();

		$("#basic_slider").noUiSlider({
			start : 40,
			behaviour : 'tap',
			connect : 'upper',
			range : {
				'min' : 20,
				'max' : 80
			}
		});

		$("#range_slider").noUiSlider({
			start : [ 40, 60 ],
			behaviour : 'drag',
			connect : true,
			range : {
				'min' : 20,
				'max' : 80
			}
		});

		$("#drag-fixed").noUiSlider({
			start : [ 40, 60 ],
			behaviour : 'drag-fixed',
			connect : true,
			range : {
				'min' : 20,
				'max' : 80
			}
		});

		var edit = function() {
			$('.click2edit').summernote({
				focus : true
			});
		};
		var save = function() {
			var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
			$('.click2edit').destroy();
		};
	</script>
</body>



</html>
